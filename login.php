<?php require_once('./template/connect.php') ?>
<?php require_once('./template/header.php') ?>
<?php //require_once('./template/navbar.php') ?>
<style>
.common-heading h2 {
    margin-bottom: -7px;
}
.radius_card {
  padding: 10px;
  border-top-left-radius: 50px;
  border-bottom-right-radius: 50px;
}
.incorrect {
  color: white;
  text-shadow: 1px 1px 2px #fb0000, 0 0 25px blue, 0 0 5px darkblue;
}
</style>
<section class="hero-section hero-bg-bg1 bg-gradient1">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8  text-center">

        <div class="bg-gradient pt60 pb60 mt30 mb30 p-5 radius_card">

          <div class="common-heading text-l">
            <h2 class="mt0 text-center">เข้าสู่ระบบ</h2>
          </div>
          <div class="form-block p-5">
            <form id="form_login" name="feedback-form">
              <div class="fieldsets row">
                <div class="col-md-12">
                  <input type="text" placeholder="Username" id="user" name="user" class="text-center" style="border-radius: 25px;" autocomplete="new-password"/>
                </div>
              </div>
              <div class="fieldsets row">
                <div class="col-md-12">
                  <input type="password" placeholder="Password" id="pass" name="pass" class="text-center" style="border-radius: 25px;" autocomplete="new-password"/>
                </div>
              </div>
              <label id="text_error" class="incorrect" style="display:none">Username or Password Incorrect</label>
              <div class="fieldsets mt20"> 
                <button type="button" id="btn_login" name="submit" class="lnk btn-main bg-btn">Login <span class="circle"></span></button> 
              </div>
            </form>
          </div>

        </div>
         
      </div>
    </div>
  </div>
</section>

<?php //require_once('./template/mobile_menu.php') ?>
<?php require_once('./template/script.php') ?>
  
<script>
$('#btn_login').click(function () { 
  var user = $('#user').val();
  var pass = $('#pass').val();
  $.ajax({
    type: "POST",
    url: "template/action_login.php",
    data: {
      user:user,
      pass:pass
    },
    success: function(res) 
    {
      if (res == 1) {
        window.location.href = './';
      }else{
        $('#text_error').show(200);
      }
    }
  });
});
</script>