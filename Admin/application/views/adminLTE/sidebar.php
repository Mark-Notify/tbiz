<?php 
  $user = ssUser();
  // pp($user);
?>
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="<?php echo base_url('assets/plugins/') ?>admin-lte/dist/img/AdminLTELogo.png" alt="TBIZ Education" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">T-BIZ Education</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo base_url('assets/plugins/') ?>admin-lte/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="TBIZ Education">
        </div>
        <div class="info d-inline-block w-100">
          <a href="#" class="d-block">กฤษณะ  ศิริโท</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- <li class="nav-item has-treeview menu-open">
            <a href="<?php echo base_url('Home') ?>" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
          </li> -->
          <?php 
            if (!empty($user['employee_id']) && $user['employee_id'] == 1) {
          ?>
          <li class="nav-header">บัญชีและการเงิน</li>
          <li class="nav-item">
            <a href="<?php echo base_url('Income') ?>" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                บัญชีรายรับ
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url('Salary') ?>" class="nav-link">
              <i class="nav-icon fas fa-dollar-sign"></i>
              <p>
                โปรแกรมเงินเดือน
              </p>
            </a>
          </li>
          <li class="nav-header">งานทะเบียน</li>
          <li class="nav-item">
            <a href="<?php echo base_url('Teacher') ?>" class="nav-link">
            <i class="nav-icon fas fa-address-book"></i>
              <p>
                ทะเบียนครู
              </p>
            </a>
          </li>
          <?php } ?>

          <li class="nav-header">รายงาน</li>
          <?php 
            if (!empty($user['teacher_id'])) {
          ?>
          <li class="nav-item">
            <a href="<?php echo base_url('TeacherDTR') ?>" class="nav-link">
            <i class="nav-icon fas fa-align-left"></i>
              <p>
                <!-- ใบลงเวลาครู -->
                DTR
              </p>
            </a>
          </li>
          <?php } ?>
          <li class="nav-item">
            <a href="<?php echo base_url('TeacherSUB') ?>" class="nav-link">
            <i class="nav-icon fas fa-align-left"></i>
              <p>
                <?php
                  if (!empty($user['employee_id']) && $user['employee_id'] == 1) {
                    echo 'ใบลงเวลาครูสอนแทน';
                  }else{
                    echo 'Substitute';
                  }
                ?>
              </p>
            </a>
          </li>

          <?php 
            if (!empty($user['employee_id']) && $user['employee_id'] == 1) {
          ?>
          <li class="nav-item">
            <a href="<?php echo base_url('ReportDTR') ?>" class="nav-link">
            <i class="nav-icon fas fa-align-left"></i>
              <p>
                สรุปใบลงเวลาครู
              </p>
            </a>
          </li>
          <?php } ?>

          <li class="nav-item">
            <a href="<?php echo base_url('ReportSUB') ?>" class="nav-link">
            <i class="nav-icon fas fa-align-left"></i>
              <p>
                <?php
                  if (!empty($user['employee_id']) && $user['employee_id'] == 1) {
                    echo 'สรุปใบค่าสอนแทน';
                  }else{
                    echo 'Report Substitute';
                  }
                ?>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url('CashAdvance') ?>" class="nav-link">
            <i class="nav-icon fas fa-align-left"></i>
              <p>
              <?php
                  if (!empty($user['employee_id']) && $user['employee_id'] == 1) {
                    echo 'ฟอร์มเบิกเงินล่วงหน้า';
                  }else{
                    echo 'Cash Advance';
                  }
                ?>
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>