<style>
  body{
    font-family:'Sarabun';
  }

  .table th {
    text-align: center !important;
  }

  .l-light {
    font-weight: normal !important;
  }

  fieldset{
    border-radius:5px;
    border: 2px solid !important;
    padding: 1rem !important;
  }

  legend{
    width:auto;
    margin: 0px;
  }

  /* .dataTables_length > label, .dataTables_filter > label{
    display: inline-flex;
    margin: 5px 10px 5px 0;
  } */

  div.dataTables_wrapper div.dataTables_length label {
    font-weight: normal;
    text-align: left;
    white-space: nowrap;
  }
  div.dataTables_wrapper div.dataTables_filter label {
    font-weight: normal;
    white-space: nowrap;
    text-align: left;
  }

  label:not(.form-check-label):not(.custom-file-label) {
    font-weight: normal;
    text-align: left;
    white-space: nowrap;
    display: inline-flex;
    /* margin: 0px 0px 0px 0; */
  }

  .dataTables_paginate, .dataTables_filter {
    float: right;
  }

  .have-click {
    cursor: pointer;
    color:#e84393 !important;
  }

  /* Loading */
  .loading {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    min-height: 100%;
    background-color: rgba(0, 0, 0, 0.2);
    background-image: url(<?php echo base_url('/assets/images/loading.gif') ?>);
    background-size: 200px 200px;
    background-position: center center;
    background-repeat: no-repeat;
    z-index: 9999;
    text-align: center;
    padding-top: 50%;
    font-weight: bold;
    color: #FFF;
    display: none;
  }

  .modal-xl {
    max-width: 70%;
  }

  @media (max-width: 576px){
    .modal-xl {
      max-width: 100%;
    }
  }

  .modal-xxl {
    max-width: 90%;
  }

  @media (max-width: 576px){
    .modal-xxl {
      max-width: 100%;
    }
  }

.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:active {
    /* color: #080808; */
    /* border: #000; */
    /* display: block; */
    width: 100%;
    height: calc(2.25rem + 2px);
    padding: .375rem .75rem;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    box-shadow: inset 0 0 0 transparent;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}

.bootstrap-select > .dropdown-toggle {
    position: relative;
    width: 100%;
    text-align: right;
    white-space: nowrap;
    display: -webkit-inline-box;
    display: -webkit-inline-flex;
    display: -ms-inline-flexbox;
    display: inline-flex;
    -webkit-box-align: center;
    -webkit-align-items: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-pack: justify;
    -webkit-justify-content: space-between;
    -ms-flex-pack: justify;
    justify-content: space-between;
    border-color: #b5adad;
}

#btnGoTop {
    display: none;
    position: fixed;
    top: 1rem;
    right: 1rem;
    z-index: 100;
    font-size: 1.5rem;
    padding: 0.7rem;
}

#btnGoBottom {
    display: none;
    position: fixed;
    top: 5.5rem;
    right: 1rem;
    z-index: 100;
    font-size: 1.5rem;
    padding: 0.7rem;
}

#btnGoTop2 {
    display: none;
    position: fixed;
    top: 1rem;
    right: 1rem;
    z-index: 100;
    font-size: 1.5rem;
    padding: 0.7rem;
}

#btnGoBottom2 {
    display: none;
    position: fixed;
    top: 5.5rem;
    right: 1rem;
    z-index: 100;
    font-size: 1.5rem;
    padding: 0.7rem;
}

.dotted {
    border:none;
    border-bottom:thin dashed black;
    padding-bottom:0;
    outline:none;
}
</style>