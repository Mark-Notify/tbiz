<?php $this->load->view('session') ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php echo $title ?></title>
    </head>
    <?php $this->load->view($theme.'/style') ?>
    <?php $this->load->view($theme.'/ready') ?>
    <body class="hold-transition sidebar-mini layout-fixed sidebar-collapse">
    <!-- Loading -->
    <div class="loading"></div>

        <div class="wrapper">
            <?php $this->load->view($theme.'/navbar') ?>
            <?php $this->load->view($theme.'/sidebar') ?>
            <div class="content-wrapper">
                <?php $this->load->view($content) ?>
            </div>
        </div>
        <?php $this->load->view($theme.'/footer') ?>
    </body>
    <?php $this->load->view($theme.'/script') ?>
</html>