<!-- Jquery -->
<!-- <script src="<?php echo base_url('assets/plugins/') ?>jquery/js/jquery.min.js"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script> -->
<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
<!-- Jquery UI -->
<script src="<?php echo base_url('assets/plugins/') ?>jquery-ui/js/jquery-ui.min.js"></script>
<!-- Sweetalert2 -->
<script src="<?php echo base_url('assets/plugins/') ?>sweetalert2/js/sweetalert2@9.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url() ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/dataTables.bootstrap4.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/responsive.bootstrap.min.js"></script>
<!-- Datetimepicker -->
<script src="<?php echo base_url() ?>assets/plugins/daterangepicker/moment.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datetimepicker/js/bootstrap-datetimepicker.js"></script>
<!-- Datepicker -->
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/datepicker/datepicker.css">
<!-- arert เล็กๆ มุมขวา -->
<!-- <script src="<?php echo base_url() ?>assets/plugins/toastr/toastr.min.js"></script> -->