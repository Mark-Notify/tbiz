<script>
  $(function() {
    $('.main_data_table').DataTable();

    $('.selectpicker').selectpicker();

    $('.datepicker').datepicker({
        format: 'yyyy-mm-d',
        language: 'th',   
        immediateUpdates: true,
        todayBtn: true,
        todayHighlight: true,
        "setDate": new Date(),
        "autoclose": true
    });
  });

  function main_loading(time, fx) 
  {
    $('.loading').show();
    setTimeout(function(){ 
        fx();
        $('.loading').hide();
    }, time); 
  }

  function main_save(url_save_to,form_id) 
  {
    let return_ = false;
    let formData = new FormData($('#'+form_id)[0]);
    $.ajax({
      type: 'POST',
      url: url_save_to,
      data: formData,
      contentType: false,
      processData: false,
      dataType: 'json',
      async : false,
      success: function(res)
      {
        return_ = res;
      }
    });
    return return_;
  }

  function res_swal(result,reload=0,fx={})
  {
    if((result==false) || (result=='') || (result==0))
    {
      result = {
        type: 'error',
        title: 'แจ้งเตือน',
        text: 'เกิดข้อผิดพลาดโปรดติดต่อเจ้าหน้าที่!',
      }
      reload = 0;
    }
    
    Swal.fire({
      icon:result.type,
      title:result.title,
      text:result.text,
      html:result.html,
      footer:result.footer,
      onClose: () => {
        if(reload==1)
        {
          location.reload();
        }
        else if(reload==2)
        {
          if(result.type == 'success') // ถ้าผิดพลาด ไม่ปิด modal
          {
            $('.modal').modal('hide');
          }
        }
        else if(reload==3)
        {
          if(result.type == 'success') // ถ้าผิดพลาด ไม่ปิด modal
          {
            fx();
            $('.modal').modal('hide');
          }
        }
      }
    });
  }
  
  function main_validated(form_id) 
  {
    let return_ = true;
    $('#'+form_id).addClass('was-validated');

    // $('#'+form_id+' .datepicker').addClass('is-invalid');
    // $('#'+form_id+' .datepicker:required').addClass('is-invalid');

    if(return_)
    {
      $('#'+form_id+' input:text, #'+form_id+' textarea, #'+form_id+' .selectpicker').each(function( index ) {

        if($(this).is(':required') && $(this).val() == '')
        {
          if($(this).hasClass('datepicker'))
          {
              // $(this).addClass('is-invalid');
          }

          $(this).focus();
          return_ = false;
          return false;
        }

      });
    }
    return return_;
  }

  function main_data_to_form(url,id) 
  {
    return_ = 0;
    $.ajax({
        url: url+id,
        dataType: 'json',
        async : false,
        success: function(res)
        {
            return_ = res;
        }
    });
    return return_;
  }

  function cc(str)
  {
    console.log(str);
  }

  function al(str)
  {
    alert(str);
  }

  // ตรวจสอบว่ามีรูปในเครื่องหรือไม่
  function main_file_exist(path) {
    return $.ajax({
      type: "HEAD",
      async: true,
      url: path
    }).done(function(){
      return true;
    }).fail(function () {
      return false;
    })
  }

  function main_post(url, data=null) 
  {
      var return_ = false;
      $.ajax({
          url: url,
          type: 'post',
          data: data,
          dataType: 'json',
          async : false,
          success: function(res)
          {
              return_ = res;
          }
      });
      return return_;
  }

  function mount($mount)
  {
    $array = ["","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"];
    return $array[$mount];
  }

  function sum(input) 
  { 
      if (toString.call(input) !== "[object Array]") 
      return false; 
        
      var total = 0; 
      for(var i=0;i<input.length;i++) 
      {                  
        if(isNaN(input[i])) { 
            continue; 
        } 
        total += Number(input[i]); 
      } 
      return total; 
  } 

  function main_loading(time, fx) 
  {
      $('.loading').show();
      setTimeout(function(){ 
          fx();
          $('.loading').hide();
      }, time); 
  }

  function cc(str)
{
    console.log(str);
}

function al(str)
{
    alert(str);
}

function FC(number) 
{
    number = number.toString();
    number = number.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    return number;
}

function F2C(number) 
{
    number = Number(number)+Number(0.00);
    number = number.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    return number;
}

function DF2C(number) 
{
    number = number.replace(/,/g, '');
    return number;
}

$('input[type="file"]').change(function(e){
  var fileName = e.target.files[0].name;
  $('.custom-file-label').html(fileName);
});

$('.selectpicker_all').selectpicker({
  noneSelectedText: 'กรุณาเลือก',
  selectAllText: 'เลือกทั้งหมด',
  deselectAllText: 'ลบทั้งหมด',
  countSelectedText: function (numSelected, numTotal) {
    return (numSelected == 1) ? "{0} <?php echo label('item_selected') ?>" : "{0} <?php echo label('items_selected') ?>";
  },
});

// เช็ครูปภาพ
function check_image(src = null)
{
  var path = '<?php echo base_url('assets/images/no-image.jpg') ?>';
  if (src == null) 
  {
    return path;
  }
  else
  {
    if (main_file_exist(src)) 
    {
      return '<?php echo base_url() ?>'+src;
    }
    else
    {
      return $path;
    }
  }
}

// ตรวจสอบว่ามีรูปในเครื่องหรือไม่
function main_file_exist(path) {
  return $.ajax({
      type: "HEAD",
      async: true,
      url: path
  }).done(function(){
      return true;
  }).fail(function () {
      return false;
  })
}

function no_image(elm) 
{
  elm.onerror=null;
  elm.src='<?php echo base_url('assets/images/no-image.jpg') ?>';
}

// คลุมดำ text
$('body').on('click','.c_select', function () {
	this.select();
});

// กรอกเลขทศนิยม
$('body').on('keypress keyup blur','.f_number',function (event) {

    $(this).val($(this).val().replace(/[^0-9\.]/g,''));
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }

});

// กรอกเลขจำนวนเต็ม
$('body').on('keypress keyup blur','.i_number',function (event) {

    $(this).val($(this).val().replace(/[^\d].+/, ""));
    if ((event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
});

$(window).bind('keydown', function(event) {
    if (event.ctrlKey || event.metaKey) 
    {
        switch (String.fromCharCode(event.which).toLowerCase()) 
        {
            case 's':
                event.preventDefault();
                $('#input_form .c_save').click();
            break;
            // case 'n':
            //     event.preventDefault();
            //     $('.c_new').click();
            // break;
            // case 'e':
            //     event.preventDefault();
            //     $('.c_exit, .close').click();
            // break;
            // case 'm':
            //     event.preventDefault();
            //     $('.c_menu').click();
            // break;
            // case 'l':
            //     event.preventDefault();
            //     location.href = "<?php echo base_url('Users/Logout') ?>";
            //     // $('.c_logout').click();
            // break;
        }
    }
});
</script>