<!-- Bootstrap -->
<script src="<?php echo base_url('assets/plugins/') ?>bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Fontawesome -->
<script src="<?php echo base_url('assets/plugins/') ?>fontawesome/js/fontawesome.min.js"></script>
<!-- Overlay Scrollbars -->
<script src="<?php echo base_url('assets/plugins/') ?>overlayscrollbars/js/overlayscrollbars.min.js"></script>
<!-- Summernote -->
<script src="<?php echo base_url('assets/plugins/') ?>summernote/js/summernote.min.js"></script>
<!-- Admin LTE -->
<script src="<?php echo base_url('assets/plugins/') ?>admin-lte/dist/js/adminlte.min.js"></script>
<!-- AdminLTE Customize -->
<script src="<?php echo base_url('assets/plugins/') ?>admin-lte/dist/js/demo.js"></script>

<!-- Dashboard -->
<!-- <script src="<?php echo base_url('assets/plugins/') ?>admin-lte/dist/js/pages/dashboard.js"></script> -->
<!-- Datepicker -->
<script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
<script src="<?php echo base_url('assets/plugins/datepicker/locales/bootstrap-datepicker.th.js') ?>"></script>
<!-- Selectpicker -->
<script src="<?php echo base_url('assets/plugins/selectpicker/js/bootstrap-select.js') ?>"></script>
<!-- Main JS -->
<?php $this->load->view('adminLTE/main_js/js') ?>
