<!-- Bootstrap -->
<link rel="stylesheet" href="<?php echo base_url('assets/plugins/') ?>bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?php echo base_url('assets/plugins/') ?>fontawesome/css/fontawesome.min.css">
<!-- Icheck -->
<link rel="stylesheet" href="<?php echo base_url('assets/plugins/') ?>icheck/css/icheck.min.css">
<!-- Switch -->
<link rel="stylesheet" href="<?php echo base_url('assets/plugins/') ?>switch/css/switch.min.css">
<!-- Overlay Scrollbars -->
<link rel="stylesheet" href="<?php echo base_url('assets/plugins/') ?>overlayscrollbars/css/overlayscrollbars.min.css">
<!-- Summernote -->
<link rel="stylesheet" href="<?php echo base_url('assets/plugins/') ?>summernote/css/summernote.min.css">
<!-- Google Font: Sarabun -->
<link href="https://fonts.googleapis.com/css?family=Sarabun" rel="stylesheet">
<!-- Admin LTE -->
<link rel="stylesheet" href="<?php echo base_url('assets/plugins/') ?>admin-lte/dist/css/adminlte.min.css">
<!-- Datetimepicker -->
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/datetimepicker/css/bootstrap-datetimepicker.css">
<!-- Selectpicker -->
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/selectpicker/css/bootstrap-select.css">
<!-- Main CSS -->
<?php $this->load->view('adminLTE/main_css/css') ?>