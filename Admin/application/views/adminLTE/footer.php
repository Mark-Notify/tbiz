<footer class="main-footer">
    <strong>Copyright &copy; 2020 <a href="">T-BIZ</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 1.0
    </div>
</footer>
<aside class="control-sidebar control-sidebar-dark"></aside>