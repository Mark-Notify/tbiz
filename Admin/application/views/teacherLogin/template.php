<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title><?php echo $title ?></title>
	<!-- Main CSS -->
	<?php $this->load->view('teacherLogin/style') ?>
  <?php $this->load->view('adminLTE/ready') ?>
  </head>
  <body>
    <form class="box" id="form_login">
      <h1><?php echo $title ?></h1>
      <input type="text" name="username" id="username" placeholder="Teacher Username" autocomplete="off">
      <input type="password" name="password" id="password" placeholder="Teacher Password" autocomplete="new-password">
      <button type="submit">Login</button>
      <div class="invalid-feedback alert_login text-center">Username or Password is Incorrect!</div>
      </div>
    </form>
  </body>
</html>

<script>
$('#form_login').submit(function(e){

e.preventDefault();

if(validation())
{
    var encoded = encodeURIComponent($('#password').val());
    var password_hash = btoa(encoded);
    var remember = 0;
    if($('#remember').is(':checked')){
        remember = 1;
    }

    $.ajax({
        url: "<?php echo base_url('User/loginTeacherAction') ?>",
        type: "POST",
        data: {
            username : $('#username').val(),
            password : password_hash,
            remember : remember,
        },
        success: function(result){
            if(result==1)
            {
                $('.alert_login').hide();
                $('body').fadeOut('slow', function() {
                    window.parent.location.href = '<?php echo base_url('TeacherDTR') ?>';
                });
            }
            else
            {
                $('#Username').removeClass('is-valid').addClass('is-invalid');
                $('#Password').removeClass('is-valid').addClass('is-invalid').val('');
                $('.alert_login').show();
            }
        },
        error: function (jqXHR, exception) {
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }
            console.log(msg);
        },
    });
}

return false;
});

function validation()
{
let return_ = true;
if($('#Username').val()=='')
{
    return_ = false;
    $('#Username').removeClass('is-valid').addClass('is-invalid');
}
else
{
    $('#Username').removeClass('is-invalid').addClass('is-valid');
}

if($('#Password').val()=='')
{
    return_ = false;
    $('#Password').removeClass('is-valid').addClass('is-invalid');
}
else
{
    $('#Password').removeClass('is-invalid').addClass('is-valid');
}

return return_;
}
</script>