<style>
body{
  margin: 0;
  padding: 0;
  background:url("<?php echo base_url('assets/images/background.jpg') ?>");
  background-size:cover;
  font-family: sans-serif;
  /* background: #34495e; */
}
.box{
  width: 500px;
  padding: 40px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%,-50%);
  background: rgba(0,0,0,0.7);
  text-align: center;
  -webkit-box-shadow: 10px 14px 22px 0px rgba(0,0,0,0.76);
  -moz-box-shadow: 10px 14px 22px 0px rgba(0,0,0,0.76);
  box-shadow: 10px 14px 22px 0px rgba(0,0,0,0.76);
}
.box h1{
  color: white;
  text-transform: uppercase;
  font-weight: 500;
}
.box input[type = "text"],.box input[type = "password"]{
  border:0;
  background: none;
  display: block;
  margin: 20px auto;
  text-align: center;
  border: 2px solid #3498db;
  padding: 14px 10px;
  width: 200px;
  outline: none;
  color: white;
  border-radius: 24px;
  transition: 0.25s;
}
.box input[type = "text"]:focus,.box input[type = "password"]:focus{
  width: 280px;
  border-color: #2ecc71;
}
.box button[type="submit"]{
  border:0;
  background: none;
  display: block;
  margin: 20px auto;
  text-align: center;
  border: 2px solid #2ecc71;
  padding: 14px 40px;
  outline: none;
  color: white;
  border-radius: 24px;
  transition: 0.25s;
  cursor: pointer;
}
.box button[type="submit"]:hover{
  background: #2ecc71;
}
.box button[type="button"]{
  border:0;
  background: none;
  display: block;
  margin: 20px auto;
  text-align: center;
  border: 2px solid #E91E63;
  padding: 14px 40px;
  outline: none;
  color: white;
  border-radius: 24px;
  transition: 0.25s;
  cursor: pointer;
}
.box button[type="button"]:hover{
  background: #E91E63;
}
.alert_login {
  display:none;
  color: red;
  text-shadow: 0 0 5px #FFF, 0 0 10px #FFF, 0 0 15px #FFF, 0 0 20px #49ff18, 0 0 30px #49FF18, 0 0 40px #49FF18, 0 0 55px #49FF18, 0 0 75px #49ff18;
}

</style>