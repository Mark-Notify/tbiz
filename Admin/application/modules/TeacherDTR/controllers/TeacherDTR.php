<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TeacherDTR extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
				
		// check session
        checkTeacherSession();
	}

	public function index()
	{
		$teacher_id = (!empty($_SESSION['user']['teacher_id']))?$_SESSION['user']['teacher_id']:"";
        $date = date('Y-m-1');
        $data['HEAD'] = $this->Main_model->mainGetDB([
            'select'    => '*', 
            'from'      => 'teacher_dtr', 
            'where'     => "t_dtr_teacher_id = $teacher_id AND t_dtr_date = '$date'",
            'result'    => 'row_array'
        ]);
        $h_id = $data['HEAD']['t_dtr_id'];
        $data['DETAIL'] = $this->Main_model->mainGetDB([
            'select'    => '*', 
            'from'      => 'teacher_dtr_detail', 
            'where'     => "t_dtr_id = $h_id",
            'result'    => 'result_array'
        ]);

		$data['school'] = $this->Main_model->mainGetMulti('school','school_status = 1');
		$data['county'] = $this->Main_model->mainGetMulti('county','county_status = 1');
		template('DTR','index', $data, 'adminLTE');
	}

	public function saveInsert()
	{
		$users = $_SESSION['user'];
        $dateTime = mainDateTime();
        $post = $this->input->post();

        // px($users);
		$data = [
			// 't_dtr_id'                  => $post['t_dtr_id'],
            't_dtr_date'                => $post['t_dtr_date'],
            't_dtr_subject'             => $post['t_dtr_subject'],
            't_dtr_teacher_id'          => $post['t_dtr_teacher_id'],
            't_dtr_teacher_code'        => $post['t_dtr_teacher_code'],
            't_dtr_teacher_name'        => $post['t_dtr_teacher_name'],
            't_dtr_school_id'           => $post['t_dtr_school_id'],
            't_dtr_school_name'         => $post['t_dtr_school_name'],
            't_dtr_county_name'         => $post['t_dtr_county_name'],
            't_dtr_county_id'           => $post['t_dtr_county_id'],
            't_dtr_total'               => $post['t_dtr_total'],
            'staff_created_at'      	=> $dateTime,
            'staff_updated_at'      	=> $dateTime,
            'staff_created_id'      	=> $users['teacher_id'],
            'staff_created_name'    	=> $users['teacher_name'],
            'staff_updated_id'      	=> $users['teacher_id'],
            'staff_updated_name'    	=> $users['teacher_name']
        ];
		// px($data);
        $insert_id = $this->Main_model->mainInsert('teacher_dtr', $data, true);
        $detail = [];
        foreach ($post['t_dtr_d_date'] as $key => $value) 
        {
            $detail[] = [
                // 't_dtr_d_id'            => $post['t_dtr_d_id'],
                't_dtr_id'              => $insert_id,
                't_dtr_d_date'          => $post['t_dtr_d_date'][$key],
                't_dtr_d_day'           => $post['t_dtr_d_day'][$key],
                't_dtr_d_time_in'       => $post['t_dtr_d_time_in'][$key],
                't_dtr_d_time_1'        => $post['t_dtr_d_time_1'][$key],
                't_dtr_d_time_2'        => $post['t_dtr_d_time_2'][$key],
                't_dtr_d_time_3'        => $post['t_dtr_d_time_3'][$key],
                't_dtr_d_time_4'        => $post['t_dtr_d_time_4'][$key],
                't_dtr_d_time_5'        => $post['t_dtr_d_time_5'][$key],
                't_dtr_d_time_6'        => $post['t_dtr_d_time_6'][$key],
                't_dtr_d_time_7'        => $post['t_dtr_d_time_7'][$key],
                't_dtr_d_time_out'      => $post['t_dtr_d_time_out'][$key],
                't_dtr_d_time_total'    => $post['t_dtr_d_time_total'][$key]
            ];
        }
        $insert_id = $this->Main_model->mainDelete('teacher_dtr_detail', ['t_dtr_id'=>$insert_id]);
        $insert_id = $this->Main_model->mainInsertMulti('teacher_dtr_detail', $detail);
        

        if($insert_id){
            echo json_encode(array('type' => 'success','title' => 'บันทึกสำเร็จ','text' => 'กดปุ่มเพื่อดำเนินการต่อ'));
        }else{
            echo json_encode(array('type' => 'error','title' => 'บันทึกผิดพลาด','text' => 'โปรดลองใหม่อีกครั้ง'));
        }
	}

	public function saveUpdate()
	{
		$users = $_SESSION['user'];
        $dateTime = mainDateTime();
        $post = $this->input->post();

        // px($users);
		$data = [
			// 't_dtr_id'                  => $post['t_dtr_id'],
            't_dtr_date'                => $post['t_dtr_date'],
            't_dtr_subject'             => $post['t_dtr_subject'],
            't_dtr_teacher_id'          => $post['t_dtr_teacher_id'],
            't_dtr_teacher_code'        => $post['t_dtr_teacher_code'],
            't_dtr_teacher_name'        => $post['t_dtr_teacher_name'],
            't_dtr_school_id'           => $post['t_dtr_school_id'],
            't_dtr_school_name'         => $post['t_dtr_school_name'],
            't_dtr_county_name'         => $post['t_dtr_county_name'],
            't_dtr_county_id'           => $post['t_dtr_county_id'],
            't_dtr_total'               => $post['t_dtr_total'],
            'staff_created_at'      	=> $dateTime,
            'staff_updated_at'      	=> $dateTime,
            'staff_created_id'      	=> $users['teacher_id'],
            'staff_created_name'    	=> $users['teacher_name'],
            'staff_updated_id'      	=> $users['teacher_id'],
            'staff_updated_name'    	=> $users['teacher_name']
        ];
		// px($data);
        $result = $this->Main_model->mainUpdate('teacher_dtr', $data, ['t_dtr_id' => $post['t_dtr_id']]);
        $detail = [];
        foreach ($post['t_dtr_d_date'] as $key => $value) 
        {
            $detail[] = [
                't_dtr_id'              => $post['t_dtr_id'],
                't_dtr_d_date'          => $post['t_dtr_d_date'][$key],
                't_dtr_d_day'           => $post['t_dtr_d_day'][$key],
                't_dtr_d_time_in'       => $post['t_dtr_d_time_in'][$key],
                't_dtr_d_time_1'        => $post['t_dtr_d_time_1'][$key],
                't_dtr_d_time_2'        => $post['t_dtr_d_time_2'][$key],
                't_dtr_d_time_3'        => $post['t_dtr_d_time_3'][$key],
                't_dtr_d_time_4'        => $post['t_dtr_d_time_4'][$key],
                't_dtr_d_time_5'        => $post['t_dtr_d_time_5'][$key],
                't_dtr_d_time_6'        => $post['t_dtr_d_time_6'][$key],
                't_dtr_d_time_7'        => $post['t_dtr_d_time_7'][$key],
                't_dtr_d_time_out'      => $post['t_dtr_d_time_out'][$key],
                't_dtr_d_time_total'    => $post['t_dtr_d_time_total'][$key]
            ];
        }
        // px($detail);
        $result = $this->Main_model->mainDelete('teacher_dtr_detail', ['t_dtr_id'=>$post['t_dtr_id']]);
        $result = $this->Main_model->mainInsertMulti('teacher_dtr_detail', $detail);
        
        if($result){
            echo json_encode(array('type' => 'success','title' => 'บันทึกสำเร็จ','text' => 'กดปุ่มเพื่อดำเนินการต่อ'));
        }else{
            echo json_encode(array('type' => 'error','title' => 'บันทึกผิดพลาด','text' => 'โปรดลองใหม่อีกครั้ง'));
        }
    
	}

	public function getData()
    {
        $school_id = $this->input->post('school_id');
		$teacher_id = (!empty($_SESSION['user']['teacher_id']))?$_SESSION['user']['teacher_id']:"";
        $date = date('Y-m-1');
        $data['HEAD'] = $this->Main_model->mainGetDB([
            'select'    => '*', 
            'from'      => 'teacher_dtr', 
            'where'     => "t_dtr_teacher_id = $teacher_id AND t_dtr_date = '$date' AND t_dtr_school_id = $school_id",
            'result'    => 'row_array'
        ]);
        $h_id = $data['HEAD']['t_dtr_id'];
        $data['DETAIL'] = $this->Main_model->mainGetDB([
            'select'    => '*', 
            'from'      => 'teacher_dtr_detail', 
            'where'     => "t_dtr_id = $h_id",
            'result'    => 'result_array'
        ]);
        echo json_encode($data);
    }
    
    public function changeSchool()
    {
        $county_id = $this->input->post('county_id');
        $data = $this->Main_model->mainGet('county', "county_id = ".$county_id);
		echo json_encode($data);
    }
}
