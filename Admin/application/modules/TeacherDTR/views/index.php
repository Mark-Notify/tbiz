<style>
  .row_center {
    vertical-align : middle !important;
    text-align:center !important;
  }
</style>
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">DTR</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">DTR</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <form id="input_form">
      <div class="row">
        <section class="col-lg-12 connectedSortable">
          <div class="card card-success card-outline">
            <div class="card-header">
              <h3 class="card-title">
                <i class="fas fa-chart-pie mr-1"></i>
                Information
              </h3>
              <!-- <div class="card-tools">
                <button type="button" class="btn btn-primary" onclick="form_add(this)">สร้างข้อมูล</button>
              </div> -->
            </div>
            
            <div class="ml-2 mr-2 pl-2 pr-2">
              <div class="row">

                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4 mb-2 mt-2">
                  <div class="form-group">
                    <label class="l-light" for="t_dtr_school_id">SchoolName</label>
                      <select name="t_dtr_school_id" id="t_dtr_school_id" class="form-control selectpicker show-menu-arrow" data-size="8" data-live-search="true" onchange="get_data_form_school()">
                        <option value="" selected>PleaseSelect</option>
                        <?php
                          if (!empty(school($_SESSION['user']['teacher_school_id']))) 
                          {
                            foreach (school($_SESSION['user']['teacher_school_id']) as $key => $value) 
                            {
                              echo '<option value="'.$value['school_id'].'">'.$value['school_name_en'].'</option>';
                            }
                          }
                        ?>
                      </select>
                  </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4 mb-2 mt-2">
                  <div class="form-group">
                    <label class="l-light" for="t_dtr_subject">Subject</label>
                    <select id="t_dtr_subject" name="t_dtr_subject" class="form-control">
                      <option value="CH" <?php echo ($_SESSION['user']['teacher_text_code'] == "CH") ? "selected" : "CH" ; ?>>China (CH)</option>
                      <option value="EN" <?php echo ($_SESSION['user']['teacher_text_code'] == "EN") ? "selected" : "EN" ; ?>>English (EN)</option>
                      <option value="JP" <?php echo ($_SESSION['user']['teacher_text_code'] == "JP") ? "selected" : "JP" ; ?>>Japanese (JP)</option>
                    </select>
                  </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4 mb-2 mt-2">
                  <div class="form-group">
                    <label class="l-light" for="t_dtr_county_id">District</label>
                    <select name="t_dtr_county_id" id="t_dtr_county_id" class="form-control selectpicker show-menu-arrow" data-size="8"  data-live-search="true">
                      <option value="">PleaseSelect</option>
                      <?php
                        if (!empty(county($_SESSION['user']['teacher_district_id']))) 
                        {
                          foreach (county($_SESSION['user']['teacher_district_id']) as $key => $value) 
                          {
                            if ($key == 0) {
                              echo '<option value="'.$value['county_id'].'" selected>'.$value['county_name_en'].'</option>';
                            }else{
                              echo '<option value="'.$value['county_id'].'">'.$value['county_name_en'].'</option>';
                            }
                          }
                        }
                      ?>
                    </select>
                  </div>
                </div>

              </div>
            </div>

            <div class="card-body">

              <input type="hidden" name="t_dtr_id" id="t_dtr_id" value="<?php echo $HEAD['t_dtr_id'] ?>">
              <input type="hidden" name="t_dtr_date" id="t_dtr_date" value="<?php echo date('Y-m-1') ?>">
              <input type="hidden" name="t_dtr_teacher_id" id="t_dtr_teacher_id" value="<?php echo (!empty($_SESSION['user']['teacher_id']))?$_SESSION['user']['teacher_id']:""; ?>">
              <input type="hidden" name="t_dtr_teacher_code" id="t_dtr_teacher_code" value="<?php echo (!empty($_SESSION['user']['teacher_code']))?$_SESSION['user']['teacher_code']:""; ?>">
              <input type="hidden" name="t_dtr_teacher_name" id="t_dtr_teacher_name" value="<?php echo (!empty($_SESSION['user']['teacher_name']))?$_SESSION['user']['teacher_name']:""; ?>">
              <input type="hidden" name="t_dtr_school_name" id="t_dtr_school_name" value="">
              <input type="hidden" name="t_dtr_county_name" id="t_dtr_county_name" value="">
              <input type="hidden" name="t_dtr_total" id="t_dtr_total" value="">

              <div class="row">
                <div class="table-responsive">
                  <table class="table table-bordered table-striped">
                    <thead>
                      <tr align="center">
                        <th colspan="3"><label class="l-light">Teacher Code : </label> <?php echo (!empty($_SESSION['user']['teacher_code']))?$_SESSION['user']['teacher_code']:""; ?></th>  
                        <th colspan="7">(Hr.Week,ชม./สัปดาห์)</th>    
                        <th colspan="3"><label class="l-light">Teacher's name : </label> <?php echo (!empty($_SESSION['user']['teacher_name']))?$_SESSION['user']['teacher_name']:""; ?></th>
                      </tr>
                      <tr align="center">
                        <th rowspan="3" class="row_center">Date</th>
                        <th rowspan="3" class="row_center">Day</th>
                        <th rowspan="3" class="row_center">Time In</th>
                        <th colspan="10">Time</th>
                      </tr>
                      <tr align="center">
                        <th>8:30</th>
                        <th>9:30</th>
                        <th>10:30</th>
                        <th rowspan="2" class="row_center">LunchBreak</th>
                        <th>12:30</th>
                        <th>13:30</th>
                        <th>14:30</th>
                        <th>15:30</th>
                        <th rowspan="2" class="row_center">Time Out</th>
                        <th rowspan="2" class="row_center">Hour/Day</th>
                      </tr>
                      <tr align="center">
                        <th>9:30</th>
                        <th>10:30</th>
                        <th>11:30</th>
                        <!-- <th>LunchBreak</th> -->
                        <th>13:30</th>
                        <th>14:30</th>
                        <th>15:30</th>
                        <th>16:30</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                        $j= 1;
                        for ($i=0; $i <= 30; $i++) { 
                      ?>
                      <tr align="center">
                        <td><?php echo $j ?><input type="hidden" name="t_dtr_d_date[]" id="t_dtr_d_date_<?php echo $i ?>" value="<?php echo $j ?>"></td>
                        <td><input type="text" class="form-control clear text-center t_dtr_d_day" name="t_dtr_d_day[]" value=""></td>
                        <td><input type="time" class="form-control clear text-center t_dtr_d_time_in" name="t_dtr_d_time_in[]" value=""></td>
                        <td><input type="text" class="form-control clear text-center t_dtr_d_time_1" name="t_dtr_d_time_1[]" value=""></td>
                        <td><input type="text" class="form-control clear text-center t_dtr_d_time_2" name="t_dtr_d_time_2[]" value=""></td>
                        <td><input type="text" class="form-control clear text-center t_dtr_d_time_3" name="t_dtr_d_time_3[]" value=""></td>
                        <td></td> <!-- Lunch Break -->
                        <td><input type="text" class="form-control clear text-center t_dtr_d_time_4" name="t_dtr_d_time_4[]" value=""></td>
                        <td><input type="text" class="form-control clear text-center t_dtr_d_time_5" name="t_dtr_d_time_5[]" value=""></td>
                        <td><input type="text" class="form-control clear text-center t_dtr_d_time_6" name="t_dtr_d_time_6[]" value=""></td>
                        <td><input type="text" class="form-control clear text-center t_dtr_d_time_7" name="t_dtr_d_time_7[]" value=""></td>
                        <td><input type="time" class="form-control clear text-center t_dtr_d_time_out" name="t_dtr_d_time_out[]" value=""></td>
                        <td><input type="text" class="form-control clear text-center cal_hour i_number c_select t_dtr_d_time_total" name="t_dtr_d_time_total[]" value="" onkeyup="cal_hour()"></td>
                      </tr>
                      <?php 
                          $j++;
                        } 
                      ?>
                      <tr>
                        <td colspan="10" align="right">Total</td>
                        <td colspan="3" align="center" id="total"><?php echo $HEAD['t_dtr_total'] ?>  Hour/Month</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

            </div>
            
            <div class="card-footer clearfix">
              <button type="button" class="btn btn-success float-right c_save" onclick="form_save()"><i class="fas fa-check"></i> บันทึก</button>
            </div>
          </div>
        </section>
      </div>
    </form>
  </div>
</section>

<?php echo $this->load->view('jquery') ?>