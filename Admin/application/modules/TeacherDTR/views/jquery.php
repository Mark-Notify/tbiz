<script>
$(function () {
  $('#datetimepicker3').datetimepicker({
      format: 'LT'
  });
});
	function cal_hour() 
  {
    var total = 0;
    $('.cal_hour').each(function (index, value) { 
          
      var number = $(this).val();
      total += Number(number)
    });
    $('#total').text(total+" Hour/Month");
    $('#t_dtr_total').val(total);
  }

  function form_save()
  {
    set_data();
    main_loading(500, function(){
      var head_id = $('#t_dtr_id').val();
      if(main_validated('input_form'))
      {
        if((head_id==0) || (head_id==''))//เพิ่ม
        {
          var res = main_save('<?php echo base_url('TeacherDTR/saveInsert') ?>', 'input_form');
          res_swal(res, 1);
        }
        else//แก้ไข
        {
          var res = main_save('<?php echo base_url('TeacherDTR/saveUpdate') ?>', 'input_form');
          res_swal(res, 1);
        }
      }
    });
  }

  function set_data() 
  {
    var school = $('#t_dtr_school_id option:selected').text();
    var county = $('#t_dtr_county_id option:selected').text();
    
    $('#t_dtr_school_name').val(school);
    $('#t_dtr_county_name').val(county);
  }

  function get_data_form_school() 
  {
    $('.clear').val("");
    var school_id = $('#t_dtr_school_id option:selected').val();
    var res = main_post('<?php echo base_url('TeacherDTR/getData') ?>',{school_id:school_id});
    // cc(res)
    $.each(res.HEAD, function (key, val) { 
       $('#'+key).val(val);
    });
    $.each(res.DETAIL, function (key, val) { 
      var tr = $('#t_dtr_d_date_'+key).closest('tr');
      $.each(val, function (k, v) { 
        if (k == 't_dtr_d_time_total' && v == 0) {
          $(tr).find('.'+k).val("");
        }else{
          $(tr).find('.'+k).val(v).keyup();
        }
      });
    });
  }

</script>