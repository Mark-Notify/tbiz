<?php
require_once FCPATH . '/vendor/autoload.php';

use Fpdf\Fpdf;

class PDF extends Fpdf
{
    function Header()
    {
        
        // $path = FCPATH."uploads/company/logo/2020-04-09_1586404119876.png";
        // $this->Image($path,10,6,30);
        $this->SetY(0);
        $this->SetFont('THSarabun','B',14);
        $this->Cell(0, 7, iconv('UTF-8','TIS-620', ""), 0, 1, 'C');
        $this->Cell(0, 7, iconv('UTF-8','TIS-620', ""), 0, 1, 'C');
        $this->Cell(0, 7, iconv('UTF-8','TIS-620','รายงานการรับเงินรายเดือน'), 0, 1, 'C');
        $this->SetFont('THSarabun','B',12);
        $this->Cell(100, 4, iconv('UTF-8','TIS-620','บันทึกข้อมูลโดย'), 0, 0, 'R');
        $this->Cell(30, 4, iconv('UTF-8','TIS-620',''), 'B', 0, 'C');
        $this->Cell(10, 4, iconv('UTF-8','TIS-620','วันที่'), 0, 0, 'C');
        $this->Cell(30, 4, iconv('UTF-8','TIS-620',''), 'B', 0, 'C');
        $this->Cell(10, 4, iconv('UTF-8','TIS-620','เวลา'), 0, 0, 'C');
        $this->Cell(30, 4, iconv('UTF-8','TIS-620',''), 'B', 1, 'C');
        $this->Ln(5);

    }

    function Footer()
    {
        // Go to 1.5 cm from bottom
        $this->SetY(-15);
        // Select Arial italic 8
        $this->SetFont('THSarabun','I',8);
        // Print centered page number
        $this->Cell(0,10,'Page '.$this->PageNo().' / {nb}',0,0,'R');
    }
}

$fpdf = new PDF('L');

$fpdf->AddThaiFont();
$fpdf->AddPage();
$fpdf->SetTitle('Test Title');
$fpdf->AliasNbPages();
$fpdf->SetFont('THSarabun','B',14);

$w = [8,15,15,30,20,25,12,15,15,15,15,15,20,15,15,15];
$fpdf->Cell($w[0], 5, iconv('UTF-8','TIS-620',''), 'LT', 0, 'C');
$fpdf->Cell($w[1], 5, iconv('UTF-8','TIS-620',''), 'LT', 0, 'C');
$fpdf->Cell($w[2], 5, iconv('UTF-8','TIS-620',''), 'LT', 0, 'C');
$fpdf->Cell($w[3], 5, iconv('UTF-8','TIS-620','ชื่อ'), 'LT', 0, 'C');
$fpdf->Cell($w[4], 5, iconv('UTF-8','TIS-620',''), 'LT', 0, 'C');
$fpdf->Cell($w[5], 5, iconv('UTF-8','TIS-620',''), 'LT', 0, 'C');
$fpdf->Cell($w[6], 5, iconv('UTF-8','TIS-620','ชม.'), 'LT', 0, 'C');
$fpdf->Cell($w[7]+20, 5, iconv('UTF-8','TIS-620','เวลาทำงานจริงของครู'), 'LTB', 0, 'C');
// $fpdf->Cell($w[8], 5, iconv('UTF-8','TIS-620',''), 'LT', 0, 'C');
$fpdf->Cell($w[9], 5, iconv('UTF-8','TIS-620','รวม'), 'LT', 0, 'C');
$fpdf->Cell($w[10], 5, iconv('UTF-8','TIS-620',''), 'LT', 0, 'C');
$fpdf->Cell($w[11], 5, iconv('UTF-8','TIS-620',''), 'T', 0, 'C');
$fpdf->Cell($w[12], 5, iconv('UTF-8','TIS-620',''), 'LT', 0, 'C');
$fpdf->Cell($w[13], 5, iconv('UTF-8','TIS-620',''), 'LT', 0, 'C');
$fpdf->Cell($w[14], 5, iconv('UTF-8','TIS-620',''), 'T', 0, 'C');
$fpdf->Cell($w[15], 5, iconv('UTF-8','TIS-620',''), 'LTR', 1, 'C');

$fpdf->Cell($w[0], 5, iconv('UTF-8','TIS-620','#'), 'LR', 0, 'C');
$fpdf->Cell($w[1], 5, iconv('UTF-8','TIS-620','วันที่'), 'R', 0, 'C');
$fpdf->Cell($w[2], 5, iconv('UTF-8','TIS-620','เขต'), 'R', 0, 'C');
$fpdf->Cell($w[3], 5, iconv('UTF-8','TIS-620','โรงเรียน'), 'R', 0, 'C');
$fpdf->Cell($w[4], 5, iconv('UTF-8','TIS-620','รหัสครู'), 'R', 0, 'C');
$fpdf->Cell($w[5], 5, iconv('UTF-8','TIS-620','ชื่อครู'), 'R', 0, 'C');
$fpdf->Cell($w[6], 5, iconv('UTF-8','TIS-620','ที่วางบิล'), 'R', 0, 'C');
$fpdf->Cell($w[7]+5, 5, iconv('UTF-8','TIS-620','ชั่วโมงทำงาน'), 'RB', 0, 'C');
$fpdf->Cell($w[8], 5, iconv('UTF-8','TIS-620','ชม.ครูลา'), 'RB', 0, 'C');
$fpdf->Cell($w[9], 5, iconv('UTF-8','TIS-620','DTR+SUB'), 'R', 0, 'C');
$fpdf->Cell($w[10]+15, 5, iconv('UTF-8','TIS-620','รับจากโรงเรียน'), 'R', 0, 'C');
// $fpdf->Cell($w[11], 5, iconv('UTF-8','TIS-620',''), 'R', 0, 'C');
$fpdf->Cell($w[12], 5, iconv('UTF-8','TIS-620','ชื่อผู้รับ'), 'R', 0, 'C');
$fpdf->Cell($w[13]+15, 5, iconv('UTF-8','TIS-620','นำฝาก'), 'R', 0, 'C');
// $fpdf->Cell($w[14], 5, iconv('UTF-8','TIS-620',''), 'R', 0, 'C');
$fpdf->Cell($w[15], 5, iconv('UTF-8','TIS-620','หมายเหตุ'), 'R', 1, 'C');

$fpdf->Cell($w[0], 5, iconv('UTF-8','TIS-620',''), 'LB', 0, 'C');
$fpdf->Cell($w[1], 5, iconv('UTF-8','TIS-620',''), 'LB', 0, 'C');
$fpdf->Cell($w[2], 5, iconv('UTF-8','TIS-620',''), 'LB', 0, 'C');
$fpdf->Cell($w[3], 5, iconv('UTF-8','TIS-620',''), 'LB', 0, 'C');
$fpdf->Cell($w[4], 5, iconv('UTF-8','TIS-620',''), 'LB', 0, 'C');
$fpdf->Cell($w[5], 5, iconv('UTF-8','TIS-620',''), 'LB', 0, 'C');
$fpdf->Cell($w[6], 5, iconv('UTF-8','TIS-620',''), 'LB', 0, 'C');
$fpdf->Cell($w[7]+5, 5, iconv('UTF-8','TIS-620','DTR'), 'LB', 0, 'C');
$fpdf->Cell($w[8], 5, iconv('UTF-8','TIS-620','SUB'), 'LB', 0, 'C');
$fpdf->Cell($w[9], 5, iconv('UTF-8','TIS-620',''), 'LB', 0, 'C');
$fpdf->Cell($w[10], 5, iconv('UTF-8','TIS-620','ชม.'), 'TLB', 0, 'C');
$fpdf->Cell($w[11], 5, iconv('UTF-8','TIS-620','จำนวนเงิน'), 'TLB', 0, 'C');
$fpdf->Cell($w[12], 5, iconv('UTF-8','TIS-620',''), 'LB', 0, 'C');
$fpdf->Cell($w[13], 5, iconv('UTF-8','TIS-620','ธนาคาร'), 'TLB', 0, 'C');
$fpdf->Cell($w[14], 5, iconv('UTF-8','TIS-620','เลขที่บัญชี'), 'TLB', 0, 'C');
$fpdf->Cell($w[15], 5, iconv('UTF-8','TIS-620',''), 'LBR', 1, 'C');

// $fpdf->MultiCell(277,11, iconv('UTF-8','TIS-620','รายงานการรับเงินรายเดือน'),1,'C',0);

$fpdf->Output();