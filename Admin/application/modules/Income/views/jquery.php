<script>
$(function() {
  main_loading(500, function(){

    var data = main_post('<?php echo base_url('Income/getDataSearch') ?>');
    set_table(data['table']);
    set_info(data['info']);
  });
});

function form_setting() 
{
	$('#input_model_setting').modal('show');
}

function form_add(elm)
{
  form_reset();
  $('#input_model').modal('show');
  $('#input_model #head_modal').html($(elm).html());
}

function form_save()
{
	set_data();
  main_loading(500, function(){
    var head_id = $('#income_id').val();
    if(main_validated('input_form'))
    {
      if((head_id==0) || (head_id==''))//เพิ่ม
      {
        var res = main_save('<?php echo base_url('Income/saveInsert') ?>', 'input_form');
        res_swal(res, 2);
      }
      else//แก้ไข
      {
        var res = main_save('<?php echo base_url('Income/saveUpdate') ?>', 'input_form');
        res_swal(res, 2);
      }
      get_data_search();
    }
  });
}

function form_edit(id) 
{
  form_reset();

  $('#input_model').modal('show');
  $('#head_modal').html('<i class="fas fa-folder-plus"></i> แก้ไขข้อมูล');

  main_loading(500, function(){
    form_insert_data(id);
  });

	//   $id = $('#income_id').val();
	//   if ($id != 0) {
	// 	$('.no_edit').prop('readonly',true);
	//   }else{
	// 	$('.no_edit').prop('readonly',false);
	//   }
}

function set_data() 
{
  $('#income_teacher_name').val($('#income_teacher_id option:selected').text());
  $('#income_county_name').val($('#income_county_id option:selected').text());
  $('#income_school_name').val($('#income_school_id option:selected').text());
}

  // นำเข้าข้อมูลไปใส่ฟอร์ม
function form_insert_data(id) 
{
  var data = main_data_to_form('<?php echo base_url('Income/getData/') ?>', id);
//   cc(data)
  $.each(data, function(key, val) {

    if (key == 'income_petition') 
    {
		if (val != "") {
			var path = '<?php echo base_url('uploads/income/') ?>'+id+'/petition/'+val;
		}else{
			var path = '<?php echo base_url() ?>/assets/images/no-image.jpg'
		}
      	$('#income_petition_image').attr('src',path);
    }
    else if (key == 'income_deposit_slip') 
    {
	  	if (val != "") {
			var path = '<?php echo base_url('uploads/income/') ?>'+id+'/slip/'+val;
		}else{
			var path = '<?php echo base_url() ?>/assets/images/no-image.jpg'
		}
      	$('#income_deposit_slip_image').attr('src',path);
    }
    else
    {
      $('#'+key).val(val);
    }
  });

  $('.selectpicker').selectpicker('refresh');
}

function form_reset() 
{
  // หนึงรายการ
  $('#input_form')[0].reset();  // เคลียร์ฟอร์ม
  $('#input_form').removeClass('was-validated');
  $('#income_id').val(0); // ไอดี 0
}

function cal_billing_price() 
{
  // ราคาวางบิล
  var bill_hours = $('#income_billing_hours').val();
  var bill_money = $('#income_billing_money').val();

  // จำนวนเงินที่รับมา
  var hours = $('#income_real_hours').val();
  var money = $('#income_real_money').val();

  var total_hours = Number(bill_hours)-Number(hours);
  var total_money = Number(bill_money)-Number(money);
  $('#income_accrual_hours').val(F2C(total_hours));
  $('#income_accrual_money').val(F2C(total_money));
}

function cal_time() 
{
  var dtr = $('#income_working_hours').val();
  var sub = $('#income_hours_leave').val();
  var total = Number(dtr)+Number(sub);
  $('#income_hours_total').val(F2C(total));
}

function get_data_search() 
{
  var data = {
    teacher:$('#search_income_teacher_id option:selected').val(),
    school:$('#search_income_school_id option:selected').val(),
    month:$('#input_search_month option:selected').val(),
    year:$('#input_search_year option:selected').val(),
    accrual:$('#search_accrual_money:checked').val(),
    receive:$('#search_receive_name:checked').val(),
  }
  var data = main_post('<?php echo base_url('Income/getDataSearch') ?>',data);
  update_table(data['table'])
  set_info(data['info']);
}

function set_table(data) 
{  
	DATA_1 = $('#table_income').DataTable({
		data: data,
		order: [[ 0, "asc" ]],
		columns: [
			{ 
				width: "5%", 
				className: "text-center",
				orderable: false
			},
			{ 
				width: "10%", 
				className: "text-center",
			},
			{ 
				width: "10%", 
				className: "text-left",
			},
			{ 
				width: "10%", 
				className: "text-left",
			},
			{ 
				width: "8%", 
				className: "text-center",
			},
			{ 
				width: "10%", 
				className: "text-center",
			},
			{ 
				width: "8%", 
				className: "text-center",
			},
			{ 
				width: "8%", 
				className: "text-center",
			},
			{ 
				width: "8%", 
				className: "text-center",
			},
			{ 
				width: "5%", 
				className: "text-center",
			},
			{ 
				width: "10%", 
				className: "text-center",
			}
		],
		pageLength: 10,
		// pagingType: "numbers",
		// dom: 'plrtip',
		// searching: false,
		lengthMenu: [10,25,50,100],
		oLanguage: {
			sSearchPlaceholder: "ค้นหา",
			sLengthMenu: "แสดง _MENU_ รายการ",
			sSearch: "ค้นหา",
			sInfo: "กำลังแสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
			sInfoEmpty: "กำลังแสดง 0 ถึง 0 จาก 0 รายการ",
			sZeroRecords: "ไม่มีข้อมูลในตาราง",
			sProcessing: "กำลังค้นหา",
		}
	});
}

function update_table(data)
{
	DATA_1.clear();
	DATA_1.rows.add(data);
	DATA_1.draw();
}

function set_info(data) 
{
  $('#balance_money').text(F2C(data['accrual'])+" บาท");
  $('#total_receive').text(F2C(data['receive'])+" บาท");
  $('#total_hours').text(F2C(data['real_hours'])+" ชั่วโมง");
  $('#total_money').text(F2C(data['billing_money'])+" บาท");
}

function change_bank_name()
{
	var number 	= $('#income_bank_id option:selected').data('number');
	var name 	= $('#income_bank_id option:selected').data('name');

	$('#income_bank_number').val(number);
	$('#income_bank_name').val(name);
}

function change_county()
{
	var id 	= $('#income_county_id option:selected').val();
	var res = main_post('<?php echo base_url('Income/changeCounty') ?>',{id:id});
	var option = "";
	$.each(res, function (index, value) {
		option += '<option value="'+value.school_id+'">'+value.school_name+'</option>';		
	});
	$('#income_school_id').html(option).selectpicker('refresh');
}

function change_school() 
{
	var id 	= $('#income_school_id option:selected').val();
	var res = main_post('<?php echo base_url('Income/changeSchool') ?>',{id:id});
	var option = '';
	$.each(res, function (index, value) {
		option += '<option value="'+value.teacher_id+'" data-code="'+value.teacher_code+'" data-subject="'+value.teacher_text_code+'" data-price="'+value.teacher_school_price+'">'+value.teacher_name+'</option>';		
	});
	$('#income_teacher_id').html(option).selectpicker('refresh');
}

function change_teacher()
{
	var code = $('#income_teacher_id option:selected').data('code');
	$('#income_teacher_code').val(code);
}

function cal_price_billing() 
{
	var hours 		= $('#income_billing_hours').val();
	var school 		= $('#income_teacher_id option:selected').data('price');
	
	if (school == '0.00') {
		var code = $('#income_teacher_code').val();
		toastr.warning('ครูรหัส '+code+' ไม่ได้มีการตั้งค่าจำนวนเงิน');
		Swal.fire({
			position: 'top-end',
			icon: 'error',
			title: 'ครูรหัส '+code+' ไม่ได้มีการตั้งค่าจำนวนเงิน',
			showConfirmButton: false,
			timer: 1500
		});
	}
	var price 		= Number(hours)*Number(school);
	$('#income_billing_money').val(F2C(price));
	$('#income_accrual_hours').val(hours)
	$('#income_accrual_money').val(F2C(price))
}

function show_image(input,path_show) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#'+path_show).attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

</script>