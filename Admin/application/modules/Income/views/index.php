<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6 d-flex">
          <h1 class="m-0 text-dark font-weight-bold">บัญชีรายรับ</h1>
          <button type="button" class="btn btn-outline-primary ml-2" onclick="form_setting()"  title="ตั้งค่า"><i class="fas fa-cogs"></i></button>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">บัญชีรายรับ</li>
          </ol>
        </div>
      </div>
    </div>
  </div>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <section class="col-lg-12 connectedSortable">
        <div class="card card-success card-outline">
          <div class="card-header">
            <h3 class="card-title">
              <i class="fas fa-chart-pie mr-1"></i>
              ข้อมูล
            </h3>
            <div class="card-tools">
              <button type="button" class="btn btn-primary" onclick="form_add(this)">สร้างข้อมูล</button>
            </div>

          </div>
          
          <div class="ml-2 mr-2 pl-2 pr-2">
            <div class="row">

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-2 mb-2 mt-2">
                <div class="form-group">
                  <label class="l-light" for="input_search_year"> ปีที่วางบิล</label>
                  <select name="input_search_year" id="input_search_year" onchange="get_data_search()" class="form-control selectpicker show-menu-arrow" data-size="8"  data-live-search="true" title="ปี">
                    <?php 
                      $y = (int)date('Y');
                      $y_pre = 2018;
                      $y_next = (int)date('Y')+10;
                      for ($i=$y_pre; $i < $y_next; $i++) 
                      { 
                        echo '<option value="'.$i.'">'.$i.'</option>';
                      }
                    ?>
                  </select>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-2 mb-2 mt-2">
                <div class="form-group">
                  <label class="l-light" for="input_search_month">เดือนที่วางบิล</label>
                  <select name="input_search_month" id="input_search_month" onchange="get_data_search()" class="form-control selectpicker show-menu-arrow" data-size="8" data-live-search="true" title="เดือน">
                    <?php
                      for ($i=0; $i <= 12 ; $i++) { 
                          echo '<option value="'.$i.'">'.month($i).'</option>';                        
                      }
                    ?>
                  </select>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-2 mb-2 mt-2">
                <div class="form-group">
                  <label class="l-light" for="search_income_teacher_id">ชื่อครู</label>
                    <select name="search_income_teacher_id" id="search_income_teacher_id" class="form-control selectpicker show-menu-arrow" data-size="8" data-live-search="true" onchange="get_data_search()">
                      <option value="">กรุณาเลือกชื่อครู</option>
                      <?php
                        if (!empty($teacher)) 
                        {
                          foreach ($teacher as $key => $value) 
                          {
                            echo '<option value="'.$value['teacher_id'].'">'.$value['teacher_name'].'</option>';
                          }
                        }
                      ?>
                    </select>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-2 mb-2 mt-2">
                <div class="form-group">
                  <label class="l-light" for="search_income_school_id">ชื่อโรงเรียน</label>
                    <select name="search_income_school_id" id="search_income_school_id" class="form-control selectpicker show-menu-arrow" data-size="8" data-live-search="true" onchange="get_data_search()">
                      <option value="">กรุณาเลือกชื่อโรงเรียน</option>
                      <?php
                        if (!empty($school)) 
                        {
                          foreach ($school as $key => $value) 
                          {
                            echo '<option value="'.$key.'">'.$value.'</option>';
                          }
                        }
                      ?>
                    </select>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-2 mb-2 mt-2">
                <label class="l-light">อื่นๆ </label>
                <div class="form-group mt-2">
                  <div class="icheck-success">
                      <input type="checkbox" id="search_accrual_money" name="search_accrual_money" value="1" onclick="get_data_search()">
                      <label class="l-light" for="search_accrual_money"> จำนวนเงินคงค้าง</label>
                  </div>

                  <div class="icheck-success">
                      <input type="checkbox" id="search_receive_name" name="search_receive_name" value="1" onclick="get_data_search()">
                      <label class="l-light" for="search_receive_name"> รายการที่ยังไม่มีการชำระ</label>
                  </div>

                </div>
              </div>
                        

            </div>

            <div class="row">
            
              <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                  <span class="info-box-icon bg-success elevation-1"><i class="fas fa-dollar-sign"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">จำนวนเงินคงค้างรวม</span>
                    <span class="info-box-number" id="balance_money">0</span>
                  </div>
                </div>
              </div>

              <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                  <span class="info-box-icon bg-success elevation-1"><i class="fas fa-dollar-sign"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">ยอดรับเงินรวม</span>
                    <span class="info-box-number" id="total_receive">0</span>
                  </div>
                </div>
              </div>

              <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                  <span class="info-box-icon bg-success elevation-1"><i class="far fa-clock"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">จำนวนชั่วโมงทั้งหมด</span>
                    <span class="info-box-number" id="total_hours">0</span>
                  </div>
                </div>
              </div>

              <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                  <span class="info-box-icon bg-success elevation-1"><i class="fas fa-dollar-sign"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">จำนวนเงินที่วางบิลทั้งหมด</span>
                    <span class="info-box-number" id="total_money">0</span>
                  </div>
                </div>
              </div>

            </div>

          </div>
            

          <div class="card-body">

            <div class="row">
              <div class="table-responsive">
                <table class="table table-bordered table-striped" id="table_income">
                  <thead>
                    <tr align="center">
                      <th>#</th>
                      <th width="10%">รหัสครู</th>
                      <th width="10%">ชื่อครู</th>
                      <th width="10%">ชื่อโรงเรียน</th>
                      <th width="10%">เดือนที่วางบิล</th>
                      <th width="10%">การวางบิล<br> (จำนวน ชม. / ราคา)</th>
                      <th width="10%">เวลาทำงานจริง</th>
                      <th width="10%">เวลาทำงานรับจากโรงเรียน</th>
                      <th width="10%">ชื่อผู้รับเงิน</th>
                      <th width="10%">ชม.คงค้าง</th>
                      <th width="10%">จำนวนเงินคงค้าง</th>
                    </tr>
                  </thead>
                  <tbody>
                  
                  </tbody>
                </table>
              </div>
            </div>

          </div>
          
        </div>
      </section>
      
    </div>
  </div>
</section>

<?php echo $this->load->view('modal') ?>
<?php echo $this->load->view('modal_setting') ?>
<?php echo $this->load->view('jquery') ?>