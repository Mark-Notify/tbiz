<div class="modal fade" id="input_model">
  <div class="modal-dialog modal-xxl modal-dialog-centered" role="document">
    <form id="input_form">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="head_modal">สร้างข้อมูล</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          
          <fieldset class="border-danger mb-2">
            <legend>ใบวางบิล</legend>
            <div class="row p-2">
              
              <input type="hidden" name="income_id" id="income_id" value="0">
              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="staff_created_at">วันที่สร้างข้อมูล</label>
                  <input type="text" class="form-control text-center datepicker" id="staff_created_at" name="staff_created_at" value="<?php echo date('Y-m-d H:i:s') ?>" readonly>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="income_billing_year"> ปีที่วางบิล</label>
                  <select name="income_billing_year" id="income_billing_year" class="form-control selectpicker show-menu-arrow" data-size="10"  data-live-search="true" title="กรุณาเลือกปี">
                    <!-- <option value="" selected>$y_next</option> -->
                    <?php 
                      $y = (int)date('Y');
                      $y_pre = 2018;
                      $y_next = (int)date('Y')+10;
                      for ($i=$y_pre; $i < $y_next; $i++) 
                      { 
                        if ($i == $y) 
                        {
                          echo '<option value="'.$i.'" selected>'.$i.'</option>';
                        }
                        else
                        {
                          echo '<option value="'.$i.'">'.$i.'</option>';
                        }
                      }
                    ?>
                  </select>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="income_billing_month">เดือนที่วางบิล</label>
                  <select name="income_billing_month" id="income_billing_month" class="form-control selectpicker show-menu-arrow" data-size="8"  data-live-search="true">
                    <?php
                      for ($i=0; $i <= 12 ; $i++) 
                      { 
                        if ($i == date('m')) 
                        {
                          echo '<option value="'.$i.'" selected>'.month($i).'</option>';
                        }
                        else
                        {
                          echo '<option value="'.$i.'">'.month($i).'</option>'; 
                        }                       
                      }
                    ?>
                  </select>
                </div>
              </div>
              
              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="income_date_in">วันที่เข้าสอน</label>
                  <input type="text" class="form-control datepicker" id="income_date_in" name="income_date_in">
                </div>
              </div>
              
              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="income_date_out">วันที่สิ้นสุดการสอน</label>
                  <input type="text" class="form-control datepicker" id="income_date_out" name="income_date_out">
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <input type="hidden" name="income_county_name" id="income_county_name">
                  <label class="l-light" for="income_county_id">เขต <span style="color:red">*</span></label>
                  <select name="income_county_id" id="income_county_id" class="form-control selectpicker show-menu-arrow"  data-size="8"  data-live-search="true" required onchange="change_county()">
                    <option value="">กรุณาเลือกเขต</option>
                    <?php
                      if (!empty($county)) 
                      {
                        foreach ($county as $key => $value) 
                        {
                          echo '<option value="'.$key.'">'.$value.'</option>';
                        }
                      }
                    ?>
                  </select>                      
                  <div class="valid-feedback">สามารถใช้งานได้</div>
                  <div class="invalid-feedback">กรุณากรอกข้อมูล</div>              
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <input type="hidden" name="income_school_name" id="income_school_name">
                  <label class="l-light" for="income_school_id">ชื่อโรงเรียน <span style="color:red">*</span></label>
                  <select name="income_school_id" id="income_school_id" class="form-control selectpicker show-menu-arrow" data-size="8"  data-live-search="true" required title="กรุณาเลือกโรงเรียน" onchange="change_school()">
                    <option value="">กรุณาเลือกโรงเรียน</option>
                    <?php
                      if (!empty($st_school)) 
                      {
                        foreach ($st_school as $key => $value) 
                        {
                          echo '<option value="'.$value['school_id'].'" data-price_ch="'.$value['school_tuition_ch'].'" data-price_en="'.$value['school_tuition_en'].'" data-price_jp="'.$value['school_tuition_jp'].'"></option>'.$value['school_name'].'</option>';
                        }
                      }
                    ?>
                  </select>                       
                  <div class="valid-feedback">สามารถใช้งานได้</div>
                  <div class="invalid-feedback">กรุณากรอกข้อมูล</div>             
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <input type="hidden" name="income_teacher_subject" value="">
                  <label class="l-light" for="income_teacher_code"> รหัสครู</label>
                  <input type="text" class="form-control text-center" id="income_teacher_code" name="income_teacher_code" readonly>
                </div>
              </div>
              
              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <input type="hidden" name="income_teacher_name" id="income_teacher_name">
                  <label class="l-light" for="income_teacher_id">ชื่อครู <span style="color:red">*</span></label>
                    <select name="income_teacher_id" id="income_teacher_id" class="form-control selectpicker show-menu-arrow" data-size="8" data-live-search="true" required onchange="change_teacher()" title="กรุณาเลือกชื่อครู">
                    <option value="">กรุณาเลือกชื่อครู</option>
                      <?php
                        if (!empty($teacher)) 
                        {
                          foreach ($teacher as $key => $value) 
                          {
                            echo '<option value="'.$value['teacher_id'].'" data-code="'.$value['teacher_code'].'" data-subject="'.$value['teacher_text_code'].'" data-price="'.$value['teacher_school_price'].'">'.$value['teacher_name'].'</option>';
                          }
                        }
                      ?>
                    </select>                  
                  <div class="valid-feedback">สามารถใช้งานได้</div>
                  <div class="invalid-feedback">กรุณากรอกข้อมูล</div>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="income_billing_hours">ชม.ที่วางบิล</label>
                  <input type="text" class="form-control text-center" id="income_billing_hours" name="income_billing_hours" onkeyup="cal_price_billing()">
                </div>
              </div>

              <!-- <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="income_billing_money_hours">จำนวนเงิน/ชั่วโมง</label>
                  <input type="text" class="form-control text-center" id="income_billing_money_hours" name="income_billing_money_hours" readonly>
                </div>
              </div> -->

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="income_billing_money">จำนวนเงิน</label>
                  <input type="text" class="form-control text-center" id="income_billing_money" name="income_billing_money" readonly>
                </div>
              </div>

            </div>
          </fieldset>

          <fieldset class="border-danger mb-2">
            <legend>รายงานการรับเงิน</legend>
            <div class="row p-2">
            
              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="income_working_hours">เวลาทำงานจริงของครู</label>
                  <div class="input-group">
                    <input type="text" class="form-control text-center" id="income_working_hours" name="income_working_hours" onkeyup="cal_time()" placeholder="DTR">
                    <input type="text" class="form-control text-center" id="income_hours_leave" name="income_hours_leave" onkeyup="cal_time()" placeholder="SUB">
                  </div>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="income_hours_total">รวม (DTR+SUB)</label>
                  <input type="text" class="form-control text-center" id="income_hours_total" name="income_hours_total" readonly>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="income_real_hours">รับจากโรงเรียน</label>
                  <div class="input-group">
                    <input type="text" class="form-control text-center" id="income_real_hours" name="income_real_hours" onkeyup="cal_billing_price()" placeholder="ชม.">
                    <input type="text" class="form-control text-center" id="income_real_money" name="income_real_money" onkeyup="cal_billing_price()" placeholder="จำนวนเงิน">
                  </div>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="income_accrual_hours">จำนวนค้างรับ</label>
                    <div class="input-group">
                      <input type="text" class="form-control text-center" readonly id="income_accrual_hours" name="income_accrual_hours" placeholder="ชม.">
                      <input type="text" class="form-control text-center" readonly id="income_accrual_money" name="income_accrual_money" placeholder="จำนวนเงิน">
                    </div>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="income_receive_name">ชื่อผู้รับ</label>
                  <input type="text" class="form-control" id="income_receive_name" name="income_receive_name">
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="income_bank_id"> เลือกธนาคาร</label>
                  <select name="income_bank_id" id="income_bank_id" class="form-control no_edit" onchange="change_bank_name()" title="กรุณาเลือกธนาคาร">
                    <option value="">กรุณาเลือกธนาคาร</option>
                    <option value="1" data-number="735-2649-557" data-name="ธ.กสิกร">บช.โรงเรียน (ธ.กสิกร)</option>
                    <option value="2" data-number="569-0177-563" data-name="ธ.กรุงไทย">บช.โรงเรียน (ธ.กรุงไทย)</option>
                    <option value="3" data-number="195-0013-836" data-name="ธ.กรุงไทย">บช.โรงเรียน (ธ.กรุงไทย)</option>
                    <option value="4" data-number="983-422-4591" data-name="ธ.กรุงไทย">บช.บริษัท (ธ.กรุงไทย)</option>
                    <option value="5" data-number="131-4076-017" data-name="ธ.กรุงเทพ">บช.ศศิภัสส์ศา (ธ.กรุงเทพ)</option>
                  </select>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="income_bank_name">ธนาคาร</label>
                  <input type="text" class="form-control no_edit" id="income_bank_name" name="income_bank_name" readonly>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="income_bank_number">เลขบัญชี</label>
                  <input type="text" class="form-control no_edit" id="income_bank_number" name="income_bank_number" readonly>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="income_petition">ฎีกา</label>
                  <input type="file" id="income_petition" name="income_petition" style="display:none" onchange="show_image(this,'income_petition_image')">
                  <img src="<?php echo base_url('assets/images/no-image.jpg') ?>" accept="image/*" id="income_petition_image" width="100%" onclick="$('#income_petition').click()">
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="income_deposit_slip">ใบฝาก</label>
                  <input type="file" class="form-control" id="income_deposit_slip" name="income_deposit_slip" style="display:none" onchange="show_image(this,'income_deposit_slip_image')">
                  <img src="<?php echo base_url('assets/images/no-image.jpg') ?>" accept="image/*" id="income_deposit_slip_image" width="100%">
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="income_deposit_date">วันที่รับเงิน</label>
                  <input type="text" class="form-control datepicker" id="income_deposit_date" name="income_deposit_date">
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="income_remark">หมายเหตุ</label>
                  <textarea id="income_remark" name="income_remark" class="form-control"></textarea>
                </div>
              </div>
              
            </div>
          </fieldset>

        </div>
            
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
          <div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="fas fa-user-check"></i>
                </span>
              </div>
              <input type="text" class="form-control text-center" name="staff_created_name" id="staff_created_name" placeholder="" readonly>
            </div>
          </div>
          <button type="button" class="btn btn-success" onclick="form_save()">บันทึก</button>
        </div>
      </div>
    </form>
  </div>
</div>