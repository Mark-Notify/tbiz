<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Income extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
				
		// check session
		checkSession();
		
		// load model
		// $this->load->model('Option_model');
        // load library
		// $this->load->library('UploadsFile');
	}

	public function index()
	{
		$data['teacher'] = $this->Option_model->optionTeacher();
		$data['school'] = $this->Option_model->optionSchool();
		$data['county'] = $this->Option_model->optionCounty();
		$data['income'] = $this->Main_model->mainGetMulti('income','income_status = 1','income_id DESC');
		$data['st_school'] = $this->Main_model->mainGetMulti('school','school_status = 1');
		template('บัญชีรายรับ','index', $data, 'adminLTE');
	}

	public function saveInsert()
	{
		$users = ssUser();
        $dateTime = mainDateTime();
		$post = $this->input->post();

		$data = array(
			// 'income_id' 			=> $post['income_id'],
			// 'create_date' 			=> $post['create_date'],
			'income_date_in' 		=> $post['income_date_in'],
			'income_date_out' 		=> $post['income_date_out'],
			'income_billing_year' 	=> $post['income_billing_year'],
			'income_billing_month' 	=> $post['income_billing_month'],
			'income_county_id' 		=> $post['income_county_id'],
			'income_county_name' 	=> $post['income_county_name'],
			'income_school_id' 		=> $post['income_school_id'],
			'income_school_name' 	=> $post['income_school_name'],
			'income_teacher_id' 	=> $post['income_teacher_id'],
			'income_teacher_name' 	=> $post['income_teacher_name'],
			'income_teacher_code' 	=> $post['income_teacher_code'],
			'income_billing_hours' 	=> $post['income_billing_hours'],
			'income_billing_money' 	=> DF2C($post['income_billing_money']),
			'income_working_hours' 	=> $post['income_working_hours'],
			'income_hours_leave' 	=> $post['income_hours_leave'],
			'income_hours_total' 	=> $post['income_hours_total'],
			'income_real_hours' 	=> $post['income_real_hours'],
			'income_real_money' 	=> DF2C($post['income_real_money']),
			'income_accrual_hours' 	=> $post['income_accrual_hours'],
			'income_accrual_money' 	=> DF2C($post['income_accrual_money']),
			'income_receive_name' 	=> $post['income_receive_name'],
			'income_bank_id' 		=> $post['income_bank_id'],
			'income_bank_name' 		=> $post['income_bank_name'],
			'income_bank_number' 	=> $post['income_bank_number'],
			// 'income_petition' 		=> $post['income_petition'],
			// 'income_deposit_slip' 	=> $post['income_deposit_slip'],
			'income_remark' 		=> $post['income_remark'],
            'staff_created_at'      => $dateTime,
            'staff_updated_at'      => $dateTime,
            'staff_created_id'      => $users['employee_id'],
            'staff_created_name'    => $users['employee_username'],
            'staff_updated_id'      => $users['employee_id'],
            'staff_updated_name'    => $users['employee_username']
		);
		
		$income_id = $this->Main_model->mainInsert('income', $data, TRUE);
		// เพิ่มรูปถ่าย
        if (isset($_FILES['income_petition']['name']) && $_FILES['income_petition']['name'] != '') 
        {
            $path = FCPATH ."uploads/income/$income_id/petition/";
            $file = $_FILES['income_petition'];
            $income_petition = $this->uploadsfile->fileUpload($file,$path);
            $this->Main_model->mainUpdate('income', ['income_petition' => $income_petition], ['income_id' => $income_id]);
		}

		// เพิ่มรูปถ่าย
        if (isset($_FILES['income_deposit_slip']['name']) && $_FILES['income_deposit_slip']['name'] != '') 
        {
            $path = FCPATH ."uploads/income/$income_id/slip/";
            $file = $_FILES['income_deposit_slip'];
            $income_deposit_slip = $this->uploadsfile->fileUpload($file,$path);
            $this->Main_model->mainUpdate('income', ['income_deposit_slip' => $income_deposit_slip], ['income_id' => $income_id]);
		}
		
		if($income_id) {
            echo json_encode(array('type' => 'success','title' => 'บันทึกสำเร็จ','text' => 'กดปุ่มเพื่อดำเนินการต่อ'));
        }else {
            echo json_encode(array('type' => 'error','title' => 'บันทึกผิดพลาด','text' => 'โปรดลองใหม่อีกครั้ง'));
        }
	}

	public function saveUpdate()
	{
		$users = ssUser();
        $dateTime = mainDateTime();
		$post = $this->input->post();

		$data = array(
			// 'income_id' 			=> $post['income_id'],
			// 'create_date' 			=> $post['create_date'],
			'income_date_in' 		=> $post['income_date_in'],
			'income_date_out' 		=> $post['income_date_out'],
			'income_billing_year' 	=> $post['income_billing_year'],
			'income_billing_month' 	=> $post['income_billing_month'],
			'income_county_id' 		=> $post['income_county_id'],
			'income_county_name' 	=> $post['income_county_name'],
			'income_school_id' 		=> $post['income_school_id'],
			'income_school_name' 	=> $post['income_school_name'],
			'income_teacher_id' 	=> $post['income_teacher_id'],
			'income_teacher_name' 	=> $post['income_teacher_name'],
			'income_teacher_code' 	=> $post['income_teacher_code'],
			'income_billing_hours' 	=> $post['income_billing_hours'],
			'income_billing_money' 	=> DF2C($post['income_billing_money']),
			'income_working_hours' 	=> $post['income_working_hours'],
			'income_hours_leave' 	=> $post['income_hours_leave'],
			'income_hours_total' 	=> $post['income_hours_total'],
			'income_real_hours' 	=> $post['income_real_hours'],
			'income_real_money' 	=> DF2C($post['income_real_money']),
			'income_accrual_hours' 	=> $post['income_accrual_hours'],
			'income_accrual_money' 	=> DF2C($post['income_accrual_money']),
			'income_receive_name' 	=> $post['income_receive_name'],
			'income_bank_id' 		=> $post['income_bank_id'],
			'income_bank_name' 		=> $post['income_bank_name'],
			'income_bank_number' 	=> $post['income_bank_number'],
			// 'income_petition' 		=> $post['income_petition'],
			// 'income_deposit_slip' 	=> $post['income_deposit_slip'],
			'income_remark' 		=> $post['income_remark'],
            // 'staff_created_at'      => $dateTime,
            'staff_updated_at'      => $dateTime,
            // 'staff_created_id'      => $users['employee_id'],
            // 'staff_created_name'    => $users['employee_username'],
            'staff_updated_id'      => $users['employee_id'],
            'staff_updated_name'    => $users['employee_username']
		);
		
		$result = $this->Main_model->mainUpdate('income', $data, 'income_id = '.$post['income_id']);

		// เพิ่มรูปถ่าย
        if (isset($_FILES['income_petition']['name']) && $_FILES['income_petition']['name'] != '') 
        {
			$path = FCPATH ."uploads/income/$post[income_id]/petition/";
			rmInFolder($path);
            $file = $_FILES['income_petition'];
            $income_petition = $this->uploadsfile->fileUpload($file,$path);
            $this->Main_model->mainUpdate('income', ['income_petition' => $income_petition], ['income_id' => $post['income_id']]);
		}

		// เพิ่มรูปถ่าย
        if (isset($_FILES['income_deposit_slip']['name']) && $_FILES['income_deposit_slip']['name'] != '') 
        {
			$path = FCPATH ."uploads/income/$post[income_id]/slip/";
			rmInFolder($path);
            $file = $_FILES['income_deposit_slip'];
            $income_deposit_slip = $this->uploadsfile->fileUpload($file,$path);
            $this->Main_model->mainUpdate('income', ['income_deposit_slip' => $income_deposit_slip], ['income_id' => $post['income_id']]);
		}

		if($result) {
            echo json_encode(array('type' => 'success','title' => 'บันทึกสำเร็จ','text' => 'กดปุ่มเพื่อดำเนินการต่อ'));
        }else {
            echo json_encode(array('type' => 'error','title' => 'บันทึกผิดพลาด','text' => 'โปรดลองใหม่อีกครั้ง'));
        }
	}

	public function getData($id)
	{
		$data = $this->Main_model->mainGet('income', 'income_id = '.$id);
		echo json_encode($data);
		}
		
		public function Report()
		{
			$this->load->view('fpdf');
	}
  
	public function getDataSearch()
	{
		$post = $this->input->post();
		// px($post);
		$str_where = "";
		$where_arr[] = "income_status = 1";

		if (!empty($post['accrual'])) 
		{
			$where_arr[] = "income_accrual_money != 0";
		}

		if (!empty($post['year']) && $post['year'] != "0") 
		{
			$where_arr[] = "income_billing_year = $post[year]";
		}

		if (!empty($post['month']) && $post['month'] != "0") 
		{
			$where_arr[] = "income_billing_month = $post[month]";
		}
		
		if (!empty($post['teacher'])) 
		{
			$where_arr[] = "income_teacher_id = $post[teacher]";
		}

		if (!empty($post['school'])) 
		{
			$where_arr[] = "income_school_id = $post[school]";
		}

		if (!empty($post['receive'])) 
		{
			$where_arr[] = "income_receive_name = ''";
		}
		
		if (count($where_arr) > 0) 
		{
			$str_where = implode(" AND " , $where_arr);
		}

		$data = $this->Main_model->mainGetMulti('income',$str_where,'income_id DESC');
		// pp_sql();
		$real_hours = [];
		$billing_money = [];
		$accrual = [];
		$receive = [];
		$reslut['table'] = [];
		$num_tr = 1;

		foreach ($data as $key => $rows) {
			$real_hours[] += (int)$rows['income_real_hours'];
			$billing_money[] += (int)$rows['income_billing_money'];
			$accrual[] += (int)$rows['income_accrual_money'];
			$receive[] += (int)$rows['income_billing_money']-(int)$rows['income_accrual_money'];
			$reslut['table'][] = [
				$num_tr, 
				'<a class="have-click" onclick="form_edit('.$rows['income_id'].')">'.$rows['income_teacher_code'].'</a>',  
				$rows['income_teacher_name'], 
				$rows['income_school_name'], 
				month($rows['income_billing_month']),
				$rows['income_billing_hours']." / ".F2C($rows['income_billing_money']), 
				$rows['income_hours_total'], 
				$rows['income_real_hours'], 
				$rows['income_receive_name'],
				$rows['income_accrual_hours'],
				F2C($rows['income_accrual_money'])
			];
			$num_tr++;
		}
		// pp($accrual);
		$reslut['info']['accrual'] = array_sum($accrual);
		$reslut['info']['receive'] = array_sum($receive);
		$reslut['info']['real_hours'] = array_sum($real_hours);
		$reslut['info']['billing_money'] = array_sum($billing_money);
		echo json_encode($reslut);
	}

	public function changeCounty()
	{
		$post = $this->input->post();
		$data = $this->Main_model->mainGetMulti('school', "school_county_id = ".$post['id']);
		echo json_encode($data);
	}

	public function changeSchool()
	{
		$post = $this->input->post();
		$data = $this->Main_model->mainGetMulti('teacher', 'teacher_school_id LIKE \''.'%\"'.$post['id'].'\"%\'');
		echo json_encode($data);
	}

	public function savePrice()
	{
		$post = $this->input->post();
		foreach ($post['school_tuition_ch'] as $key => $value) 
		{
			$data = [
				'school_tuition_ch' => $post['school_tuition_ch'][$key],
				'school_tuition_en' => $post['school_tuition_en'][$key],
				'school_tuition_jp' => $post['school_tuition_jp'][$key],
			];
			$result = $this->Main_model->mainUpdate('school', $data, ['school_id' => $post['school_id_price'][$key]]);
		}
		
		if($result) {
            echo json_encode(array('type' => 'success','title' => 'บันทึกสำเร็จ','text' => 'กดปุ่มเพื่อดำเนินการต่อ'));
        }else {
            echo json_encode(array('type' => 'error','title' => 'บันทึกผิดพลาด','text' => 'โปรดลองใหม่อีกครั้ง'));
        }
	}
}
