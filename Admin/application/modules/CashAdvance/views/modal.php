<div class="modal fade" id="input_model">
  <div class="modal-dialog modal-xxl modal-dialog-centered" role="document">
    <form id="input_form">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="head_modal">Create</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          
          <fieldset class="border-danger mb-2">
            <legend>Cash Advance</legend>
            <div class="row p-2">
              
            <input type="hidden" name="ca_id" id="ca_id" value="">

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <input type="hidden" name="ca_school_name" id="ca_school_name" value="">
                <div class="form-group">
                  <label class="l-light" for="ca_school_id"> School name</label>
                  <select name="ca_school_id" id="ca_school_id" class="form-control selectpicker_all  show-menu-arrow" data-size="8" data-live-search="true" title="PleaseSelect">
                    <?php
                      if (!empty($school)) 
                      {
                        foreach ($school as $key => $value) 
                        {
                          echo '<option value="'.$value['school_id'].'">'.$value['school_name'].'</option>';
                        }
                      }
                    ?>
                  </select>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="ca_date">Date</label>
                  <input type="text" class="form-control datepicker text-center" id="ca_date" name="ca_date" value="<?php echo date('Y-m-d') ?>" readonly>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <input type="hidden" name="ca_teacher_name" id="ca_teacher_name">
                  <label class="l-light" for="ca_teacher_id">Teacher</label>
                    <select name="ca_teacher_id" id="ca_teacher_id" class="form-control selectpicker show-menu-arrow" data-size="8" data-live-search="true" onchange="get_data_search()">
                      <option value="">PleaseSelect</option>
                      <?php
                        if (!empty($teacher)) 
                        {
                          foreach ($teacher as $key => $value) 
                          {
                            echo '<option value="'.$value['teacher_id'].'">'.$value['teacher_name'].'</option>';
                          }
                        }
                      ?>
                    </select>
                </div>
              </div>
              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="ca_passport">Passport Number</label>
                  <input type="text" class="form-control" id="ca_passport" name="ca_passport" value="" onkeyup="key_passport()">
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="ca_address">Address</label>
                  <textarea name="ca_address" id="ca_address" cols="" rows="" class="form-control"></textarea>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                  <label class="l-light" for="ca_amount">Amount</label>
                <div class="input-group mb-3">
                  <input type="text" class="form-control f_number text-center" id="ca_amount" name="ca_amount" value="">
                  <div class="input-group-append">
                    <span class="input-group-text">Bath</span>
                  </div>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-6 mb-2">
                <div class="form-group">
                  <label class="l-light" for="ca_amount_text">Amount in words</label>
                  <input type="text" class="form-control" id="ca_amount_text" name="ca_amount_text" value="">
                </div>
              </div>
              
              <div class="col-12 d-inline w-100 text-center mb-3 mt-3">
                <span style="background-color: white;"></span>
                <hr style="margin: -10px 0px 15px;">
              </div>

              <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-2">
                <label class="l-light" for="">Reasons For Requesting Withdrawal</label>
                <div class="form-group clearfix">
                  <div class="icheck-success d-inline">
                    <input type="checkbox" id="ca_reason_1_id" name="ca_reason_1" value="1">
                    <label for="ca_reason_1_id"><span style="min-width: 230px;">Pay Visa Extension Free </span> <input type="text" class="dotted text-center mr-2" name="ca_reason_1_price" id="ca_reason_1_price"> Bath</label>
                  </div>
                </div>

                <div class="form-group clearfix">
                  <div class="icheck-success d-inline">
                    <input type="checkbox" id="ca_reason_2_id" name="ca_reason_2" value="1">
                    <label for="ca_reason_2_id"><span style="min-width: 230px;">Pay Visa work Permit Free </span><input type="text" class="dotted text-center mr-2" name="ca_reason_2_price" id="ca_reason_2_price"> Bath</label>
                  </div>
                </div>

                <div class="form-group clearfix">
                  <div class="icheck-success d-inline">
                    <input type="checkbox" id="ca_reason_3_id" name="ca_reason_3" value="1">
                    <label for="ca_reason_3_id"><span style="min-width: 230px;">Pay NON-B Visa Free </span><input type="text" class="dotted text-center mr-2" name="ca_reason_3_price" id="ca_reason_3_price"> Bath</label>
                  </div>
                </div>

                <div class="form-group clearfix">
                  <div class="icheck-success d-inline">
                    <input type="checkbox" id="ca_reason_4_id" name="ca_reason_4" value="1">
                    <label for="ca_reason_4_id"><span style="min-width: 230px;">Pay Apartment Bill Free </span><input type="text" class="dotted text-center mr-2" name="ca_reason_4_price" id="ca_reason_4_price"> Bath</label>
                  </div>
                </div>

                <div class="form-group clearfix">
                  <div class="icheck-success d-inline">
                    <input type="checkbox" id="ca_reason_5_id" name="ca_reason_5" value="1">
                    <label for="ca_reason_5_id">
                      <span style="min-width: 230px;">(<input type="text" class="dotted" name="ca_reason_5_text" id="ca_reason_5_text">)</span> 
                      <input type="text" class="dotted text-center mr-2" name="ca_reason_5_price" id="ca_reason_5_price"> Bath
                    </label>
                  </div>
                </div>

                <div class="form-group">
                  <span style="min-width: 230px;margin-left: 146px;">Total Amount = </span>
                  <input type="text" class="dotted text-center mr-2" id="ca_total" name="ca_total" value=""> Bath
                </div>

                <div class="form-group">
                  <span>I leave a copy of my passport no.</span>
                  <input type="text" class="dotted text-center mr-2" id="ca_passport_2" name="ca_passport_2" value="">
                  as proof. I will pay the said Amount of money back on 
                  <input type="text" class="dotted datepicker text-center mr-2" id="ca_date_money_back" name="ca_date_money_back" value="<?php echo date('Y-m-d') ?>">
                  to the "Lender" After I have received my salary.
                </div>

                <div class="text-center">
                  <input type="text" class="dotted text-center" id="ca_name_borrow" name="ca_name_borrow" value="" placeholder="Signature"><br>
                  Borrower                
                </div>
                
              </div>

              <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-2">

                <div class="form-group clearfix">
                  <div class="icheck-success d-inline">
                    <input type="radio" id="ca_transfer_1" name="ca_transfer" value="1" checked>
                    <label for="ca_transfer_1"><span style="min-width: 230px;">1. Pay through bank account. Please enter account No. </span>
                      <input type="text" class="dotted text-center mr-2" name="ca_bank_code" id="ca_bank_code"> 
                      Bank Name
                      <input type="text" class="dotted text-center mr-2" name="ca_bank_name" id="ca_bank_name"> 
                    </label>
                  </div>
                </div>

                <div class="form-group clearfix">
                  <div class="icheck-success d-inline">
                    <input type="radio" id="ca_transfer_2" name="ca_transfer" value="2">
                    <label for="ca_transfer_2">2. Pick Cash
                      Your Contact Number.
                      <input type="text" class="dotted text-center mr-2" name="ca_contact" id="ca_contact"> 
                    </label>
                  </div>
                </div>

              </div>

              <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-2">
                <div class="callout callout-success">
                  <h5>Note.</h5>
                  <p>Fill out the form 11,12,13 receive your money on the 15 of the mounth Only</p>
                  <p>Fill out the form 21,22,23 receive your money on the 25 of the mounth Only</p>
                </div>
              </div>

            </div>
          </fieldset>

        </div>
            
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
          <div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="fas fa-user-check"></i>
                </span>
              </div>
              <input type="text" class="form-control text-center" name="staff_created_name" id="staff_created_name" placeholder="" readonly>
            </div>
          </div>
          <button type="button" class="btn btn-success" onclick="form_save()">บันทึก</button>
        </div>
      </div>
    </form>
  </div>
</div>