<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('User_model');
	}

	public function index()
	{
		$data['user'] = "";
		template('Login','index', $data, 'adminLogin');
    }
    
    public function TeacherLogin()
	{
		$data['user'] = "";
		template('Login for Teacher','index', $data, 'teacherLogin');
	}

	// เข้าสู่ระบบ
    public function loginAction()
    {
		// px($this->input->post());
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        // $remember = $this->input->post('remember');

        // $password = base64_decode($password);
        $password = urldecode(base64_decode($password));

        $password_hash = myPassword($password);
        $row_login = $this->User_model->checkLogin($username,$password_hash);
        if(is_array($row_login) && count($row_login))
        {
            $this->setSession($row_login);
            // $this->setOnline();

            //จำรหัสผ่าน 30 วัน
            // if($remember == 1)
            // {
            //     $day = 30;
            //     $time = $day * 24 * 60 * 60 * 1000;
            //     $cookieEncode = base64_encode("MA".base64_encode($username)."CtEaSnSt".base64_encode($password)."RK");
            //     set_cookie('cceososktiaent', $cookieEncode, $time);
            // }
            
            echo 1;
        }
        else
        {
            echo 0;
        }
    }

    // เข้าสู่ระบบ
    public function loginTeacherAction()
    {
		// px($this->input->post());
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        // $remember = $this->input->post('remember');

        $password = base64_decode($password);
        $password = urldecode($password);

        $password_hash = myPassword($password);
        $row_login = $this->User_model->checkTeacherLogin($username,$password_hash);

        if(is_array($row_login) && count($row_login))
        {
            $this->setSession($row_login);
            echo 1;
        }
        else
        {
            echo 0;
        }
    }
    
    // ออกจากระบบ
    public function logout()
    {
        // $this->setOffline();
        // $this->session->unset_userdata('users');
        // $this->session->unset_userdata('time_active');
        $this->session->sess_destroy();
        delete_cookie('cceososktiaent');
		redirect('/User', 'refresh');
	}
	
	// เพิ่ม Session 
    private function setSession($user)
    {
        unset($user['employee_password']);
        unset($user['teacher_password']);
        $this->session->set_userdata(['user'=>$user]);
    }
}
