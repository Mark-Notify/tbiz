<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_model{
    
    public function checkLogin($user,$pass)
    {
        $this->db->select('*');
        $this->db->where("employee_username",$user);
        $this->db->where("employee_password",$pass);
        $this->db->where("employee_status != '2'");
        $query = $this->db->get('employee');
        $result = $query->row_array();
        // pp_sql();
        return $result;
    }

    public function checkTeacherLogin($user,$pass)
    {
        $result = $this->db->where("teacher_code = '$user' AND teacher_password = '$pass' AND teacher_status = '1'")->get('teacher');
        return $result->row_array();
    }
}