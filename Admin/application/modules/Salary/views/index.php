<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">โปรแกรมเงินเดือน</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">โปรแกรมเงินเดือน</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <form id="">
      <div class="row">
        <section class="col-lg-12 connectedSortable">
          <div class="card card-success card-outline">
            <div class="card-header">
              <h3 class="card-title">
                <i class="fas fa-chart-pie mr-1"></i>
                ข้อมูล
              </h3>
              <div class="card-tools">
                <button type="button" class="btn btn-primary" onclick="form_add(this)">สร้างข้อมูล</button>
              </div>

            </div>
            
            <div class="ml-2 mr-2 pl-2 pr-2">
              <div class="row">
              <?php //pp(school()) ?>

                <div class="col-sm-12 col-md-6 col-lg-3 col-xl-3 mb-2">
                  <div class="form-group">
                    <label class="l-light" for="search_salary_month">เดือนที่วางบิล</label>
                    <select name="search_salary_month" id="search_salary_month" onchange="get_data_search()" class="form-control selectpicker show-menu-arrow" data-size="8" data-live-search="true" title="เดือน">
                      <?php
                        for ($i=0; $i <= 12 ; $i++) { 
                          if ($i == date('m')) {
                            echo '<option value="'.$i.'" selected>'.month($i).'</option>';  
                          }else{
                            echo '<option value="'.$i.'">'.month($i).'</option>';  
                          }
                                                
                        }
                      ?>
                    </select>
                  </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-3 col-xl-3 mb-2">
                  <div class="form-group">
                    <input type="hidden" name="search_salary_county_name" id="search_salary_county_name">
                    <label class="l-light" for="search_salary_county_id">เขต </label>
                    <select name="search_salary_county_id" id="search_salary_county_id" class="form-control selectpicker show-menu-arrow"  data-size="8"  data-live-search="true" required onchange="get_data_search()">
                      <option value="">กรุณาเลือกเขต</option>
                      <?php
                        if (!empty($county)) 
                        {
                          foreach ($county as $key => $value) 
                          {
                            echo '<option value="'.$key.'">'.$value.'</option>';
                          }
                        }
                      ?>
                    </select>                      
                    <div class="valid-feedback">สามารถใช้งานได้</div>
                    <div class="invalid-feedback">กรุณากรอกข้อมูล</div>              
                  </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-3 col-xl-3 mb-2">
                  <div class="form-group">
                    <input type="hidden" name="search_salary_school_name" id="search_salary_school_name">
                    <label class="l-light" for="search_salary_school_id">ชื่อโรงเรียน </label>
                    <select name="search_salary_school_id" id="search_salary_school_id" class="form-control selectpicker show-menu-arrow" data-size="8"  data-live-search="true" required title="กรุณาเลือกโรงเรียน" onchange="get_data_search()">
                      <option value="">กรุณาเลือกโรงเรียน</option>
                      <?php
                        if (!empty($school)) 
                        {
                          foreach ($school as $key => $value) 
                          {
                            echo '<option value="'.$value['school_id'].'" data-price="'.$value['school_tuition_fee'].'">'.$value['school_name'].'</option>';
                          }
                        }
                      ?>
                    </select>                       
                    <div class="valid-feedback">สามารถใช้งานได้</div>
                    <div class="invalid-feedback">กรุณากรอกข้อมูล</div>             
                  </div>
                </div>
                
                <div class="col-sm-12 col-md-6 col-lg-3 col-xl-3 mb-2">
                  <div class="form-group">
                    <input type="hidden" name="search_salary_teacher_name" id="search_salary_teacher_name">
                    <label class="l-light" for="search_salary_teacher_id">ชื่อครู </label>
                      <select name="search_salary_teacher_id" id="search_salary_teacher_id" class="form-control selectpicker show-menu-arrow" data-size="8" data-live-search="true" required title="กรุณาเลือกชื่อครู">
                      <option value="">กรุณาเลือกชื่อครู</option>
                        <?php
                          if (!empty($teacher)) 
                          {
                            foreach ($teacher as $key => $value) 
                            {
                              echo '<option value="'.$value['teacher_id'].'" data-code="'.$value['teacher_code'].'">'.$value['teacher_name'].'</option>';
                            }
                          }
                        ?>
                      </select>                  
                    <div class="valid-feedback">สามารถใช้งานได้</div>
                    <div class="invalid-feedback">กรุณากรอกข้อมูล</div>
                  </div>
                </div>

              </div>
            </div>

            <div class="card-body">

              <div class="row">
                <div class="table-responsive">
                  <table class="table table-bordered table-striped" id="table_salary">
                    <thead>
                      <tr align="center">
                        <th>#</th>
                        <th width="10%">รหัสครู</th>
                        <th width="10%">ชื่อครู</th>
                        <th width="10%">ชื่อโรงเรียน</th>
                        <th width="10%">ชื่อเขต</th>
                        <th width="10%">เงินเดือน</th>
                        <th width="10%">ยอดหักทั้งหมด</th>
                        <th width="10%">คงเหลือ</th>
                      </tr>
                    </thead>
                    <tbody>
                    
                    </tbody>
                  </table>
                </div>
              </div>

            </div>
            
            <div class="card-footer clearfix">
              <!-- <button type="button" class="btn btn-success float-right c_save" onclick="form_save()"><i class="fas fa-check"></i> บันทึก</button> -->
            </div>
          </div>
        </section>
      </div>
    </form>
  </div>
</section>

<?php echo $this->load->view('modal') ?>
<?php echo $this->load->view('jquery') ?>