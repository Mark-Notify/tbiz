<script>
$(function() {
  main_loading(500, function(){
	var data = {
		month:$('#search_salary_month option:selected').val(),
		county:$('#search_salary_county_id option:selected').val(),
		school:$('#search_salary_school_id option:selected').val(),
		teacher:$('#search_salary_teacher_id option:selected').val(),
	}
    var data = main_post('<?php echo base_url('Salary/getDataSearch') ?>',data);
    set_table(data['table']);
  });
})

function form_add(elm)
{
  form_reset();
  $('#input_model').modal('show');
  $('#teacher_text_code').change();
  $('#input_model #head_modal').html($(elm).html());
}

function form_save()
{
  main_loading(500, function(){
	set_data();
    var head_id = $('#salary_id').val();
    if(true)
    {
      if((head_id==0) || (head_id==''))//เพิ่ม
      {
        var res = main_save('<?php echo base_url('Salary/saveInsert') ?>','input_form');
        res_swal(res, 2);
      }
      else//แก้ไข
      {
        var res = main_save('<?php echo base_url('Salary/saveUpdate') ?>','input_form');
        res_swal(res, 2);
      }
      get_data_search();
    }
  });
}

function set_data() 
{
	$('#salary_county_name').val($('#salary_county_id option:selected').text());
	$('#salary_school_name').val($('#salary_school_id option:selected').text());
	$('#salary_teacher_name').val($('#salary_teacher_id option:selected').text());
}

function form_edit(id) 
{
  form_reset();

  $('#input_model').modal('show');
  $('#head_modal').html('<i class="fas fa-folder-plus"></i> แก้ไขข้อมูล');

  main_loading(500, function(){
	form_insert_data(id);
	get_data_teacher();
  });
}

// นำเข้าข้อมูลไปใส่ฟอร์ม
function form_insert_data(id) 
{
  var data = main_data_to_form('<?php echo base_url('Salary/getData/') ?>', id);
  $.each(data, function(key, val) {
	$('#'+key).val(val);
  });
	//   cc(data.detail)
  for (let i = 0; i < data.detail.length; i++) 
  {
	if (data.detail[i].salary_d_cash_advance == 0) {
		data.detail[i].salary_d_cash_advance = "";
	}
	if (data.detail[i].salary_d_sub == 0) {
		data.detail[i].salary_d_sub = "";
	}
	if (data.detail[i].salary_d_other == 0) {
		data.detail[i].salary_d_other = "";
	}
	if (data.detail[i].salary_d_sub_total == 0) {
		data.detail[i].salary_d_sub_total = "";
	}
	$('#salary_d_cash_advance_'+i).val(data.detail[i].salary_d_cash_advance);
	$('#salary_d_sub_'+i).val(data.detail[i].salary_d_sub);
	$('#salary_d_other_'+i).val(data.detail[i].salary_d_other);
	$('#salary_d_sub_total_'+i).val(data.detail[i].salary_d_sub_total);
  }
  
  
  $('.selectpicker').selectpicker('refresh');
}

function form_reset() 
{
  // หนึงรายการ
  $('#input_form')[0].reset();  // เคลียร์ฟอร์ม
  $('#input_form').removeClass('was-validated');
  $('#salary_id').val(0); // ไอดี 0
  $('#tbody_clone').html("");
  $('.selectpicker').selectpicker('refresh');
}

function get_data_search() 
{
  var data = {
	month:$('#search_salary_month option:selected').val(),
    county:$('#search_salary_county_id option:selected').val(),
    school:$('#search_salary_school_id option:selected').val(),
	teacher:$('#search_salary_teacher_id option:selected').val(),
  }
  var data = main_post('<?php echo base_url('Salary/getDataSearch') ?>',data);
  update_table(data['table'])
}

function set_table(data) 
{  
	DATA_1 = $('#table_salary').DataTable({
		data: data,
		order: [[ 0, "asc" ]],
		columns: [
			{ 
				width: "5%", 
				className: "text-center",
				orderable: false
			},
			{ 
				width: "8%", 
				className: "text-center",
			},
			{ 
				width: "12%", 
				className: "text-left",
			},
			{ 
				width: "10%", 
				className: "text-left",
			},
			{ 
				width: "10%", 
				className: "text-center",
			},
			{ 
				width: "10%", 
				className: "text-right",
			},
			{ 
				width: "10%", 
				className: "text-right",
			},
			{ 
				width: "10%", 
				className: "text-right",
			}
		],
		pageLength: 10,
		// pagingType: "numbers",
		// dom: 'plrtip',
		// searching: false,
		lengthMenu: [10,25,50,100],
		oLanguage: {
			sSearchPlaceholder: "ค้นหา",
			sLengthMenu: "แสดง _MENU_ รายการ",
			sSearch: "ค้นหา",
			sInfo: "กำลังแสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
			sInfoEmpty: "กำลังแสดง 0 ถึง 0 จาก 0 รายการ",
			sZeroRecords: "ไม่มีข้อมูลในตาราง",
			sProcessing: "กำลังค้นหา",
		}
	});
}

function update_table(data)
{
	DATA_1.clear();
	DATA_1.rows.add(data);
	DATA_1.draw();
}

function change_county()
{
	var id 	= $('#salary_county_id option:selected').val();
	var res = main_post('<?php echo base_url('Salary/changeCounty') ?>',{id:id});
	var option = "";
	$.each(res, function (index, value) {

		option += '<option value="'+value.school_id+'">'+value.school_name+'</option>';		
	});
	$('#salary_school_id').html(option).selectpicker('refresh');
}

function change_school() 
{
	var id 	= $('#salary_school_id option:selected').val();
	var res = main_post('<?php echo base_url('Salary/changeSchool') ?>',{id:id});
	var option = '';
	$.each(res, function (index, value) {

		option += '<option value="'+value.teacher_id+'" data-code="'+value.teacher_code+'" data-price="'+value.teacher_school_price+'">'+value.teacher_name+'</option>';		
	});
	$('#salary_teacher_id').html(option).selectpicker('refresh');
}

function cal_salary_per_day(elm) 
{
	var tr = $(elm).closest('tr');
	var cash_advance = $(tr).find('.salary_d_cash_advance').val();
	var sub = $(tr).find('.salary_d_sub').val();
	var other = $(tr).find('.salary_d_other').val();
	var total = Number(DF2C(cash_advance))+Number(DF2C(sub))+Number(DF2C(other));
	$(tr).find('.salary_d_sub_total').val(F2C(total));
	cal_salary_sum_total();
	cal_salary_total();
}

function cal_salary_sum_total() 
{
	var salary_sum_total = 0;
	$('.salary_d_sub_total').each(function (key, val) { 
		if ($(this).val() != '0.00') {
			salary_sum_total += Number(DF2C($(this).val()));
		}
	});
	$('#salary_sum_total').val(F2C(Number(salary_sum_total)));
}

function cal_salary_total() 
{
	var salary_sum_total = $('#salary_sum_total').val();
	var salary_price = $('#salary_price').val();
	var total = Number(DF2C(salary_price))-Number(DF2C(salary_sum_total));
	$('#salary_price_total').val(F2C(Number(total)))
}

function get_data_teacher()
{
	var code = $('#salary_teacher_id option:selected').data('code');
	var teacher_id = $('#salary_teacher_id option:selected').val();
	$('#salary_teacher_code').val(code);
	get_cash_advance(teacher_id);
	get_teacher_sub(teacher_id);
}

function get_cash_advance(teacher_id) 
{
	var res = main_post('<?php echo base_url('Salary/getCashAdvance') ?>',{teacher_id:teacher_id});
	// cc(res)
	var html = "";
	for (let i = 0; i < res.length; i++) 
	{
		html += `<tr align="center">
					<td><input type="text" class="form-control text-center" name="salary_d_date[]" value="`+res[i].ca_date+`" readonly></td>
					<td><input type="text" class="form-control text-center f_number salary_d_cash_advance" name="salary_d_cash_advance[]" onkeyup="cal_salary_per_day(this)" value="`+res[i].ca_amount+`"></td>
					<td><input type="text" class="form-control text-center f_number salary_d_sub" name="salary_d_sub[]" value=""></td>
					<td><input type="text" class="form-control text-center f_number salary_d_other" name="salary_d_other[]" value=""></td>
					<td><input type="text" class="form-control text-center f_number salary_d_sub_total" name="salary_d_sub_total[]" value="" readonly></td>
				</tr>`;
	}
	
	$('#tbody_clone').append(html);
	$('.salary_d_cash_advance').keyup();
	// cc(html)
}

function get_teacher_sub(teacher_id) 
{
	var res = main_post('<?php echo base_url('Salary/getTeacherSub') ?>',{teacher_id:teacher_id});
	// cc(res)
	var html = "";
	for (let i = 0; i < res.length; i++) 
	{
		html += `<tr align="center">
					<td><input type="text" class="form-control text-center" name="salary_d_date[]" value="`+res[i].t_sub_date+`" readonly></td>
					<td><input type="text" class="form-control text-center f_number salary_d_cash_advance" name="salary_d_cash_advance[]" onkeyup="cal_salary_per_day(this)" value=""></td>
					<td><input type="text" class="form-control text-center f_number salary_d_sub" name="salary_d_sub[]" value="`+res[i].t_sub_price+`"></td>
					<td><input type="text" class="form-control text-center f_number salary_d_other" name="salary_d_other[]" value=""></td>
					<td><input type="text" class="form-control text-center f_number salary_d_sub_total" name="salary_d_sub_total[]" value="" readonly></td>
				</tr>`;
	}
	
	$('#tbody_clone').append(html);
	$('.salary_d_cash_advance').keyup();
}
</script>