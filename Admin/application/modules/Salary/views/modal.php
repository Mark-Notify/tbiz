<div class="modal fade" id="input_model">
  <div class="modal-dialog modal-xl modal-dialog-centered">
    <form id="input_form">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="head_modal">สร้างข้อมูล</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          
          <fieldset class="border-danger mb-2">
            <legend>คำนวณเงินเดือนรายบุคคล</legend>
            <div class="row p-2">
              
              <input type="hidden" name="salary_id" id="salary_id" value="">

                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                  <div class="form-group">
                    <input type="hidden" name="salary_county_name" id="salary_county_name">
                    <label class="l-light" for="salary_county_id">เขต </label>
                    <select name="salary_county_id" id="salary_county_id" class="form-control selectpicker show-menu-arrow"  data-size="8"  data-live-search="true" required onchange="change_county()">
                      <option value="">กรุณาเลือกเขต</option>
                      <?php
                        if (!empty($county)) 
                        {
                          foreach ($county as $key => $value) 
                          {
                            echo '<option value="'.$key.'">'.$value.'</option>';
                          }
                        }
                      ?>
                    </select>                      
                    <div class="valid-feedback">สามารถใช้งานได้</div>
                    <div class="invalid-feedback">กรุณากรอกข้อมูล</div>              
                  </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                  <div class="form-group">
                    <input type="hidden" name="salary_school_name" id="salary_school_name">
                    <label class="l-light" for="salary_school_id">ชื่อโรงเรียน </label>
                    <select name="salary_school_id" id="salary_school_id" class="form-control selectpicker show-menu-arrow" data-size="8"  data-live-search="true" required title="กรุณาเลือกโรงเรียน" onchange="change_school()">
                      <option value="">กรุณาเลือกโรงเรียน</option>
                      <?php
                        if (!empty($school)) 
                        {
                          foreach ($school as $key => $value) 
                          {
                            echo '<option value="'.$value['school_id'].'">'.$value['school_name'].'</option>';
                          }
                        }
                      ?>
                    </select>                       
                    <div class="valid-feedback">สามารถใช้งานได้</div>
                    <div class="invalid-feedback">กรุณากรอกข้อมูล</div>             
                  </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                  <div class="form-group">
                    <label class="l-light" for="salary_teacher_code"> รหัสครู</label>
                    <input type="text" class="form-control text-center" id="salary_teacher_code" name="salary_teacher_code" readonly>
                  </div>
                </div>
                
                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                  <div class="form-group">
                    <input type="hidden" name="salary_teacher_name" id="salary_teacher_name">
                    <label class="l-light" for="salary_teacher_id">ชื่อครู </label>
                      <select name="salary_teacher_id" id="salary_teacher_id" class="form-control selectpicker show-menu-arrow" data-size="8" data-live-search="true" required onchange="get_data_teacher()" title="กรุณาเลือกชื่อครู">
                      <option value="">กรุณาเลือกชื่อครู</option>
                        <?php
                          if (!empty($teacher)) 
                          {
                            foreach ($teacher as $key => $value) 
                            {
                              echo '<option value="'.$value['teacher_id'].'" data-code="'.$value['teacher_code'].'" data-price="'.$value['teacher_school_price'].'">'.$value['teacher_name'].'</option>';
                            }
                          }
                        ?>
                      </select>                  
                    <div class="valid-feedback">สามารถใช้งานได้</div>
                    <div class="invalid-feedback">กรุณากรอกข้อมูล</div>
                  </div>
                </div>

              
                <div class="col-12 d-inline w-100 text-center mb-3 mt-3">
                  <span style="background-color: white;"></span>
                  <hr style="margin: -10px 0px 15px;">
                </div>   

                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-2">
                  <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="table_income">
                      <thead>
                        <tr align="center">
                          <th width="20%">วันที่</th>
                          <th width="20%">Cash Advance</th>
                          <th width="20%">สอนแทน</th>
                          <th width="20%">อื่นๆ</th>
                          <th width="20%">ยอดรวม</th>
                        </tr>
                      </thead>
                      <tbody id="tbody_clone">
                      
                      </tbody>
                    </table>
                  </div>
                </div>

                <div class="col-sm-12 col-md-6 col-xl-9 mb-2 text-right height-center">ยอดรวม</div>
                <div class="col-sm-12 col-md-6 col-xl-3 mb-2">
                    <input type="text" class="form-control text-right f_number text-danger" name="salary_sum_total" id="salary_sum_total" value="" readonly>
                </div>

                <div class="col-sm-12 col-md-6 col-xl-9 mb-2 text-right height-center">เงินเดือน</div>
                <div class="col-sm-12 col-md-6 col-xl-3 mb-2">
                  <input type="text" class="form-control text-right f_number text-primary" name="salary_price" value="" placeholder="ระบุเงินเดือน" id="salary_price" onkeyup="cal_salary_total()">
                </div>

                <div class="col-sm-12 col-md-6 col-xl-9 mb-2 text-right height-center">ยอดที่ต้องจ่าย</div>
                <div class="col-sm-12 col-md-6 col-xl-3 mb-2">
                  <input type="text" class="form-control text-right f_number text-success" name="salary_price_total" value="" id="salary_price_total" readonly>
                </div>
              
            </div>
          </fieldset>

        </div>
            
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
          <div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="fas fa-user-check"></i>
                </span>
              </div>
              <input type="text" class="form-control text-center" name="staff_created_name" id="staff_created_name" placeholder="" readonly>
            </div>
          </div>
          <button type="button" class="btn btn-success c_save" onclick="form_save()">บันทึก</button>
        </div>
      </div>
    </form>
  </div>
</div>