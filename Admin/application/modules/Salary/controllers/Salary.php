<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Salary extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
				
		// check session
        checkSession();
        
        // load library
        // $this->load->library('UploadsFile');
	}

	public function index()
	{
		$data['county'] = $this->Option_model->optionCounty();
		$data['teacher'] = $this->Main_model->mainGetMulti('teacher','teacher_status = 1','teacher_id DESC');
		$data['school'] = $this->Main_model->mainGetMulti('school','school_status = 1');
		template('โปรแกรมเงินเดือน','index', $data, 'adminLTE');
	}

	public function saveInsert()
	{
		$users = ssUser();
    $dateTime = mainDateTime();
    $post = $this->input->post();
    // px($post);
        
		$HEAD = [
			// 'salary_id'                   => $post['salary_id'],
      'salary_county_id'            => $post['salary_county_id'],
      'salary_county_name'          => $post['salary_county_name'],
      'salary_school_id'            => $post['salary_school_id'],
      'salary_school_name'          => $post['salary_school_name'],
      'salary_teacher_id'           => $post['salary_teacher_id'],
      'salary_teacher_name'         => $post['salary_teacher_name'],
      'salary_teacher_code'         => $post['salary_teacher_code'],
      'salary_price'                => DF2C($post['salary_price']),
      'salary_sum_total'            => DF2C($post['salary_sum_total']),
      'salary_price_total'          => DF2C($post['salary_price_total']),
      'staff_created_at'            => $dateTime,
      'staff_updated_at'            => $dateTime,
      'staff_created_id'            => $users['employee_id'],
      'staff_created_name'          => $users['employee_username'],
      'staff_updated_id'            => $users['employee_id'],
      'staff_updated_name'          => $users['employee_username']
    ];
		// px($HEAD);
    $insert_id = $this->Main_model->mainInsert('salary', $HEAD, true);
    
    foreach ($post['salary_d_date'] as $key => $value) 
    {
      $DETAIL = [
        'salary_id'                 => $insert_id,
        'salary_d_date'             => $post['salary_d_date'][$key],
        'salary_d_cash_advance'     => DF2C($post['salary_d_cash_advance'][$key]),
        'salary_d_sub'              => DF2C($post['salary_d_sub'][$key]),
        'salary_d_other'            => DF2C($post['salary_d_other'][$key]),
        'salary_d_sub_total'        => DF2C($post['salary_d_sub_total'][$key])
      ];
      $result = $this->Main_model->mainInsert('salary_detail', $DETAIL);
    }

    if($result) {
      echo json_encode(array('type' => 'success','title' => 'บันทึกสำเร็จ','text' => 'กดปุ่มเพื่อดำเนินการต่อ'));
    }else{
      echo json_encode(array('type' => 'error','title' => 'บันทึกผิดพลาด','text' => 'โปรดลองใหม่อีกครั้ง'));
    }
        
	}

	public function saveUpdate()
	{
		$users = ssUser();
    $dateTime = mainDateTime();
    $post = $this->input->post();
    // px($post);
		$HEAD = [
			// 'salary_id'                   => $post['salary_id'],
      'salary_county_id'            => $post['salary_county_id'],
      'salary_county_name'          => $post['salary_county_name'],
      'salary_school_id'            => $post['salary_school_id'],
      'salary_school_name'          => $post['salary_school_name'],
      'salary_teacher_id'           => $post['salary_teacher_id'],
      'salary_teacher_name'         => $post['salary_teacher_name'],
      'salary_teacher_code'         => $post['salary_teacher_code'],
      'salary_price'                => DF2C($post['salary_price']),
      'salary_sum_total'            => DF2C($post['salary_sum_total']),
      'salary_price_total'          => DF2C($post['salary_price_total']),
      'staff_created_at'            => $dateTime,
      'staff_updated_at'            => $dateTime,
      'staff_created_id'            => $users['employee_id'],
      'staff_created_name'          => $users['employee_username'],
      'staff_updated_id'            => $users['employee_id'],
      'staff_updated_name'          => $users['employee_username']
    ];
		// px($HEAD);
    $result = $this->Main_model->mainUpdate('salary', $HEAD, 'salary_id = '.$post['salary_id']);
    $result = $this->Main_model->mainDelete('salary_detail', 'salary_id = '.$post['salary_id']);
    
    foreach ($post['salary_d_date'] as $key => $value) 
    {
      $DETAIL = [
        'salary_id'                 => $post['salary_id'],
        'salary_d_date'             => $post['salary_d_date'][$key],
        'salary_d_cash_advance'     => DF2C($post['salary_d_cash_advance'][$key]),
        'salary_d_sub'              => DF2C($post['salary_d_sub'][$key]),
        'salary_d_other'            => DF2C($post['salary_d_other'][$key]),
        'salary_d_sub_total'        => DF2C($post['salary_d_sub_total'][$key])
      ];
      $result = $this->Main_model->mainInsert('salary_detail', $DETAIL);
    }
    if($result) {
        echo json_encode(array('type' => 'success','title' => 'บันทึกสำเร็จ','text' => 'กดปุ่มเพื่อดำเนินการต่อ'));
    }else {
        echo json_encode(array('type' => 'error','title' => 'บันทึกผิดพลาด','text' => 'โปรดลองใหม่อีกครั้ง'));
    }
	}

	public function getData($salary_id)
  {
    $data = $this->Main_model->mainGet('salary', 'salary_id = '.$salary_id);
    $data['detail'] = $this->Main_model->mainGetMulti('salary_detail', 'salary_id = '.$salary_id);
    echo json_encode($data);
  }
	
  public function getDataSearch()
  {
      $post = $this->input->post();
      // px($post);
      $str_where = "";
      $where_arr[] = "salary_status = 1";
      
      if (!empty($post['county']) && $post['county'] != "") {
        $where_arr[] = "salary_county_id = $post[county]";
      }

      if (!empty($post['school']) && $post['school'] != "") {
        $where_arr[] = "salary_school_id = $post[school]";
      }

      if (!empty($post['teacher']) && $post['teacher'] != "") {
        $where_arr[] = "salary_teacher_id = $post[teacher]";
      }

      if (!empty($post['month']) && $post['month'] != "") {
        $where_arr[] = "MONTH(staff_created_at) = $post[month]";
      }
      
      if (count($where_arr) > 0) {
        $str_where = implode(" AND " , $where_arr);
      }

      $data = $this->Main_model->mainGetMulti('salary',$str_where,'salary_id DESC');
      // pp_sql();
      // pp($data);
      $reslut['table'] = [];
      $num_tr = 1;

      foreach ($data as $key => $rows) {
        $reslut['table'][] = [
          $num_tr, 
          '<a class="have-click" onclick="form_edit('.$rows['salary_id'].')">'.$rows['salary_teacher_code'].'</a>',  
          $rows['salary_teacher_name'], 
          $rows['salary_school_name'], 
          $rows['salary_county_name'], 
          F2C($rows['salary_price']),
          F2C($rows['salary_sum_total']),
          F2C($rows['salary_price_total'])
        ];
        $num_tr++;
      }
      echo json_encode($reslut);
  }

  public function changeCounty()
	{
		$post = $this->input->post();
		$data = $this->Main_model->mainGetMulti('school', "school_county_id = ".$post['id']);
		echo json_encode($data);
	}

	public function changeSchool()
	{
		$post = $this->input->post();
		$data = $this->Main_model->mainGetMulti('teacher', 'teacher_school_id = '.$post['id']);
		echo json_encode($data);
	}

  public function getCashAdvance()
  {
    $teacher_id = $this->input->post('teacher_id');
    $reslut = $this->Main_model->mainGetMulti('cash_advance', ['ca_teacher_id'=>$teacher_id],'ca_id DESC');
    echo json_encode($reslut);
  }

  public function getTeacherSub()
  {
    $teacher_id = $this->input->post('teacher_id');
    $reslut = $this->Main_model->mainGetMulti('teacher_sub', ['t_sub_for_teacher_id'=>$teacher_id],'t_sub_id DESC');
    echo json_encode($reslut);
  }
}
