<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ReportBorrow extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
				
		// check session
        checkSession();
        
        // load library
        // $this->load->library('UploadsFile');
	}

	public function index()
	{
		$data['county'] = $this->Main_model->mainGetMulti('county','county_status = 1','county_id DESC');
		$data['teacher'] = $this->Main_model->mainGetMulti('teacher','teacher_status = 1','teacher_id DESC');
		$data['school'] = $this->Main_model->mainGetMulti('school','school_status = 1');
		template('รายงานการเบิก','index', $data, 'adminLTE');
	}

	public function saveInsert()
	{
		$users = ssUser();
    $dateTime = mainDateTime();
    $post = $this->input->post();
    if (empty($post['ca_reason_1'])) {
      $post['ca_reason_1'] = 0;
    }
    if (empty($post['ca_reason_2'])) {
      $post['ca_reason_2'] = 0;
    }
    if (empty($post['ca_reason_3'])) {
      $post['ca_reason_3'] = 0;
    }
    if (empty($post['ca_reason_4'])) {
      $post['ca_reason_4'] = 0;
    }
    if (empty($post['ca_reason_5'])) {
      $post['ca_reason_5'] = 0;
    }
    if (empty($post['ca_transfer'])) {
      $post['ca_transfer'] = 0;
    }
        
		$data = array(
			// 'ca_id'                   => $post['ca_id'],
      'ca_school_id'            => $post['ca_school_id'],
      'ca_school_name'          => $post['ca_school_name'],
      'ca_date'                 => $post['ca_date'],
      'ca_name'                 => $post['ca_name'],
      'ca_passport'             => $post['ca_passport'],
      'ca_address'              => $post['ca_address'],
      'ca_amount'               => $post['ca_amount'],
      'ca_amount_text'          => $post['ca_amount_text'],
      'ca_reason_1'             => $post['ca_reason_1'],
      'ca_reason_1_price'       => $post['ca_reason_1_price'],
      'ca_reason_2'             => $post['ca_reason_2'],
      'ca_reason_2_price'       => $post['ca_reason_2_price'],
      'ca_reason_3'             => $post['ca_reason_3'],
      'ca_reason_3_price'       => $post['ca_reason_3_price'],
      'ca_reason_4'             => $post['ca_reason_4'],
      'ca_reason_4_price'       => $post['ca_reason_4_price'],
      'ca_reason_5'             => $post['ca_reason_5'],
      'ca_reason_5_text'        => $post['ca_reason_5_text'],
      'ca_reason_5_price'       => $post['ca_reason_5_price'],
      'ca_total'                => $post['ca_total'],
      'ca_passport_2'           => $post['ca_passport_2'],
      'ca_date_money_back'      => $post['ca_date_money_back'],
      'ca_name_borrow'          => $post['ca_name_borrow'],
      'ca_transfer'             => $post['ca_transfer'],
      'ca_bank_code'            => $post['ca_bank_code'],
      'ca_bank_name'            => $post['ca_bank_name'],
      'ca_contact'              => $post['ca_contact'],
      'staff_created_at'      	=> $dateTime,
      'staff_updated_at'      	=> $dateTime,
      'staff_created_id'      	=> $users['employee_id'],
      'staff_created_name'    	=> $users['employee_username'],
      'staff_updated_id'      	=> $users['employee_id'],
      'staff_updated_name'    	=> $users['employee_username']
		);
		// px($data);
		$result = $this->Main_model->mainInsert('cash_advance', $data, true);
    if($result) {
        echo json_encode(array('type' => 'success','title' => 'บันทึกสำเร็จ','text' => 'กดปุ่มเพื่อดำเนินการต่อ'));
    }else{
        echo json_encode(array('type' => 'error','title' => 'บันทึกผิดพลาด','text' => 'โปรดลองใหม่อีกครั้ง'));
    }
        
	}

	public function saveUpdate()
	{
		$users = ssUser();
    $dateTime = mainDateTime();
    $post = $this->input->post();
    // px($post);
    if (empty($post['ca_reason_1'])) {
      $post['ca_reason_1'] = 0;
    }
    if (empty($post['ca_reason_2'])) {
      $post['ca_reason_2'] = 0;
    }
    if (empty($post['ca_reason_3'])) {
      $post['ca_reason_3'] = 0;
    }
    if (empty($post['ca_reason_4'])) {
      $post['ca_reason_4'] = 0;
    }
    if (empty($post['ca_reason_5'])) {
      $post['ca_reason_5'] = 0;
    }
    if (empty($post['ca_transfer'])) {
      $post['ca_transfer'] = 0;
    }
    // px($post);
		$data = array(
			// 'ca_id'                   => $post['ca_id'],
      'ca_school_id'            => $post['ca_school_id'],
      'ca_school_name'          => $post['ca_school_name'],
      'ca_date'                 => $post['ca_date'],
      'ca_name'                 => $post['ca_name'],
      'ca_passport'             => $post['ca_passport'],
      'ca_address'              => $post['ca_address'],
      'ca_amount'               => $post['ca_amount'],
      'ca_amount_text'          => $post['ca_amount_text'],
      'ca_reason_1'             => $post['ca_reason_1'],
      'ca_reason_1_price'       => $post['ca_reason_1_price'],
      'ca_reason_2'             => $post['ca_reason_2'],
      'ca_reason_2_price'       => $post['ca_reason_2_price'],
      'ca_reason_3'             => $post['ca_reason_3'],
      'ca_reason_3_price'       => $post['ca_reason_3_price'],
      'ca_reason_4'             => $post['ca_reason_4'],
      'ca_reason_4_price'       => $post['ca_reason_4_price'],
      'ca_reason_5'             => $post['ca_reason_5'],
      'ca_reason_5_text'        => $post['ca_reason_5_text'],
      'ca_reason_5_price'       => $post['ca_reason_5_price'],
      'ca_total'                => $post['ca_total'],
      'ca_passport_2'           => $post['ca_passport_2'],
      'ca_date_money_back'      => $post['ca_date_money_back'],
      'ca_name_borrow'          => $post['ca_name_borrow'],
      'ca_transfer'             => $post['ca_transfer'],
      'ca_bank_code'            => $post['ca_bank_code'],
      'ca_bank_name'            => $post['ca_bank_name'],
      'ca_contact'              => $post['ca_contact'],
      'staff_created_at'      	=> $dateTime,
      'staff_updated_at'      	=> $dateTime,
      'staff_created_id'      	=> $users['employee_id'],
      'staff_created_name'    	=> $users['employee_username'],
      'staff_updated_id'      	=> $users['employee_id'],
      'staff_updated_name'    	=> $users['employee_username']
		);
    // px($data);
    $result = $this->Main_model->mainUpdate('cash_advance', $data, 'ca_id = '.$post['ca_id']);
    if($result) {
        echo json_encode(array('type' => 'success','title' => 'บันทึกสำเร็จ','text' => 'กดปุ่มเพื่อดำเนินการต่อ'));
    }else {
        echo json_encode(array('type' => 'error','title' => 'บันทึกผิดพลาด','text' => 'โปรดลองใหม่อีกครั้ง'));
    }
	}

	public function getData($id)
  {
    $data = $this->Main_model->mainGet('cash_advance', 'ca_id = '.$id);
    echo json_encode($data);
  }
	
  public function getDataSearch()
  {
      $post = $this->input->post();
      // px($post);
      $str_where = "";
      $where_arr[] = "ca_status = 1";
      
      
      if (count($where_arr) > 0) {
      $str_where = implode(" AND " , $where_arr);
      }

      $data = $this->Main_model->mainGetMulti('cash_advance',$str_where,'ca_id DESC');
      $data = $this->Main_model->mainGetMulti('cash_advance',$str_where,'ca_id DESC');
      // pp_sql();
      $accrual = [];
      $reslut['table'] = [];
      $num_tr = 1;

      foreach ($data as $key => $rows) {
        $reslut['table'][] = [
          $num_tr, 
          '<a class="have-click" onclick="form_edit('.$rows['ca_id'].')">'.$rows['ca_date'].'</a>',  
          $rows['ca_name'], 
          $rows['ca_passport'],
          $rows['ca_address'], 
          $rows['ca_amount'], 
          $rows['ca_total'],
          $rows['ca_date_money_back'],
          $rows['ca_name_borrow'],
          $rows['ca_bank_name']." - ".$rows['ca_bank_code'],
          $rows['ca_contact']
        ];
        $num_tr++;
      }
      echo json_encode($reslut);
  }
}
