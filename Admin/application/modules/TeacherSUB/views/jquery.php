<script>
var $tr = '<tr>'+
				'<td><input type="text" class="form-control t_sub_d_period" name="t_sub_d_period[]" required></td>'+
				'<td><input type="text" class="form-control t_sub_d_time text-center" name="t_sub_d_time[]"></td>'+
				'<td><input type="text" class="form-control t_sub_d_class" name="t_sub_d_class[]"></td>'+
				'<td><input type="text" class="form-control t_sub_d_topic" name="t_sub_d_topic[]"></td>'+
				'<td><input type="text" class="form-control t_sub_d_description" name="t_sub_d_description[]"></td>'+
				'<td class="text-center"><i class="fas fa-minus-circle text-danger" style="font-size:24px;" onclick="remove_row(this)"></i></td>'+
			'</tr>';

$(function() {
  main_loading(500, function(){

    var data = main_post('<?php echo base_url('TeacherSUB/getDataSearch') ?>');
    set_table(data['table']);
  });
})

function set_data() 
{
	var teacher_name = $('#t_sub_for_teacher_id option:selected').text();
	var school_name = $('#t_sub_school_id option:selected').text();
	$('#t_sub_for_teacher_name').val(teacher_name);
	$('#t_sub_school_name').val(school_name);
}

function form_add(elm)
{
  form_reset();
  $('#input_model').modal('show');
  $('#teacher_text_code').change();
  $('#input_model #head_modal').html($(elm).html());
}

function form_save()
{
  main_loading(500, function(){
    var head_id = $('#t_sub_id').val();
	set_data();
    if(main_validated('input_form') && validated())
    {
      if((head_id==0) || (head_id==''))//เพิ่ม
      {
        var res = main_save('<?php echo base_url('TeacherSUB/saveInsert') ?>', 'input_form');
        res_swal(res, 2);
      }
      else//แก้ไข
      {
        var res = main_save('<?php echo base_url('TeacherSUB/saveUpdate') ?>', 'input_form');
        res_swal(res, 2);
      }
      get_data_search();
    }
  });
}

function validated() 
{
	var return_ = false;
	$('.t_sub_d_period').each(function (index, value) { 
		if ($(this).val() == "") 
		{
			Swal.fire({
				position: 'top-end',
				icon: 'error',
				title: 'กรุณากรอกรายละเอียดในตารางข้อมูลด้านล่าง',
				showConfirmButton: false,
				timer: 1500
			});
		}
		else
		{
			return_ = true;
		}
	});
	return return_;
}

function form_edit(id) 
{
  form_reset();

  $('#input_model').modal('show');
  $('#head_modal').html('<i class="fas fa-folder-plus"></i> แก้ไขข้อมูล');

  main_loading(500, function(){
    form_insert_data(id);
  });
}

// นำเข้าข้อมูลไปใส่ฟอร์ม
function form_insert_data(id) 
{
	$('#t_body').html("");
	var data = main_data_to_form('<?php echo base_url('TeacherSUB/getData/') ?>', id);
	$.each(data.Head, function(key, val) {
		//   cc(key+"=="+val)
		if (key == 't_sub_for_teacher_id') {
			$('#'+key).selectpicker('val',val);
		}else if (key == 't_sub_school_id') {
			$('#'+key).selectpicker('val',val);
		}else{
			$('#'+key).val(val);
		}
	});
	$.each(data.Detail, function (key, val) { 
		var tr = $($tr).clone();
		$.each(val, function (k, v) { 
			// cc(k+'=='+v)
			$(tr).find('.'+k).val(v);
		});
		$('#t_body').append(tr);
	});
	$('.selectpicker_all').selectpicker('refresh');
}

function form_reset() 
{
  var tr = $($tr).clone();
  // หนึงรายการ
  $('#input_form')[0].reset();  // เคลียร์ฟอร์ม
  $('#input_form').removeClass('was-validated');
  $('#t_sub_id').val(0); // ไอดี 0
  $('#t_body').html("");
  $('#t_body').append(tr);
  $('.selectpicker_all').selectpicker('refresh');
}


function get_data_search() 
{
	var data = main_post('<?php echo base_url('TeacherSUB/getDataSearch') ?>');
  	update_table(data['table'])
}

function set_table(data) 
{  
	DATA_1 = $('#table_teacher_sub').DataTable({
		data: data,
		order: [[ 0, "asc" ]],
		columns: [
			{ 
				width: "5%", 
				className: "text-center",
				orderable: false
			},
			{ 
				width: "8%", 
				className: "text-center",
			},
			{ 
				width: "12%", 
				className: "text-left",
			},
			{ 
				width: "10%", 
				className: "text-left",
			},
			{ 
				width: "10%", 
				className: "text-center",
			},
			{ 
				width: "10%", 
				className: "text-center",
			},
			{ 
				width: "10%", 
				className: "text-center",
			},
			{ 
				width: "10%", 
				className: "text-left",
			}
		],
		pageLength: 10,
		// pagingType: "numbers",
		// dom: 'plrtip',
		// searching: false,
		lengthMenu: [10,25,50,100],
		oLanguage: {
			sSearchPlaceholder: "Search",
			sLengthMenu: "Show _MENU_ Entries",
			sSearch: "Search",
			sInfo: "Showing _START_ to _END_ of _TOTAL_ Entries",
			sInfoEmpty: "Showing 0 to 0 of 0 Entries",
			sZeroRecords: "No data In Table",
			sProcessing: "Processing",
		}
	});
}

function update_table(data)
{
	DATA_1.clear();
	DATA_1.rows.add(data);
	DATA_1.draw();
}

function add_row() 
{
	var tr = $($tr).clone();
	$('#t_body').append(tr);
}

function remove_row(elm) 
{
	var tr = $(elm).closest('tr');
    $(tr).remove();
}

</script>