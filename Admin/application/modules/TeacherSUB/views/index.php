<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Sub Teacher</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Sub Teacher</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <section class="col-lg-12 connectedSortable">
        <div class="card card-success card-outline">
          <div class="card-header">
            <h3 class="card-title">
              <i class="fas fa-chart-pie mr-1"></i>
              information
            </h3>
            <div class="card-tools">
              <button type="button" class="btn btn-primary" onclick="form_add(this)">Create</button>
            </div>

          </div>            

          <div class="card-body">

            <div class="row">
              <div class="table-responsive">
                <table class="table table-bordered table-striped" id="table_teacher_sub">
                  <thead>
                    <tr align="center">
                      <th>#</th>
                      <th width="10%">Date</th>
                      <th width="10%">Teacher Name</th>
                      <th width="10%">School Name</th>
                      <th width="10%">Sub Teacher Name</th>
                      <th width="10%">Tel</th>
                      <th width="10%">Time All</th>
                      <th width="10%">School Cordinator</th>
                    </tr>
                  </thead>
                  <tbody>
                  
                  </tbody>
                </table>
              </div>
            </div>

          </div>
          
        </div>
      </section>
      
    </div>
  </div>
</section>

<?php echo $this->load->view('modal') ?>
<?php echo $this->load->view('jquery') ?>