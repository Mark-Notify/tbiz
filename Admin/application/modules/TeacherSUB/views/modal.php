<div class="modal fade" id="input_model">
  <div class="modal-dialog modal-xxl modal-dialog-centered" role="document">
    <form id="input_form">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="head_modal">สร้างข้อมูล</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          
          <fieldset class="border-danger">
            <legend>ใบลงเวลาครูสอนแทน</legend>
            <div class="row">
              
              <input type="hidden" name="t_sub_id" id="t_sub_id" value="0">

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                <div class="form-group">
                  <label class="l-light" for="t_sub_day">วัน</label>
                  <select class="form-control" name="t_sub_day" id="t_sub_day">
                  <?php
                    foreach (day() as $key => $value) 
                    {
                      echo '<option value="'.$key.'">'.$value.'</option>';
                    }
                  ?>
                    
                  </select>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                <div class="form-group">
                  <label class="l-light" for="t_sub_date">วันที่ </label>
                  <input type="text" class="form-control datepicker text-center" id="t_sub_date" name="t_sub_date" value="<?php echo date('Y-m-d')?>" readonly>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                <input type="hidden" class="form-control" id="t_sub_for_teacher_name" name="t_sub_for_teacher_name">
                <div class="form-group">
                  <label class="l-light" for="t_sub_for_teacher_id">ครูที่ถูกสอนแทน</label>
                  <select name="t_sub_for_teacher_id" id="t_sub_for_teacher_id" class="form-control selectpicker show-menu-arrow" title="กรุณาเลือกชื่อครู" data-size="8" data-live-search="true">
                    <option value="" selected>กรุณาเลือกชื่อครู</option>
                    <?php
                      if (!empty($teacher)) 
                      {
                        foreach ($teacher as $key => $value) 
                        {
                          echo '<option value="'.$value['teacher_id'].'">'.$value['teacher_name'].'</option>';
                        }
                      }
                    ?>
                  </select>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                <input type="hidden" class="form-control" id="t_sub_school_name" name="t_sub_school_name">
                <div class="form-group">
                  <label class="l-light" for="t_sub_school_id">ชื่อโรงเรียน</label>
                  <select name="t_sub_school_id" id="t_sub_school_id" class="form-control selectpicker show-menu-arrow" title="กรุณาเลือกชื่อโรงเรียน" data-size="8" data-live-search="true">
                    <option value="" selected>กรุณาเลือกชื่อโรงเรียน</option>
                    <?php
                      if (!empty($school)) 
                      {
                        foreach ($school as $key => $value) 
                        {
                          echo '<option value="'.$value['school_id'].'">'.$value['school_name'].'</option>';
                        }
                      }
                    ?>
                  </select>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                <div class="form-group">
                  <label class="l-light" for="t_sub_teacher_name">ครูที่มาสอนแทน</label>
                  <input type="text" class="form-control" id="t_sub_teacher_name" name="t_sub_teacher_name">
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                <div class="form-group">
                  <label class="l-light" for="t_sub_tel">เบอร์โทร</label>
                  <input type="text" class="form-control" id="t_sub_tel" name="t_sub_tel">
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                <div class="form-group">
                  <label class="l-light" for="t_sub_time">เวลาทั้งหมด (ชั่วโมง)</label>
                  <input type="text" class="form-control text-center" id="t_sub_time" name="t_sub_time">
                </div>
              </div>
              
              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                <div class="form-group">
                  <label class="l-light" for="t_sub_price">จำนวนเงิน (บาท)</label>
                  <input type="text" class="form-control text-center" id="t_sub_price" name="t_sub_price">
                </div>
              </div>

              <div class="col-12 d-inline w-100 text-center mb-3 mt-3">
                <span style="background-color: white;">ตารางการลงเวลา</span>
                <hr style="margin: -10px 0px 15px;">
              </div>

              <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <button type="button" class="btn btn-dark" onclick="add_row()"><i class="fas fa-plus"></i> เพิ่มแถว</button>
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th scope="col">Period</th>
                      <th scope="col">Time</th>
                      <th scope="col">Class</th>
                      <th scope="col">Toppic</th>
                      <th scope="col">Description</th>
                      <th scope="col" >Delete</th>
                    </tr>
                  </thead>
                  <tbody id="t_body">
                    <tr>
                      <td><input type="text" class="form-control t_sub_d_period" name="t_sub_d_period[]" required></td>
                      <td><input type="text" class="form-control text-center" name="t_sub_d_time[]"></td>
                      <td><input type="text" class="form-control" name="t_sub_d_class[]"></td>
                      <td><input type="text" class="form-control" name="t_sub_d_topic[]"></td>
                      <td><input type="text" class="form-control" name="t_sub_d_description[]"></td>
                      <td class="text-center"><i class="fas fa-minus-circle text-danger" style="font-size:24px;" onclick="remove_row(this)"></i></td>
                    </tr>
                  </tbody>
                </table>
              </div>

            </div>
          </fieldset>

        </div>
            
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
          <div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="fas fa-user-check"></i>
                </span>
              </div>
              <input type="text" class="form-control text-center" name="staff_created_name" id="staff_created_name" placeholder="" readonly>
            </div>
          </div>
          <button type="button" class="btn btn-success" onclick="form_save()">บันทึก</button>
        </div>
      </div>
    </form>
  </div>
</div>