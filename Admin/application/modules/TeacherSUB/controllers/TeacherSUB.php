<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TeacherSUB extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
				
		// check session
        checkSession();
        
        // load library
        // $this->load->library('UploadsFile');
	}

	public function index()
	{
		$data['county'] = $this->Main_model->mainGetMulti('county','county_status = 1','county_id DESC');
		$data['teacher'] = $this->Main_model->mainGetMulti('teacher','teacher_status = 1','teacher_id DESC');
		$data['school'] = $this->Main_model->mainGetMulti('school','school_status = 1');
		template('SUB Teacher','index', $data, 'adminLTE');
	}

	public function saveInsert()
	{
		$users = ssUser();
        $dateTime = mainDateTime();
        $post = $this->input->post();

        // px($post);
		$head = [
			// 't_sub_id'                  => '',
            't_sub_day'                 => $post['t_sub_day'],
            't_sub_date'                => $post['t_sub_date'],
            't_sub_for_teacher_id'      => $post['t_sub_for_teacher_id'],
            't_sub_for_teacher_name'    => $post['t_sub_for_teacher_name'],
            't_sub_school_id'           => $post['t_sub_school_id'],
            't_sub_school_name'         => $post['t_sub_school_name'],
            't_sub_teacher_name'        => $post['t_sub_teacher_name'],
            't_sub_tel'                 => $post['t_sub_tel'],
            't_sub_time'                => $post['t_sub_time'],
            't_sub_price'               => $post['t_sub_price'],
            // 'staff_created_at'      	=> $dateTime,
            // 'staff_updated_at'      	=> $dateTime,
            // 'staff_created_id'      	=> $users['employee_id'],
            // 'staff_created_name'    	=> $users['employee_username'],
            // 'staff_updated_id'      	=> $users['employee_id'],
            // 'staff_updated_name'    	=> $users['employee_username']
        ];
		// px($head);
        $insert_id = $this->Main_model->mainInsert('teacher_sub', $head, true);
        if (count($post['t_sub_d_time']) > 0) 
        {
            foreach ($post['t_sub_d_time'] as $key => $value) 
            {
                $detail = [
                    't_sub_id'              => $insert_id,
                    't_sub_d_period'        => $post['t_sub_d_period'][$key],
                    't_sub_d_time'          => $post['t_sub_d_time'][$key],
                    't_sub_d_class'         => $post['t_sub_d_class'][$key],
                    't_sub_d_topic'         => $post['t_sub_d_topic'][$key],
                    't_sub_d_description'   => $post['t_sub_d_description'][$key]
                ];
                $result = $this->Main_model->mainInsert('teacher_sub_detail', $detail);
            }
        }
        if($insert_id) {
            echo json_encode(array('type' => 'success','title' => 'บันทึกสำเร็จ','text' => 'กดปุ่มเพื่อดำเนินการต่อ'));
        }else{
            echo json_encode(array('type' => 'error','title' => 'บันทึกผิดพลาด','text' => 'โปรดลองใหม่อีกครั้ง'));
        }
	}

	public function saveUpdate()
	{
		$users = ssUser();
        $dateTime = mainDateTime();
        $post = $this->input->post();

        // px($post);
		$head = [
			// 't_sub_id'                  => '',
            't_sub_day'                 => $post['t_sub_day'],
            't_sub_date'                => $post['t_sub_date'],
            't_sub_for_teacher_id'      => $post['t_sub_for_teacher_id'],
            't_sub_for_teacher_name'    => $post['t_sub_for_teacher_name'],
            't_sub_school_id'           => $post['t_sub_school_id'],
            't_sub_school_name'         => $post['t_sub_school_name'],
            't_sub_teacher_name'        => $post['t_sub_teacher_name'],
            't_sub_tel'                 => $post['t_sub_tel'],
            't_sub_time'                => $post['t_sub_time'],
            't_sub_price'               => $post['t_sub_price'],
            // 'staff_created_at'      	=> $dateTime,
            // 'staff_updated_at'      	=> $dateTime,
            // 'staff_created_id'      	=> $users['employee_id'],
            // 'staff_created_name'    	=> $users['employee_username'],
            // 'staff_updated_id'      	=> $users['employee_id'],
            // 'staff_updated_name'    	=> $users['employee_username']
        ];
		// px($head);
        $insert_id = $this->Main_model->mainUpdate('teacher_sub', $head,'t_sub_id = '.$post['t_sub_id']);
        if (count($post['t_sub_d_time']) > 0) 
        {
            $this->Main_model->mainDelete('teacher_sub_detail','t_sub_id = '.$post['t_sub_id']);
            foreach ($post['t_sub_d_time'] as $key => $value) 
            {
                $detail = [
                    't_sub_id'              => $post['t_sub_id'],
                    't_sub_d_period'        => $post['t_sub_d_period'][$key],
                    't_sub_d_time'          => $post['t_sub_d_time'][$key],
                    't_sub_d_class'         => $post['t_sub_d_class'][$key],
                    't_sub_d_topic'         => $post['t_sub_d_topic'][$key],
                    't_sub_d_description'   => $post['t_sub_d_description'][$key]
                ];
                $result = $this->Main_model->mainInsert('teacher_sub_detail', $detail);
            }
        }

        if($result) {
            echo json_encode(array('type' => 'success','title' => 'บันทึกสำเร็จ','text' => 'กดปุ่มเพื่อดำเนินการต่อ'));
        }else {
            echo json_encode(array('type' => 'error','title' => 'บันทึกผิดพลาด','text' => 'โปรดลองใหม่อีกครั้ง'));
        }
	}

	public function getData($id)
    {
        $data['Head'] = $this->Main_model->mainGet('teacher_sub', 't_sub_id = '.$id);
        $data['Detail'] = $this->Main_model->mainGetMulti('teacher_sub_detail', 't_sub_id = '.$id);
        echo json_encode($data);
	}
        
    public function getDataSearch()
    {
        $users = ssUser();
        $post = $this->input->post();
        // px($post);
        $str_where = "";
        $where_arr[] = "t_sub_status = 1";
        
        if(!empty($users['teacher_id']))
        {
            $where_arr[] = "t_sub_for_teacher_id = $users[teacher_id]";
        }
        
        if (count($where_arr) > 0) {
            $str_where = implode(" AND " , $where_arr);
        }

        $data = $this->Main_model->mainGetMulti('teacher_sub',$str_where,'t_sub_id DESC');
        // pp_sql();
        $accrual = [];
        $reslut['table'] = [];
        $num_tr = 1;

        foreach ($data as $key => $rows) {
        $reslut['table'][] = [
            $num_tr, 
            '<a class="have-click" onclick="form_edit('.$rows['t_sub_id'].')">'.$rows['t_sub_date'].'</a>',  
            $rows['t_sub_for_teacher_name'], 
            $rows['t_sub_school_name'],
            $rows['t_sub_teacher_name'], 
            $rows['t_sub_tel'], 
            $rows['t_sub_time'],
            $rows['t_sub_school_admin']
        ];
        $num_tr++;
        }
        echo json_encode($reslut);
    }

    public function CodeGen()
    {
        $post = $this->input->post();
        $data = $this->Main_model->mainGet('teacher', "teacher_code LIKE '%$post[value]%'", 'teacher_code DESC');
        
        $txt_code = "";
        $year = substr((date('Y')+543),2,2);
        if (!empty($data['teacher_code'])) 
        {
        $sub_code = explode('-',$data['teacher_code']);
        $sum_code = (int)$sub_code[1]+1;
        $txt_code .= $sub_code[0]."-".$year.sprintf('%04d',$sum_code);;
        }
        else
        {
        $txt_code .= $post['value']."-".$year.sprintf('%04d','1');;
        }
        
        echo json_encode(['code'=>$txt_code]);
    }
}
