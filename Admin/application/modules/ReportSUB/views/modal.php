<div class="modal fade" id="input_model">
  <div class="modal-dialog modal-xxl modal-dialog-centered" role="document">
    <form id="input_form">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="head_modal">สร้างข้อมูล</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          
          <fieldset class="border-danger mb-2">
            <legend>ฟอร์มค่าสอนแทน</legend>
            <div class="row p-2">
              
            <input type="hidden" name="rs_id" id="rs_id" value="">

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4 mb-2">
                <input type="hidden" name="rs_school_name" id="rs_school_name" value="">
                <div class="form-group">
                  <label class="l-light" for="rs_school_id"> ชื่อโรงเรียนที่ไปสอน</label>
                  <select name="rs_school_id" id="rs_school_id" class="form-control selectpicker_all  show-menu-arrow" data-size="8" data-live-search="true" title="กรุณาเลือก">
                    <?php
                      if (!empty($school)) 
                      {
                        foreach ($school as $key => $value) 
                        {
                          echo '<option value="'.$value['school_id'].'">'.$value['school_name'].'</option>';
                        }
                      }
                    ?>
                  </select>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4 mb-2">
                <div class="form-group">
                  <label class="l-light" for="rs_date">Date</label>
                  <input type="text" class="form-control text-center" id="rs_date" name="rs_date" value="<?php echo date('Y-m-d') ?>" readonly>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4 mb-2">
                <div class="form-group">
                  <label class="l-light" for="rs_name">Name</label>
                  <input type="text" class="form-control" id="rs_name" name="rs_name" value="">
                </div>
              </div>

              <div class="col-12 d-inline w-100 text-center mb-3 mt-3">
                <span style="background-color: white;"></span>
                <hr style="margin: -10px 0px 15px;">
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="rs_hour">จำนวนชั่วโมง<input type="text" class="dotted text-center" id="rs_hour" name="rs_hour" value="" onkeyup="cal_price()"> ชั่วโมง</label>                  
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="rs_price">ชั่วโมงละ<input type="text" class="dotted text-center" id="rs_price" name="rs_price" value="" onkeyup="cal_price()"> บาท</label>                  
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="rs_price_total">รวมเป็นเงิน<input type="text" class="dotted text-center" id="rs_price_total" name="rs_price_total" value="" readonly> บาท</label>                  
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <!-- <label class="l-light" for="">รูปแบบการจ่ายเงินเดือน</label> -->
                <div class="form-group clearfix">
                  <div class="icheck-success d-inline">
                    <input type="radio" id="rs_type_salary_1" name="rs_type_salary" value="1" checked>
                    <label for="rs_type_salary_1">รายชั่วโมง</label>
                  </div>
                  <div class="icheck-success d-inline">
                    <input type="radio" id="rs_type_salary_2" name="rs_type_salary" value="2">
                    <label for="rs_type_salary_2">รายเดือน</label>
                  </div>
                </div>
              </div>
              
              <div class="col-12 d-inline w-100 text-center mb-3 mt-3">
                <span style="background-color: white;">ใบสำคัญการจ่ายเงินค่าสอนแทน</span>
                <hr style="margin: -10px 0px 15px;">
              </div>
              
              <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-2">
                <div class="form-group">
                  <label class="l-light" for="">
                    วันที่จ่ายเงิน
                    <input type="text" class="dotted datepicker text-center" id="rs_payment_date" name="rs_payment_date" value=""> 
                    จำนวนเงิน
                    <input type="text" class="dotted text-center" id="rs_payment_amount" name="rs_payment_amount" value=""> 
                    บาท
                  </label>                  
                </div>
              </div>

              <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-2 text-center">
                <div class="form-group">
                  <label class="l-light" for="">
                    ลงชื่อผู้จ่ายเงิน
                    <input type="text" class="dotted text-center" id="rs_payer_name" name="rs_payer_name" value=""> 
                    ลงชื่อผู้รับเงิน
                    <input type="text" class="dotted text-center" id="rs_payee_name" name="rs_payee_name" value=""> 
                  </label>                  
                </div>
              </div>              
              

            </div>
          </fieldset>

        </div>
            
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
          <div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="fas fa-user-check"></i>
                </span>
              </div>
              <input type="text" class="form-control text-center" name="staff_created_name" id="staff_created_name" placeholder="" readonly>
            </div>
          </div>
          <button type="button" class="btn btn-success" onclick="form_save()">บันทึก</button>
        </div>
      </div>
    </form>
  </div>
</div>