<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">ใบสรุปค่าสอนแทน</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">ใบสรุปค่าสอนแทน</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <section class="col-lg-12 connectedSortable">
        <div class="card card-success card-outline">
          <div class="card-header">
            <h3 class="card-title">
              <i class="fas fa-chart-pie mr-1"></i>
              ข้อมูล
            </h3>
            <div class="card-tools">
              <button type="button" class="btn btn-primary" onclick="form_add(this)">สร้างข้อมูล</button>
            </div>

          </div>            

          <div class="card-body">

            <div class="row">
              <div class="table-responsive">
                <table class="table table-bordered table-striped" id="table_income">
                  <thead>
                    <tr align="center">
                      <th>#</th>
                      <th width="10%">วันที่</th>
                      <th width="10%">ชื่อครู</th>
                      <th width="10%">ชื่อโรงเรียน</th>
                      <th width="10%">เวลาสอน</th>
                      <th width="10%">จำนวนเงิน</th>
                      <th width="10%">รูปแบบการจ่ายเงินเดือน</th>
                      <th width="10%">วันที่จ่ายเงิน</th>
                      <th width="10%">จำนวนเงินที่จ่าย</th>
                      <th width="10%">ชื่อผู้จ่ายเงิน</th>
                      <th width="10%">ชื่อผู้รับเงิน</th>
                    </tr>
                  </thead>
                  <tbody>
                  
                  </tbody>
                </table>
              </div>
            </div>

          </div>
          
        </div>
      </section>
      
    </div>
  </div>
</section>

<?php echo $this->load->view('modal') ?>
<?php echo $this->load->view('jquery') ?>