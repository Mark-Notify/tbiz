<div class="modal fade" id="input_model">
  <div class="modal-dialog modal-xxl modal-dialog-centered" role="document">
    <form id="input_form">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="head_modal">สร้างข้อมูล</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          
          <fieldset class="border-danger mb-2">
            <legend>ฟอร์มค่าสอนแทน</legend>
            <div class="row p-2">
              
            <input type="hidden" name="rs_id" id="rs_id" value="">

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4 mb-2">
                <input type="hidden" name="rs_school_name" id="rs_school_name" value="">
                <div class="form-group">
                  <label class="l-light" for="rs_school_id"> ชื่อโรงเรียนที่ไปสอน</label>
                  <select name="rs_school_id" id="rs_school_id" class="form-control selectpicker_all  show-menu-arrow" data-size="8" data-live-search="true" title="กรุณาเลือก">
                    <?php
                      if (!empty($school)) 
                      {
                        foreach ($school as $key => $value) 
                        {
                          echo '<option value="'.$value['school_id'].'">'.$value['school_name'].'</option>';
                        }
                      }
                    ?>
                  </select>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4 mb-2">
                <div class="form-group">
                  <label class="l-light" for="rs_date">Date</label>
                  <input type="text" class="form-control text-center" id="rs_date" name="rs_date" value="<?php echo date('Y-m-d') ?>" readonly>
                </div>
              </div>
              
              <div class="col-12 d-inline w-100 text-center mb-3 mt-3">
                <span style="background-color: white;">ใบสำคัญการจ่ายเงินค่าสอนแทน</span>
                <hr style="margin: -10px 0px 15px;">
              </div>
              
              <div class="row">
                <div class="table-responsive">
                  <table class="table table-bordered table-striped">
                    <thead>
                      <tr align="center">
                        <th colspan="3"><label class="l-light">Teacher Code : </label> <?php echo (!empty($_SESSION['user']['teacher_code']))?$_SESSION['user']['teacher_code']:""; ?></th>  
                        <th colspan="7">(Hr.Week,ชม./สัปดาห์)</th>    
                        <th colspan="3"><label class="l-light">Teacher's name : </label> <?php echo (!empty($_SESSION['user']['teacher_name']))?$_SESSION['user']['teacher_name']:""; ?></th>
                      </tr>
                      <tr align="center">
                        <th rowspan="3" class="row_center">Date</th>
                        <th rowspan="3" class="row_center">Day</th>
                        <th rowspan="3" class="row_center">Time In</th>
                        <th colspan="10">Time</th>
                      </tr>
                      <tr align="center">
                        <th>8:30</th>
                        <th>9:30</th>
                        <th>10:30</th>
                        <th rowspan="2" class="row_center">LunchBreak</th>
                        <th>12:30</th>
                        <th>13:30</th>
                        <th>14:30</th>
                        <th>15:30</th>
                        <th rowspan="2" class="row_center">Time Out</th>
                        <th rowspan="2" class="row_center">Hour/Day</th>
                      </tr>
                      <tr align="center">
                        <th>9:30</th>
                        <th>10:30</th>
                        <th>11:30</th>
                        <!-- <th>LunchBreak</th> -->
                        <th>13:30</th>
                        <th>14:30</th>
                        <th>15:30</th>
                        <th>16:30</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                        $j= 1;
                        for ($i=0; $i <= 30; $i++) { 
                      ?>
                      <tr align="center">
                        <td><?php echo $j ?><input type="hidden" name="t_dtr_d_date[]" id="t_dtr_d_date_<?php echo $i ?>" value="<?php echo $j ?>"></td>
                        <td><input type="text" class="form-control clear text-center t_dtr_d_day" name="t_dtr_d_day[]" value=""></td>
                        <td><input type="time" class="form-control clear text-center t_dtr_d_time_in" name="t_dtr_d_time_in[]" value=""></td>
                        <td><input type="text" class="form-control clear text-center t_dtr_d_time_1" name="t_dtr_d_time_1[]" value=""></td>
                        <td><input type="text" class="form-control clear text-center t_dtr_d_time_2" name="t_dtr_d_time_2[]" value=""></td>
                        <td><input type="text" class="form-control clear text-center t_dtr_d_time_3" name="t_dtr_d_time_3[]" value=""></td>
                        <td></td> <!-- Lunch Break -->
                        <td><input type="text" class="form-control clear text-center t_dtr_d_time_4" name="t_dtr_d_time_4[]" value=""></td>
                        <td><input type="text" class="form-control clear text-center t_dtr_d_time_5" name="t_dtr_d_time_5[]" value=""></td>
                        <td><input type="text" class="form-control clear text-center t_dtr_d_time_6" name="t_dtr_d_time_6[]" value=""></td>
                        <td><input type="text" class="form-control clear text-center t_dtr_d_time_7" name="t_dtr_d_time_7[]" value=""></td>
                        <td><input type="time" class="form-control clear text-center t_dtr_d_time_out" name="t_dtr_d_time_out[]" value=""></td>
                        <td><input type="text" class="form-control clear text-center cal_hour i_number c_select t_dtr_d_time_total" name="t_dtr_d_time_total[]" value="" onkeyup="cal_hour()"></td>
                      </tr>
                      <?php 
                          $j++;
                        } 
                      ?>
                      <tr>
                        <td colspan="10" align="right">Total</td>
                        <td colspan="3" align="center" id="total"><?php echo $HEAD['t_dtr_total'] ?>  Hour/Month</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>         
              
            </div>
          </fieldset>

        </div>
            
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
          <div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="fas fa-user-check"></i>
                </span>
              </div>
              <input type="text" class="form-control text-center" name="staff_created_name" id="staff_created_name" placeholder="" readonly>
            </div>
          </div>
          <button type="button" class="btn btn-success" onclick="form_save()">บันทึก</button>
        </div>
      </div>
    </form>
  </div>
</div>