<script>
$(function() {
  main_loading(500, function(){

    var data = main_post('<?php echo base_url('ReportDTR/getDataSearch') ?>');
    set_table(data['table']);
    // set_info(data['info']);
  });
})

function form_add(elm)
{
  form_reset();
  $('#input_model').modal('show');
  $('#teacher_text_code').change();
  $('#input_model #head_modal').html($(elm).html());
}

function form_save()
{
  main_loading(500, function(){
	set_data();
    var head_id = $('#rs_id').val();
    if(main_validated('input_form'))
    {
      if((head_id==0) || (head_id==''))//เพิ่ม
      {
        var res = main_save('<?php echo base_url('ReportDTR/saveInsert') ?>', 'input_form');
        res_swal(res, 2);
      }
      else//แก้ไข
      {
        var res = main_save('<?php echo base_url('ReportDTR/saveUpdate') ?>', 'input_form');
        res_swal(res, 2);
      }
      get_data_search();
    }
  });
}

function set_data() 
{
	$('#rs_school_name').val($('#rs_school_id option:selected').text());
}

function form_edit(id) 
{
  form_reset();

  $('#input_model').modal('show');
  $('#head_modal').html('<i class="fas fa-folder-plus"></i> แก้ไขข้อมูล');

  main_loading(500, function(){
    form_insert_data(id);
  });
}

// นำเข้าข้อมูลไปใส่ฟอร์ม
function form_insert_data(id) 
{
  var data = main_data_to_form('<?php echo base_url('ReportDTR/getData/') ?>', id);
  $.each(data, function(key, val) {

	  if(key == 'rs_school_id' && val != "")
	  {
		$('#rs_school_id').selectpicker('val', val);
	  }
	  else if(key =='rs_type_salary')
	  {
		$('input[name="'+key+'"][value="'+val+'"]').prop('checked',true);
	  }
	  else
	  {
		$('#'+key).val(val);
	  }
  });
// cc(teacher_district_id)
}

function form_reset() 
{
  // หนึงรายการ
  $('#input_form')[0].reset();  // เคลียร์ฟอร์ม
  $('#input_form').removeClass('was-validated');
  $('#rs_id').val(0); // ไอดี 0
  $('.selectpicker_all').selectpicker('refresh');
}


function get_data_search() 
{
  var data = {
    mount:$('#input_search_mount option:selected').val(),
    accrual:$('#search_accrual_money:checked').val(),
  }
  var data = main_post('<?php echo base_url('ReportDTR/getDataSearch') ?>',data);
  update_table(data['table'])
//   set_info(data['info']);
}

function set_table(data) 
{  
	DATA_1 = $('#table_income').DataTable({
		data: data,
		order: [[ 0, "asc" ]],
		columns: [
			{ 
				width: "5%", 
				className: "text-center",
				orderable: false
			},
			{ 
				width: "8%", 
				className: "text-center",
			},
			{ 
				width: "12%", 
				className: "text-left",
			},
			{ 
				width: "10%", 
				className: "text-left",
			},
			{ 
				width: "10%", 
				className: "text-center",
			},
			{ 
				width: "10%", 
				className: "text-center",
			},
			{ 
				width: "10%", 
				className: "text-center",
			}
		],
		pageLength: 10,
		// pagingType: "numbers",
		// dom: 'plrtip',
		// searching: false,
		lengthMenu: [10,25,50,100],
		oLanguage: {
			sSearchPlaceholder: "ค้นหา",
			sLengthMenu: "แสดง _MENU_ รายการ",
			sSearch: "ค้นหา",
			sInfo: "กำลังแสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
			sInfoEmpty: "กำลังแสดง 0 ถึง 0 จาก 0 รายการ",
			sZeroRecords: "ไม่มีข้อมูลในตาราง",
			sProcessing: "กำลังค้นหา",
		}
	});
}

function update_table(data)
{
	DATA_1.clear();
	DATA_1.rows.add(data);
	DATA_1.draw();
}

function set_info(data) 
{
  $('#balance_money').text(data['accrual']+" บาท");
}

function cal_price() 
{
	var hour = $('#rs_hour').val();
	var price = $('#rs_price').val();
	var total = Number(hour)*Number(price);
	$('#rs_price_total').val(F2C(total));
}
</script>