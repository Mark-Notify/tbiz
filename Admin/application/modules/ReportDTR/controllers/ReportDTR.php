<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ReportDTR extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
				
		// check session
        checkSession();
        
        // load library
        // $this->load->library('UploadsFile');
	}

	public function index()
	{
		$data['county'] = $this->Main_model->mainGetMulti('county','county_status = 1','county_id DESC');
		$data['teacher'] = $this->Main_model->mainGetMulti('teacher','teacher_status = 1','teacher_id DESC');
		$data['school'] = $this->Main_model->mainGetMulti('school','school_status = 1');
		template('รายงานใบลงเวลาครู','index', $data, 'adminLTE');
	}

	public function saveInsert()
	{
		$users = ssUser();
    $dateTime = mainDateTime();
    $post = $this->input->post();
    // px($post);
        
		$data = array(
			// 'rs_id'                   => $post['rs_id'],
      'rs_school_id'            => $post['rs_school_id'],
      'rs_school_name'          => $post['rs_school_name'],
      'rs_date'                 => $post['rs_date'],
      'rs_name'                 => $post['rs_name'],
      'rs_hour'                 => $post['rs_hour'],
      'rs_price'                => $post['rs_price'],
      'rs_price_total'          => $post['rs_price_total'],
      'rs_type_salary'          => $post['rs_type_salary'],
      'rs_payment_date'         => $post['rs_payment_date'],
      'rs_payment_amount'       => $post['rs_payment_amount'],
      'rs_payer_name'           => $post['rs_payer_name'],
      'rs_payee_name'           => $post['rs_payee_name'],
      'staff_created_at'      	=> $dateTime,
      'staff_updated_at'      	=> $dateTime,
      'staff_created_id'      	=> $users['employee_id'],
      'staff_created_name'    	=> $users['employee_username'],
      'staff_updated_id'      	=> $users['employee_id'],
      'staff_updated_name'    	=> $users['employee_username']
		);
		// px($data);
		$result = $this->Main_model->mainInsert('report_sub', $data, true);
    if($result) {
        echo json_encode(array('type' => 'success','title' => 'บันทึกสำเร็จ','text' => 'กดปุ่มเพื่อดำเนินการต่อ'));
    }else{
        echo json_encode(array('type' => 'error','title' => 'บันทึกผิดพลาด','text' => 'โปรดลองใหม่อีกครั้ง'));
    }
        
	}

	public function saveUpdate()
	{
		$users = ssUser();
    $dateTime = mainDateTime();
    $post = $this->input->post();
    // px($post);
    // px($post);
		$data = array(
			// 'rs_id'                   => $post['rs_id'],
      'rs_school_id'            => $post['rs_school_id'],
      'rs_school_name'          => $post['rs_school_name'],
      'rs_date'                 => $post['rs_date'],
      'rs_name'                 => $post['rs_name'],
      'rs_hour'                 => $post['rs_hour'],
      'rs_price'                => $post['rs_price'],
      'rs_price_total'          => $post['rs_price_total'],
      'rs_type_salary'          => $post['rs_type_salary'],
      'rs_payment_date'         => $post['rs_payment_date'],
      'rs_payment_amount'       => $post['rs_payment_amount'],
      'rs_payer_name'           => $post['rs_payer_name'],
      'rs_payee_name'           => $post['rs_payee_name'],
      'staff_created_at'      	=> $dateTime,
      'staff_updated_at'      	=> $dateTime,
      'staff_created_id'      	=> $users['employee_id'],
      'staff_created_name'    	=> $users['employee_username'],
      'staff_updated_id'      	=> $users['employee_id'],
      'staff_updated_name'    	=> $users['employee_username']
		);
    // px($data);
    $result = $this->Main_model->mainUpdate('report_sub', $data, 'rs_id = '.$post['rs_id']);
    if($result) {
        echo json_encode(array('type' => 'success','title' => 'บันทึกสำเร็จ','text' => 'กดปุ่มเพื่อดำเนินการต่อ'));
    }else {
        echo json_encode(array('type' => 'error','title' => 'บันทึกผิดพลาด','text' => 'โปรดลองใหม่อีกครั้ง'));
    }
	}

	public function getData($id)
  {
    $data = $this->Main_model->mainGet('teacher_dtr', 't_dtr_id = '.$id);
    echo json_encode($data);
  }
	
  public function getDataSearch()
  {
      $post = $this->input->post();
      // px($post);
      $str_where = "";
      $where_arr[] = "t_dtr_status = 1";
      
      
      if (count($where_arr) > 0) {
        $str_where = implode(" AND " , $where_arr);
      }

      $data = $this->Main_model->mainGetMulti('teacher_dtr',$str_where,'t_dtr_id DESC');
      // pp_sql();
      $accrual = [];
      $reslut['table'] = [];
      $num_tr = 1;

      foreach ($data as $key => $rows) {
        $reslut['table'][] = [
          $num_tr, 
          '<a class="have-click" onclick="form_edit('.$rows['t_dtr_id'].')">'.$rows['t_dtr_date'].'</a>',  
          // $rows['t_dtr_date'],  
          $rows['t_dtr_teacher_code'], 
          $rows['t_dtr_teacher_name'],
          $rows['t_dtr_school_name'], 
          $rows['t_dtr_county_name'],
          $rows['t_dtr_total']
        ];
        $num_tr++;
      }
      echo json_encode($reslut);
  }
}
