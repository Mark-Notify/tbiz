<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MX_Controller {

	public function __construct()
	{
		// $this->load->model('user_model');
		parent::__construct();
		// check session
        checkSession();
	}

	public function index()
	{
		$data['user'] = "";
		template('Title adminLTE','index_lte', $data, 'adminLTE');
	}

	public function adminLTE()
	{
		$data['user'] = "";
		template('Title adminLTE','index_lte', $data, 'adminLTE');
	}

	public function adminMDB()
	{
		$data['user'] = "";
		template('Title adminMDB','index_mdb', $data, 'adminMDB');
	}

	public function Test()
	{
		$this->load->view('test');
	}
}
