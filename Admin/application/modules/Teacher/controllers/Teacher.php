<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teacher extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
				
		// check session
        checkSession();
        
        // load library
        // $this->load->library('UploadsFile');
	}

	public function index()
	{
		$data['county'] = $this->Main_model->mainGetMulti('county','county_status = 1');
		$data['teacher'] = $this->Main_model->mainGetMulti('teacher','teacher_status = 1');
		$data['school'] = $this->Main_model->mainGetMulti('school','school_status = 1');
		template('ทะเบียนประวัติครู','index', $data, 'adminLTE');
	}

	public function saveInsert()
	{
		$users = ssUser();
        $dateTime = mainDateTime();
        $post = $this->input->post();

        if (!empty($post['teacher_district_id'])) {
            $post['teacher_district_id'] = json_encode($post['teacher_district_id']);
        }else{
            $post['teacher_district_id'] = "";
        }
        
        $price = count($post['teacher_school_price']);
        if ($price > 1) 
        {
            $j = 1;
            for ($i=0; $i < $price; $i++) 
            { 
                $data = array(
                    // 'teacher_id' 				=> $post['teacher_id'],
                    'teacher_text_code' 		=> $post['teacher_text_code'],
                    'teacher_code' 				=> $post['teacher_code'].'-'.$j,
                    'teacher_prefix' 			=> $post['teacher_prefix'],
                    'teacher_name' 				=> $post['teacher_name'],
                    'teacher_nationality' 		=> $post['teacher_nationality'],
                    'teacher_sex' 				=> $post['teacher_sex'],
                    'teacher_age' 				=> $post['teacher_age'],
                    'teacher_birthday' 			=> $post['teacher_birthday'],
                    'teacher_mobile' 			=> $post['teacher_mobile'],
                    'teacher_address' 			=> $post['teacher_address'],
                    'teacher_email' 			=> $post['teacher_email'],
                    'teacher_line' 				=> $post['teacher_line'],
                    'teacher_text_iden' 		=> $post['teacher_text_iden'],
                    'teacher_educational' 		=> $post['teacher_educational'],
                    'teacher_faculty' 			=> $post['teacher_faculty'],
                    'teacher_visa_type' 		=> $post['teacher_visa_type'],
                    'teacher_visa_expire' 		=> $post['teacher_visa_expire'],
                    'teacher_profession_expire' => $post['teacher_profession_expire'],
                    'teacher_work_expire' 		=> $post['teacher_work_expire'],
                    'teacher_hours_per_mount' 	=> $post['teacher_hours_per_mount'],
                    'teaacher_type_salary' 		=> $post['teaacher_type_salary'],
                    'teacher_salary' 			=> DF2C($post['teacher_salary']),
                    'techer_subject' 			=> $post['techer_subject'],
                    // 'teacher_image' 			=> $post['teacher_image'],
                    'teacher_in_county' 		=> $post['teacher_in_county'],
                    'teacher_special_ability' 	=> $post['teacher_special_ability'],
                    'teacher_aptitude' 			=> $post['teacher_aptitude'],
                    'teacher_school_id' 	    => $post['teacher_school_id'][$i],
                    'teacher_school_price' 	    => DF2C($post['teacher_school_price'][$i]),
                    'teacher_district_id' 	    => $post['teacher_district_id'],
                    'teacher_bank_name' 		=> $post['teacher_bank_name'],
                    'teacher_bank_number' 		=> $post['teacher_bank_number'],
                    'staff_created_at'      	=> $dateTime,
                    'staff_updated_at'      	=> $dateTime,
                    'staff_created_id'      	=> $users['employee_id'],
                    'staff_created_name'    	=> $users['employee_username'],
                    'staff_updated_id'      	=> $users['employee_id'],
                    'staff_updated_name'    	=> $users['employee_username']
                );
                
                // px($data);
                $insert_id = $this->Main_model->mainInsert('teacher', $data, true);
                if (isset($_FILES['teacher_image'])) 
                {
                    $path = FCPATH .'uploads/techer/'.$insert_id.'/';
                    $file = $_FILES['teacher_image'];
                    $new_name = $this->uploadsfile->fileUpload($file,$path);
                    $result_img = $this->Main_model->mainUpdate('teacher', ['teacher_image'=>$new_name],['teacher_id'=>$insert_id]);
                }
                $j++;
            }
        }
        else
        {
            $data = array(
                // 'teacher_id' 				=> $post['teacher_id'],
                'teacher_text_code' 		=> $post['teacher_text_code'],
                'teacher_code' 				=> $post['teacher_code'],
                'teacher_prefix' 			=> $post['teacher_prefix'],
                'teacher_name' 				=> $post['teacher_name'],
                'teacher_nationality' 		=> $post['teacher_nationality'],
                'teacher_sex' 				=> $post['teacher_sex'],
                'teacher_age' 				=> $post['teacher_age'],
                'teacher_birthday' 			=> $post['teacher_birthday'],
                'teacher_mobile' 			=> $post['teacher_mobile'],
                'teacher_address' 			=> $post['teacher_address'],
                'teacher_email' 			=> $post['teacher_email'],
                'teacher_line' 				=> $post['teacher_line'],
                'teacher_text_iden' 		=> $post['teacher_text_iden'],
                'teacher_educational' 		=> $post['teacher_educational'],
                'teacher_faculty' 			=> $post['teacher_faculty'],
                'teacher_visa_type' 		=> $post['teacher_visa_type'],
                'teacher_visa_expire' 		=> $post['teacher_visa_expire'],
                'teacher_profession_expire' => $post['teacher_profession_expire'],
                'teacher_work_expire' 		=> $post['teacher_work_expire'],
                'teacher_hours_per_mount' 	=> $post['teacher_hours_per_mount'],
                'teaacher_type_salary' 		=> $post['teaacher_type_salary'],
                'teacher_salary' 			=> DF2C($post['teacher_salary']),
                'techer_subject' 			=> $post['techer_subject'],
                // 'teacher_image' 			=> $post['teacher_image'],
                'teacher_in_county' 		=> $post['teacher_in_county'],
                'teacher_special_ability' 	=> $post['teacher_special_ability'],
                'teacher_aptitude' 			=> $post['teacher_aptitude'],
                'teacher_school_id' 	    => $post['teacher_school_id'],
                'teacher_school_price' 	    => $post['teacher_school_price'],
                'teacher_district_id' 	    => $post['teacher_district_id'],
                'teacher_bank_name' 		=> $post['teacher_bank_name'],
                'teacher_bank_number' 		=> $post['teacher_bank_number'],
                'staff_created_at'      	=> $dateTime,
                'staff_updated_at'      	=> $dateTime,
                'staff_created_id'      	=> $users['employee_id'],
                'staff_created_name'    	=> $users['employee_username'],
                'staff_updated_id'      	=> $users['employee_id'],
                'staff_updated_name'    	=> $users['employee_username']
            );
            
            // px($data);
            $insert_id = $this->Main_model->mainInsert('teacher', $data, true);
            if (isset($_FILES['teacher_image'])) 
            {
                $path = FCPATH .'uploads/techer/'.$insert_id.'/';
                $file = $_FILES['teacher_image'];
                $new_name = $this->uploadsfile->fileUpload($file,$path);
                $result_img = $this->Main_model->mainUpdate('teacher', ['teacher_image'=>$new_name],['teacher_id'=>$insert_id]);
            }
        }
        
        if($insert_id) 
        {
            echo json_encode(array('type' => 'success','title' => 'บันทึกสำเร็จ','text' => 'กดปุ่มเพื่อดำเนินการต่อ'));
        }
        else 
        {
            echo json_encode(array('type' => 'error','title' => 'บันทึกผิดพลาด','text' => 'โปรดลองใหม่อีกครั้ง'));
        }
        
	}

	public function saveUpdate()
	{
		$users = ssUser();
        $dateTime = mainDateTime();
        $post = $this->input->post();
    
        if (isset($_FILES['teacher_image'])) 
        {
            $path = FCPATH .'uploads/techer/'.$post['teacher_id'].'/';
            if (!empty($post['teacher_image_old'])) 
            {
                $file = $path.$post['teacher_image_old'];
                $this->uploadsfile->fileRemove($file);
            }

            $file = $_FILES['teacher_image'];
            $newname = $this->uploadsfile->fileUpload($file,$path);
            $array['teacher_image'] = $newname;
        }
        
        if (!empty($post['teacher_district_id'])) {
            $post['teacher_district_id'] = json_encode($post['teacher_district_id']);
        }else{
            $post['teacher_district_id'] = "";
        }

        // px($post);
        if (count($post['teacher_school_price']) > 1) 
        {
            $j = 0;
            for ($i=1; $i <= count($post['teacher_school_price']); $i++) 
            { 
                // $teacher_code = $post['teacher_code'].'-'.$i;
                $data = array(
                    // 'teacher_id' 				=> $post['teacher_id'],
                    'teacher_text_code' 		=> $post['teacher_text_code'],
                    // 'teacher_code' 				=> $post['teacher_code'].'-'.$i,
                    'teacher_prefix' 			=> $post['teacher_prefix'],
                    'teacher_name' 				=> $post['teacher_name'],
                    'teacher_nationality' 		=> $post['teacher_nationality'],
                    'teacher_sex' 				=> $post['teacher_sex'],
                    'teacher_age' 				=> $post['teacher_age'],
                    'teacher_birthday' 			=> $post['teacher_birthday'],
                    'teacher_mobile' 			=> $post['teacher_mobile'],
                    'teacher_address' 			=> $post['teacher_address'],
                    'teacher_email' 			=> $post['teacher_email'],
                    'teacher_line' 				=> $post['teacher_line'],
                    'teacher_text_iden' 		=> $post['teacher_text_iden'],
                    'teacher_educational' 		=> $post['teacher_educational'],
                    'teacher_faculty' 			=> $post['teacher_faculty'],
                    'teacher_visa_type' 		=> $post['teacher_visa_type'],
                    'teacher_visa_expire' 		=> $post['teacher_visa_expire'],
                    'teacher_profession_expire' => $post['teacher_profession_expire'],
                    'teacher_work_expire' 		=> $post['teacher_work_expire'],
                    'teacher_hours_per_mount' 	=> $post['teacher_hours_per_mount'],
                    'teaacher_type_salary' 		=> $post['teaacher_type_salary'],
                    'teacher_salary' 			=> DF2C($post['teacher_salary']),
                    'techer_subject' 			=> $post['techer_subject'],
                    // 'teacher_image' 			=> $post['teacher_image'],
                    'teacher_in_county' 		=> $post['teacher_in_county'],
                    'teacher_special_ability' 	=> $post['teacher_special_ability'],
                    'teacher_aptitude' 			=> $post['teacher_aptitude'],
                    'teacher_school_id' 	    => $post['teacher_school_id'][$j],
                    'teacher_school_price' 	    => $post['teacher_school_price'][$j],
                    'teacher_district_id' 	    => $post['teacher_district_id'],
                    'teacher_bank_name' 		=> $post['teacher_bank_name'],
                    'teacher_bank_number' 		=> $post['teacher_bank_number'],
                    'staff_created_at'      	=> $dateTime,
                    'staff_updated_at'      	=> $dateTime,
                    // 'staff_created_id'      	=> $users['employee_id'],
                    // 'staff_created_name'    	=> $users['employee_username'],
                    'staff_updated_id'      	=> $users['employee_id'],
                    'staff_updated_name'    	=> $users['employee_username']
                );
                $result = $this->Main_model->mainUpdate('teacher', $data, 'teacher_id = '.$post['teacher_id']);
                $j++;
            }
        }
        else
        {
            $j = 0;
            $data = array(
                // 'teacher_id' 				=> $post['teacher_id'],
                'teacher_text_code' 		=> $post['teacher_text_code'],
                // 'teacher_code' 				=> $post['teacher_code'].'-'.$i,
                'teacher_prefix' 			=> $post['teacher_prefix'],
                'teacher_name' 				=> $post['teacher_name'],
                'teacher_nationality' 		=> $post['teacher_nationality'],
                'teacher_sex' 				=> $post['teacher_sex'],
                'teacher_age' 				=> $post['teacher_age'],
                'teacher_birthday' 			=> $post['teacher_birthday'],
                'teacher_mobile' 			=> $post['teacher_mobile'],
                'teacher_address' 			=> $post['teacher_address'],
                'teacher_email' 			=> $post['teacher_email'],
                'teacher_line' 				=> $post['teacher_line'],
                'teacher_text_iden' 		=> $post['teacher_text_iden'],
                'teacher_educational' 		=> $post['teacher_educational'],
                'teacher_faculty' 			=> $post['teacher_faculty'],
                'teacher_visa_type' 		=> $post['teacher_visa_type'],
                'teacher_visa_expire' 		=> $post['teacher_visa_expire'],
                'teacher_profession_expire' => $post['teacher_profession_expire'],
                'teacher_work_expire' 		=> $post['teacher_work_expire'],
                'teacher_hours_per_mount' 	=> $post['teacher_hours_per_mount'],
                'teaacher_type_salary' 		=> $post['teaacher_type_salary'],
                'teacher_salary' 			=> DF2C($post['teacher_salary']),
                'techer_subject' 			=> $post['techer_subject'],
                // 'teacher_image' 			=> $post['teacher_image'],
                'teacher_in_county' 		=> $post['teacher_in_county'],
                'teacher_special_ability' 	=> $post['teacher_special_ability'],
                'teacher_aptitude' 			=> $post['teacher_aptitude'],
                'teacher_school_id' 	    => $post['teacher_school_id'][$j],
                'teacher_school_price' 	    => $post['teacher_school_price'][$j],
                'teacher_district_id' 	    => $post['teacher_district_id'],
                'teacher_bank_name' 		=> $post['teacher_bank_name'],
                'teacher_bank_number' 		=> $post['teacher_bank_number'],
                'staff_created_at'      	=> $dateTime,
                'staff_updated_at'      	=> $dateTime,
                'staff_created_id'      	=> $users['employee_id'],
                'staff_created_name'    	=> $users['employee_username'],
                'staff_updated_id'      	=> $users['employee_id'],
                'staff_updated_name'    	=> $users['employee_username']
            );
            $result = $this->Main_model->mainUpdate('teacher', $data, 'teacher_id = '.$post['teacher_id']);
            $j++;
        }
        $result = $this->Main_model->mainUpdate('teacher', $data, 'teacher_id = '.$post['teacher_id']);
        if($result) {
            echo json_encode(array('type' => 'success','title' => 'บันทึกสำเร็จ','text' => 'กดปุ่มเพื่อดำเนินการต่อ'));
        }else {
            echo json_encode(array('type' => 'error','title' => 'บันทึกผิดพลาด','text' => 'โปรดลองใหม่อีกครั้ง'));
        }
	}

	public function getData($id)
    {
        $data = $this->Main_model->mainGet('teacher', 'teacher_id = '.$id);
        echo json_encode($data);
	}
	
    public function getDataSearch()
    {
        $post = $this->input->post();
        // px($post);
        $str_where = "";
        $where_arr[] = "teacher_status = 1";
        
        
        if (count($where_arr) > 0) {
            $str_where = implode(" AND " , $where_arr);
        }

        $data = $this->Main_model->mainGetMulti('teacher',$str_where,'teacher_id DESC');
        // pp_sql();
        $accrual = [];
        $reslut['table'] = [];
        $num_tr = 1;

        foreach ($data as $key => $rows) {
        $reslut['table'][] = [
            $num_tr, 
            '<a class="have-click" onclick="form_edit('.$rows['teacher_id'].')">'.$rows['teacher_code'].'</a>',  
            $rows['teacher_name'], 
            $rows['teacher_address'],
            $rows['teacher_mobile'], 
            $rows['teacher_text_iden'], 
            $rows['teacher_school_price'],
            $rows['techer_subject'],
            $rows['teacher_bank_name'],
            $rows['teacher_bank_number']
        ];
        $num_tr++;
        }
        echo json_encode($reslut);
    }

    public function CodeGen()
    {
        $post = $this->input->post();
        $data = $this->Main_model->mainGet('teacher', "teacher_code LIKE '%$post[value]%'", 'teacher_code DESC');
        // pp($data);
        $txt_code = "";
        $year = substr((date('Y')+543),2,2);
        if (!empty($data['teacher_code'])) 
        {
            $sub_code = explode('-',$data['teacher_code']);
            $sum_code = (int)$sub_code[1]+1;
            //   $sub_str  = substr($data['teacher_code'],2,6);
            //   pp($sum_code);
            $txt_code .= $sub_code[0]."-".sprintf('%05d',$sum_code);
        }
        else
        {
            $txt_code .= $post['value']."-".sprintf('%05d','1');
        }
        
        echo json_encode(['code'=>$txt_code]);
    }

    public function saveSchool()
    {
        $post = $this->input->post();
        // pp($post);
        foreach ($post['school_id'] as $key => $value) 
        {
            $data = [
                'school_name'       => $post['school_name'][$key],
                'school_name_en'    => $post['school_name_en'][$key]
            ];
            $result = $this->Main_model->mainUpdate('school', $data, 'school_id = '.$post['school_id'][$key]);
        }
        if($result) {
            echo json_encode(array('type' => 'success','title' => 'บันทึกสำเร็จ','text' => 'กดปุ่มเพื่อดำเนินการต่อ'));
        }else {
            echo json_encode(array('type' => 'error','title' => 'บันทึกผิดพลาด','text' => 'โปรดลองใหม่อีกครั้ง'));
        }
    }

    public function saveCounty()
    {
        $post = $this->input->post();
        // pp($post);
        foreach ($post['county_id'] as $key => $value) 
        {
            $data = [
                'county_name'       => $post['county_name'][$key],
                'county_name_en'    => $post['county_name_en'][$key]
            ];
            $result = $this->Main_model->mainUpdate('county', $data, 'county_id = '.$post['county_id'][$key]);
        }
        if($result) {
            echo json_encode(array('type' => 'success','title' => 'บันทึกสำเร็จ','text' => 'กดปุ่มเพื่อดำเนินการต่อ'));
        }else {
            echo json_encode(array('type' => 'error','title' => 'บันทึกผิดพลาด','text' => 'โปรดลองใหม่อีกครั้ง'));
        }
    }
}
