<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6 d-flex">
        <h1 class="m-0 text-dark">ทะเบียนประวัติครู</h1>
        <button type="button" class="btn btn-outline-warning ml-2" onclick="setting_school()" title="ตั้งค่าชือโรงเรียน"><i class="fas fa-cogs"></i></button>
        <button type="button" class="btn btn-outline-success ml-2" onclick="setting_county()" title="ตั้งค่าชือเขต"><i class="fas fa-cogs"></i></button>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">ทะเบียนประวัติครู</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <section class="col-lg-12 connectedSortable">
        <div class="card card-success card-outline">
          <div class="card-header">
            <h3 class="card-title">
              <i class="fas fa-chart-pie mr-1"></i>
              ข้อมูล
            </h3>
            <div class="card-tools">
              <button type="button" class="btn btn-primary" onclick="form_add(this)">สร้างข้อมูล</button>
            </div>

          </div>            

          <div class="card-body">

            <div class="row">
              <div class="table-responsive">
                <table class="table table-bordered table-striped" id="table_income">
                  <thead>
                    <tr align="center">
                      <th>#</th>
                      <th width="10%">รหัสครู</th>
                      <th width="10%">ชื่อครู</th>
                      <th width="10%">ที่อยู่</th>
                      <th width="10%">เบอร์โทร</th>
                      <th width="10%">เลขบัตรประชาชน</th>
                      <th width="10%">อัตรค่าสอน</th>
                      <th width="10%">วิชาที่สอน</th>
                      <th width="10%">ชื่อธนาคาร</th>
                      <th width="10%">เลขที่บัญชี</th>
                    </tr>
                  </thead>
                  <tbody>
                  
                  </tbody>
                </table>
              </div>
            </div>

          </div>
          
        </div>
      </section>
      
    </div>
  </div>
</section>

<?php echo $this->load->view('modal') ?>
<?php echo $this->load->view('modal_school') ?>
<?php echo $this->load->view('modal_county') ?>
<?php echo $this->load->view('jquery') ?>