<div class="modal fade bd-example-modal-lg" id="input_model_school" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">
      <form id="form_input_school">
        <div class="modal-header">
          <h4 class="modal-title" id="head_modal">สร้างข้อมูล</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <button type="button" id="btnGoBottom2" class="btn btn-warning" onclick="bottomFunction2()"><i class="fas fa-chevron-down"></i></button>
        <button type="button" id="btnGoTop2" class="btn btn-warning" onclick="topFunction2()"><i class="fas fa-chevron-up"></i></button>

        <div class="modal-body">
          <button type="button" class="btn btn-success mb-2 float-right" onclick="form_save_school()">บันทึก</button>
          <table class="table table-striped">
            <thead>
              <tr>
                <th width="5%" scope="col">#</th>
                <th width="45%" scope="col">ชื่อโรงเรียน</th>
                <th width="45%" scope="col">ชื่อโรงเรียนภาษาอังกฤษ</th>
              </tr>
            </thead>
            <tbody>
            <?php
              if (!empty($school)) {
                $i=1;
                foreach ($school as $key => $rows) {
            ?>
              <input type="hidden" name="school_id[]" value="<?php echo $rows['school_id']; ?>">
              <tr>
                <th scope="row"><?php echo $i++; ?></th>
                <td>
                  <input type="text" class="form-control" name="school_name[]" value="<?php echo $rows['school_name']; ?>">
                </td>
                <td>
                  <input type="text" class="form-control" name="school_name_en[]" value="<?php echo $rows['school_name_en']; ?>">
                </td>
              </tr>
            <?php
                }
              }
            ?>
            </tbody>
          </table>
          
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
          <div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="fas fa-user-check"></i>
                </span>
              </div>
              <input type="text" class="form-control text-center" name="staff_created_name" id="staff_created_name" placeholder="" readonly>
            </div>
          </div>
          <button type="button" class="btn btn-success" onclick="form_save_school()">บันทึก</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
$('#input_model_school').on('scroll', function() {
    if($(this).scrollTop() > 5){
      $('#btnGoTop2').show(200);
    }else{
      $('#btnGoTop2').hide(200);
      $('#btnGoBottom2').show(200);
    }
  });

function topFunction2() 
{
  $('#input_model_school').scrollTop(0);
}

function bottomFunction2() 
{
  var height = $("#input_model_school")[0].scrollHeight;
  $('#input_model_school').scrollTop(height);
  setTimeout(function(){ $('#btnGoBottom2').hide(200); }, 100);
}

function form_save_school() 
{
  var id = $('#income_school_id option:selected').val();
  var res = main_save('<?php echo base_url('Teacher/saveSchool') ?>','form_input_school');
  res_swal(res, 2);
}
</script>