<div class="modal fade" id="input_model">
  <div class="modal-dialog modal-xxl modal-dialog-centered" role="document">
    <form id="input_form">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="head_modal">สร้างข้อมูล</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          
          <fieldset class="border-danger mb-2">
            <legend>ทะเบียนประวัติครู</legend>
            <div class="row p-2">
              
              <input type="hidden" name="teacher_id" id="teacher_id" value="0">

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="teacher_text_code"> คำนำหน้ารหัส</label>
                  <select name="teacher_text_code" id="teacher_text_code" class="form-control" onchange="change_text_code()">
                    <option value="CH" data-subject="ภาษาจีน">ครูจีน (CH)</option>
                    <option value="EN" data-subject="ภาษาอังกฤษ">ครูอังกฤษ (EN)</option>
                    <option value="JP" data-subject="ภาษาญี่ปุ่น">ครูญี่ปุ่น (JP)</option>
                  </select>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="teacher_code">รหัสครู <span style="color:red">*</span></label>
                  <input type="text" class="form-control" id="teacher_code" name="teacher_code" required readonly>
                  <div class="valid-feedback">สามารถใช้งานได้</div>
                  <div class="invalid-feedback">กรุณากรอกข้อมูล</div>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="teacher_prefix">คำนำหน้าชื่อ</label>
                  <select name="teacher_prefix" id="teacher_prefix" class="form-control" onchange="change_prefix()">
                    <!-- <option value="" selected>เลือกคำนำหน้า</option> -->
                    <option value="1">นาย</option>
                    <option value="2">นาง</option>
                    <option value="3">นางสาว</option>
                  </select>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="teacher_name">ชื่อครู <span style="color:red">*</span></label>
                  <input type="text" class="form-control" id="teacher_name" name="teacher_name" required>
                  <div class="valid-feedback">สามารถใช้งานได้</div>
                  <div class="invalid-feedback">กรุณากรอกข้อมูล</div>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="teacher_nationality">สัญชาติ</label>
                  <input type="text" class="form-control" id="teacher_nationality" name="teacher_nationality">
                </div>
              </div>
              
              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="teacher_sex">เพศ</label>
                  <select name="teacher_sex" id="teacher_sex" class="form-control">
                    <option value="1">ชาย</option>
                    <option value="2">หญิง</option>
                  </select>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="teacher_age"> อายุ</label>
                  <input type="text" class="form-control text-center" max="3" id="teacher_age" name="teacher_age">
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="teacher_birthday"> วันเกิด</label>
                  <input type="text" class="form-control text-center datepicker" id="teacher_birthday" name="teacher_birthday">
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="teacher_mobile"> เบอร์โทร</label>
                  <input type="text" class="form-control" id="teacher_mobile" name="teacher_mobile">
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="teacher_address"> ที่อยู่</label>
                  <textarea name="teacher_address" id="teacher_address" class="form-control"></textarea>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="teacher_email"> อีเมล</label>
                  <input type="text" class="form-control" id="teacher_email" name="teacher_email">
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="teacher_line"> ไอดีไลน์</label>
                  <input type="text" class="form-control" id="teacher_line" name="teacher_line">
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="teacher_text_iden"> เลขบัตรประชาชน</label>
                  <input type="text" class="form-control" id="teacher_text_iden" name="teacher_text_iden">
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="teacher_educational"> วุฒิการศึกษา</label>
                  <input type="text" class="form-control" id="teacher_educational" name="teacher_educational">
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="teacher_faculty"> สาขา</label>
                  <input type="text" class="form-control" id="teacher_faculty" name="teacher_faculty">
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="teacher_visa_type"> ประเภทวีซ่า</label>
                  <input type="text" class="form-control" id="teacher_visa_type" name="teacher_visa_type">
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="teacher_visa_expire"> วันหมดอายุวีซ่า</label>
                  <input type="text" class="form-control text-center datepicker" id="teacher_visa_expire" name="teacher_visa_expire">
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="teacher_profession_expire"> วันหมดอายุใบประกอบวิชาชีพครู</label>
                  <input type="text" class="form-control text-center datepicker" id="teacher_profession_expire" name="teacher_profession_expire">
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="teacher_work_expire"> วันหมดอายุใบอนุญาติทำงาน</label>
                  <input type="text" class="form-control text-center datepicker" id="teacher_work_expire" name="teacher_work_expire">
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="teacher_hours_per_mount"> ชม. สอนต่อเดือน</label>
                  <input type="text" class="form-control text-center" id="teacher_hours_per_mount" name="teacher_hours_per_mount">
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <label class="l-light">รูปแบบเงินเดือน</label>
                <div class="form-group mt-2 mr-2">
                  <div class="icheck-success d-inline">
                    <input type="radio" id="teaacher_type_salary_1" name="teaacher_type_salary" value="1" checked>
                    <label class="l-light" for="teaacher_type_salary_1"> รายเดือน  </label>
                  </div>
                  <div class="icheck-success d-inline">
                    <input type="radio" id="teaacher_type_salary_2" name="teaacher_type_salary" value="2">
                    <label class="l-light" for="teaacher_type_salary_2"> รายชม.</label>
                  </div>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="teacher_salary"> เงินเดือน (รายเดือน/รายชม.)</label>
                  <input type="text" class="form-control" id="teacher_salary" name="teacher_salary">
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="techer_subject"> วิชาที่สอน</label>
                  <input type="text" class="form-control" id="techer_subject" name="techer_subject">
                </div>
              </div>
              
              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="teacher_district_id"> ชื่อเขตที่ไปสอน</label>
                  <select name="teacher_district_id[]" id="teacher_district_id" multiple class="form-control selectpicker_all  show-menu-arrow" data-size="8" data-live-search="true" title="กรุณาเลือก">
                    <?php
                      if (!empty($county)) 
                      {
                        foreach ($county as $key => $value) 
                        {
                          echo '<option value="'.$value['county_id'].'">'.$value['county_name'].'</option>';
                        }
                      }
                    ?>
                  </select>
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2" id="school_price">
                <div class="form-group">
                  <label class="l-light" for="teacher_school_id"> ชื่อโรงเรียนที่ไปสอน</label>
                  <select name="teacher_school_id[]" id="teacher_school_id" multiple class="form-control selectpicker_all  show-menu-arrow" onchange="change_teacher_school_id(this)" data-size="8" data-live-search="true" title="กรุณาเลือก">

                    <?php
                      if (!empty($school)) 
                      {
                        foreach ($school as $key => $value) 
                        {
                          echo '<option value="'.$value['school_id'].'">'.$value['school_name'].'</option>';
                        }
                      }
                    ?>
                  </select>
                </div>
              </div>
              
              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <label class="l-light" for="teacher_image"> รูปถ่าย</label>
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="teacher_image" name="teacher_image" lang="en">
                  <label class="custom-file-label" for="teacher_image">กรุณาเลือกรูปถ่าย</label>
                </div>
                <input type="hidden" name="teacher_image_old" id="teacher_image_old">
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="teacher_in_county"> ประสบการณ์สอนในประเทศ</label>
                  <input type="text" class="form-control" id="teacher_in_county" name="teacher_in_county">
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="teacher_special_ability"> ความสามารถพิเศษ</label>
                  <input type="text" class="form-control" id="teacher_special_ability" name="teacher_special_ability">
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="teacher_aptitude"> ความถนัดในการสอน (อนุบาล/ประถม/มัธยม)</label>
                  <input type="text" class="form-control" id="teacher_aptitude" name="teacher_aptitude">
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="teacher_bank_name"> ชื่อธนาคาร</label>
                  <input type="text" class="form-control" id="teacher_bank_name" name="teacher_bank_name">
                </div>
              </div>

              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2">
                <div class="form-group">
                  <label class="l-light" for="teacher_bank_number"> เลขที่บัญชี</label>
                  <input type="text" class="form-control" id="teacher_bank_number" name="teacher_bank_number">
                </div>
              </div>

            </div>
          </fieldset>

        </div>
            
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
          <div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="fas fa-user-check"></i>
                </span>
              </div>
              <input type="text" class="form-control text-center" name="staff_created_name" id="staff_created_name" placeholder="" readonly>
            </div>
          </div>
          <button type="button" class="btn btn-success" onclick="form_save()">บันทึก</button>
        </div>
      </div>
    </form>
  </div>
</div>