<div class="modal fade bd-example-modal-lg" id="input_model_county" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">
      <form id="form_input_county">
        <div class="modal-header">
          <h4 class="modal-title" id="head_modal">สร้างข้อมูล</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <button type="button" id="btnGoBottom" class="btn btn-warning" onclick="bottomFunction()"><i class="fas fa-chevron-down"></i></button>
        <button type="button" id="btnGoTop" class="btn btn-warning" onclick="topFunction()"><i class="fas fa-chevron-up"></i></button>

        <div class="modal-body">
          <button type="button" class="btn btn-success mb-2 float-right" onclick="form_save_county()">บันทึก</button>
          <table class="table table-striped">
            <thead>
              <tr>
                <th width="5%" scope="col">#</th>
                <th width="45%" scope="col">ชื่อเขต</th>
                <th width="45%" scope="col">ชื่อเขตภาษาอังกฤษ</th>
              </tr>
            </thead>
            <tbody>
            <?php
              if (!empty($county)) {
                $i=1;
                foreach ($county as $key => $rows) {
            ?>
              <input type="hidden" name="county_id[]" value="<?php echo $rows['county_id']; ?>">
              <tr>
                <th scope="row"><?php echo $i++; ?></th>
                <td>
                  <input type="text" class="form-control" name="county_name[]" value="<?php echo $rows['county_name']; ?>">
                </td>
                <td>
                  <input type="text" class="form-control" name="county_name_en[]" value="<?php echo $rows['county_name_en']; ?>">
                </td>
              </tr>
            <?php
                }
              }
            ?>
            </tbody>
          </table>
          
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
          <div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="fas fa-user-check"></i>
                </span>
              </div>
              <input type="text" class="form-control text-center" name="staff_created_name" id="staff_created_name" placeholder="" readonly>
            </div>
          </div>
          <button type="button" class="btn btn-success" onclick="form_save_county()">บันทึก</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
$('#input_model_county').on('scroll', function() {
    if($(this).scrollTop() > 5){
      $('#btnGoTop').show(200);
    }else{
      $('#btnGoTop').hide(200);
      $('#btnGoBottom').show(200);
    }
  });

function topFunction() 
{
  $('#input_model_county').scrollTop(0);
}

function bottomFunction() 
{
  var height = $("#input_model_county")[0].scrollHeight;
  $('#input_model_county').scrollTop(height);
  setTimeout(function(){ $('#btnGoBottom').hide(200); }, 100);
}

function form_save_county() 
{
  var id = $('#income_county_id option:selected').val();
  var res = main_save('<?php echo base_url('Teacher/saveCounty') ?>','form_input_county');
  res_swal(res, 2);
}
</script>