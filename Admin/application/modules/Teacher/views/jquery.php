<script>
$(function() {
  main_loading(500, function(){

    var data = main_post('<?php echo base_url('Teacher/getDataSearch') ?>');
    set_table(data['table']);
    // set_info(data['info']);
  });
})

function form_add(elm)
{
  form_reset();
  $('#input_model').modal('show');
  $('#teacher_text_code').change();
  $('#input_model #head_modal').html($(elm).html());
}

function form_save()
{
  main_loading(500, function(){
    var head_id = $('#teacher_id').val();
    if(main_validated('input_form'))
    {
      if((head_id==0) || (head_id==''))//เพิ่ม
      {
        var res = main_save('<?php echo base_url('Teacher/saveInsert') ?>', 'input_form');
        res_swal(res, 2);
      }
      else//แก้ไข
      {
        var res = main_save('<?php echo base_url('Teacher/saveUpdate') ?>', 'input_form');
        res_swal(res, 2);
      }
      get_data_search();
    }
  });
}

function form_edit(id) 
{
  form_reset();

  $('#input_model').modal('show');
  $('#head_modal').html('<i class="fas fa-folder-plus"></i> แก้ไขข้อมูล');

  main_loading(500, function(){
    form_insert_data(id);
  });
}

// นำเข้าข้อมูลไปใส่ฟอร์ม
function form_insert_data(id) 
{
  var data = main_data_to_form('<?php echo base_url('Teacher/getData/') ?>', id);
  $.each(data, function(key, val) {
	//   cc(key+"=="+val)
	  if (key == 'teacher_image') 
	  {
		$('#teacher_image_old').val(val);
	  }
	  else if(key == 'teaacher_type_salary')
	  {
		$('[name="teaacher_type_salary"][value="'+val+'"]').prop('checked',true);
	  }
	  else if(key == 'teacher_school_id' && val != "")
	  {
		$('#teacher_school_id').selectpicker();
		$('#teacher_school_id').selectpicker('val', JSON.parse(val));
	  }
	  else if(key == 'teacher_district_id' && val != "")
	  {
		$('#teacher_district_id').selectpicker();
		$('#teacher_district_id').selectpicker('val', JSON.parse(val));
	  }
	  else if(key == 'teacher_school_price')
	  {
		$('.remove_price').remove();
		var html = 
			`<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2 remove_price">
				<div class="form-group">
					<label class="l-light" for="teacher_school_price"> ค่าสอนโรงเรียนที่ 1</label>
					<input type="text" class="form-control" name="teacher_school_price[]" required value="`+val+`">
				</div>
			</div>`;
		$('#school_price').after(html);
	  }
	  else
	  {
		$('#'+key).val(val);
	  }
  });
// cc(teacher_district_id)
}

function form_reset() 
{
  // หนึงรายการ
  $('#input_form')[0].reset();  // เคลียร์ฟอร์ม
  $('#input_form').removeClass('was-validated');
  $('#teacher_id').val(0); // ไอดี 0
  $('.selectpicker_all').selectpicker('refresh');
}

function get_data_search() 
{
  var data = {
    mount:$('#input_search_mount option:selected').val(),
    accrual:$('#search_accrual_money:checked').val(),
  }
  var data = main_post('<?php echo base_url('Teacher/getDataSearch') ?>',data);
  update_table(data['table'])
//   set_info(data['info']);
}

function set_table(data) 
{  
	DATA_1 = $('#table_income').DataTable({
		data: data,
		order: [[ 0, "asc" ]],
		columns: [
			{ 
				width: "5%", 
				className: "text-center",
				orderable: false
			},
			{ 
				width: "8%", 
				className: "text-center",
			},
			{ 
				width: "12%", 
				className: "text-left",
			},
			{ 
				width: "10%", 
				className: "text-left",
			},
			{ 
				width: "10%", 
				className: "text-center",
			},
			{ 
				width: "10%", 
				className: "text-center",
			},
			{ 
				width: "10%", 
				className: "text-center",
			},
			{ 
				width: "10%", 
				className: "text-left",
			},
			{ 
				width: "10%", 
				className: "text-center",
			},
			{ 
				width: "10%", 
				className: "text-center",
			}
		],
		pageLength: 10,
		// pagingType: "numbers",
		// dom: 'plrtip',
		// searching: false,
		lengthMenu: [10,25,50,100],
		oLanguage: {
			sSearchPlaceholder: "ค้นหา",
			sLengthMenu: "แสดง _MENU_ รายการ",
			sSearch: "ค้นหา",
			sInfo: "กำลังแสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
			sInfoEmpty: "กำลังแสดง 0 ถึง 0 จาก 0 รายการ",
			sZeroRecords: "ไม่มีข้อมูลในตาราง",
			sProcessing: "กำลังค้นหา",
		}
	});
}

function update_table(data)
{
	DATA_1.clear();
	DATA_1.rows.add(data);
	DATA_1.draw();
}

function set_info(data) 
{
  $('#balance_money').text(data['accrual']+" บาท");
}

function change_text_code() 
{
	var text 	= $('#teacher_text_code option:selected').data('subject');
	var value 	= $('#teacher_text_code option:selected').val();
	$('#techer_subject').val(text);

	var res = main_post('<?php echo base_url('Teacher/CodeGen') ?>',{value:value});
	$('#teacher_code').val(res['code']);
}

function change_prefix() 
{
	var id = $('#teacher_prefix option:selected').val();
	if (id == 1) 
	{
		$('#teacher_sex').val(1);
	}
	else
	{
		$('#teacher_sex').val(2);
	}
}

function setting_school() 
{
	$('#input_model_school').modal('show');
}

function setting_county() 
{
	$('#input_model_county').modal('show');
}

function change_teacher_school_id(elm) 
{
	// cc($(elm).val())
	var school = $(elm).val();
	$('.remove_price').remove();
	var html = "";
	for (let i = 1; i <= school.length; i++) 
	{
		html += 
		`<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-2 remove_price">
			<div class="form-group">
				<label class="l-light" for="teacher_school_price"> ค่าสอนโรงเรียนที่ `+i+`</label>
				<input type="text" class="form-control" name="teacher_school_price[]" required>
			</div>
		</div>`;
	}
	$('#school_price').after(html);
}
</script>