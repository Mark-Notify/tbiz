<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Income_model extends MY_Model
{
	public function __construct()
	{
		$this->table = 'income';
		$this->primary_key = 'income_id';
		$this->return_as = 'array';

		// $this->has_one[''] = '';
		// $this->has_many['posts'] = 'Post_model';

		parent::__construct();
	}
}

?>