<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Main_model extends CI_Model
{
    //----------------------------------------------START Main--------------------------------------------------------//

    public function mainInsert($table, $data, $insert_id=false)
    {
        $res = $this->db->insert($table,$data);

        if($insert_id == true)
        {
            $res = $this->db->insert_id();
        }

        return $res;
    }

    public function mainInsertMulti($table, $data)
    {
        $res = $this->db->insert_batch($table,$data);

        return $res;
    }

    public function mainGet($table, $where=null, $order_by=null)
    {
        if($where!=null)
        {
            $this->db->where($where);
        }
        if($order_by!=null)
        {
            $this->db->order_by($order_by);
        }
        $res = $this->db->get($table);
        return $res->row_array();
    }

    public function mainGetMulti($table, $where=null, $order_by=null)
    {
        if($where!=null)
        {
            $this->db->where($where);
        }
        if($order_by!=null)
        {
            $this->db->order_by($order_by);
        }
        $res = $this->db->get($table);
        // pp_sql();
        return $res->result_array();
    }

    public function mainGetFull($select, $table, $where=null, $order_by=null)
    {
        $this->db->select($select);
        if($where!=null)
        {
            $this->db->where($where);
        }
        if($order_by!=null)
        {
            $this->db->order_by($order_by);
        }
        $result = $this->db->get($table);
        return $result->row_array();
    }

    public function mainGetFullMulti($select, $table, $where=null, $group_by=null, $order_by=null, $limit=null, $object=false)
    {
        $this->db->select($select);
        if($where!=null)
        {
            $this->db->where($where);
        }
        if($group_by!=null)
        {
            $this->db->group_by($group_by);
        }
        if($order_by!=null)
        {
            $this->db->order_by($order_by);
        }
        if($limit!=null)
        {
            $this->db->limit($limit);
        }
        $result = $this->db->get($table);

        if($object)
        {
            return $result->result_object();
        }
        else
        {
            return $result->result_array();
        }
    }

    public function mainGetDB($data)
    {
        if(!empty($data['select']))
        {
            $this->db->select($data['select']);
        }
        if(!empty($data['where']))
        {
            $this->db->where($data['where']);
        }
        if(!empty($data['group_by']))
        {
            $this->db->group_by($data['group_by']);
        }
        if(!empty($data['order_by']))
        {
            $this->db->order_by($data['order_by']);
        }
        if(!empty($data['limit']))
        {
            $this->db->limit($data['limit']);
        }

        if(!empty($data['join']))
        {
            if(is_array($data['join'][0]))
            {
                foreach ($data['join'] as $key => $value) 
                {
                    if(empty($value[2]))
                    {
                        $value[2] = null;
                    }
        
                    $this->db->join($value[0], $value[1], $value[2]);
                }
            }
            else
            {
                if(empty($data['join'][2]))
                {
                    $data['join'][2] = null;
                }
    
                $this->db->join($data['join'][0], $data['join'][1], $data['join'][2]);
            }
        }

        if(!empty($data['from']))
        {
            $result = $this->db->from($data['from']);
        }

        if(!empty($data['result']))
        {
            $result = $this->db->get();

            if($data['result'] == 'result_object')
            {
                return $result->result_object();
            }
            else if($data['result'] == 'result_array')
            {
                return $result->result_array();
            }
            else if($data['result'] == 'row_object')
            {
                return $result->row_object();
            }
            else if($data['result'] == 'row_array')
            {
                return $result->row_array();
            }
        }
        else
        {
            return $this->db->last_query();
        }
    }

    public function mainUpdate($table, $data, $where=null)
    {
        if($where!=null)
        {
            $this->db->where($where);
        }
        $res = $this->db->update($table,$data);
        return $res;
    }

    public function mainUpdateMuti($table, $data, $where=null)
    {
        if($where!=null)
        {
            $this->db->where($where);
        }
        $res = $this->db->update_batch($table,$data);
        return $res;
    }

    public function mainDelete($table, $where=null)
    {
        if($where==null)
        {
            $res = $this->db->empty_table($table);
        }
        else
        {
            $this->db->where($where);
            $res = $this->db->delete($table);
        }

        return $res;
    }

    //----------------------------------------------END Main-------------------------------------------//


    //----------------------------------------------START Codegen-------------------------------------------//
    public function getDocCodeGen($table,$order_by)
    {
        $this->db->order_by($order_by, "DESC");
        $result = $this->db->get($table);
        return $result->row_array();
    }

    public function setDocCodeGen($table,$date_set,$where)
    {
        $this->db->where($where);
        return $this->db->update($table,$date_set);
    }

    public function newDocCodeGen($table,$date_set)
    {
        return $this->db->insert($table,$date_set);
    }

    //----------------------------------------------END Codegen-------------------------------------------//


    //----------------------------------------------START Codegen-------------------------------------------//
    
    // ใช้ตอนค้นหา
    public function autoSearch($table, $where, $text, $likeArr, $limit=5)
    {
        $this->db->group_start();
        foreach ($likeArr as $key => $value) 
        {
            if($key==0)
            {
                $this->db->like($value, $text, "both");
            }
            else
            {
                $this->db->or_like($value, $text, "both");
            }
        }
        $this->db->group_end();
        if ($where != "") {
            $this->db->where($where);
        }
        $this->db->limit($limit);
        $result = $this->db->get($table);
        return $result->result_array();
    }

    // ใช่เมื่อกดเลือกแล้ว
    public function autoSelected($select, $table, $where)
    {
        $this->db->select($select);
        $this->db->where($where);
        $result = $this->db->get($table);
        return $result->row_array();
    }
    
    //----------------------------------------------END Autocomplete-------------------------------------------//

}
?>