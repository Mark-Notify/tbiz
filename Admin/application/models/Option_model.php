<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Option_model extends CI_Model
{
    public function optionTeacher()
    {
        $result = $this->db->where(['teacher_status' => 1])->get('teacher')->result_array();
        return $result;
    }

    public function optionCounty()
    {
        $result = $this->db->where(['county_status' => 1])->get('county')->result_array();
        $result_arr = [];
        if (!empty($result))
        {
            foreach ($result as $key => $value) 
            {
                $result_arr[$value['county_id']] = $value['county_name'];
            }
        }
        return $result_arr;
    }

    public function optionSchool()
    {
        $result = $this->db->where(['school_status' => 1])->get('school')->result_array();
        $result_arr = [];
        if (!empty($result))
        {
            foreach ($result as $key => $value) 
            {
                $result_arr[$value['school_id']] = $value['school_name'];
            }
        }
        return $result_arr;
    }
}
?>