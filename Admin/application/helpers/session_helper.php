<?php
// ---------------- session ------------------ //
function ssUser($request=null)
{
  $user = false;
  if(!empty($_SESSION['user']))
  {
    if($request==null)
    {
      $user = $_SESSION['user'];
    }
    else
    {
      $user = $_SESSION['user'][$request];
    }
  }
  return $user;
}

function ssLang($full=true)
{
  $lang = false;
  if(!empty($_SESSION['lang']))
  {
    if($full==true)
    {
      $lang = $_SESSION['lang']['full_country'];
    }
    else
    {
      $lang = $_SESSION['lang']['country'];
    }
  }
  return $lang;
}

function ssActive($lifetime=60)
{
  // $lifetime = กำหนดเวลาที่สามารถอยู่ในระบบ (นาที)
  
  if(isset($_SESSION['time_active']))
  {
    $seclogin = (time()-$_SESSION['time_active'])/60;

    //หากไม่ได้ Active ในเวลาที่กำหนด
    if($seclogin > $lifetime)
    {
      return false;
    }
    else
    {
      $_SESSION['time_active'] = time();
      return true;
    }
  }
  else
  {
    $_SESSION['time_active'] = time();
    return true;
  }
}

// online หรือไม่
function ssOnline()
{
  $ci =& get_instance();
  $user = ssUser();
  $online = $ci->db->where(['employee_id'=>$user['employee_id']])->get('online');
  return $online->num_rows();
}

// จำกัดเวลาในการเข้าสู่ระบบ
function ssTimer()
{
  $ci =& get_instance();
  $user = ssUser();
  $maintime = mainTime();
  $time = $ci->db->where(['employee_timer_id'=>$user['employee_timer_id']])->get('employee_timer');
  $result = $time->row_array();
  $in = TimeDiff($maintime,$result['employee_timer_in']);
  $out = TimeDiff($maintime,$result['employee_timer_out']);

  if ($result['employee_timer_id'] != 1) 
  {
    if ($result['employee_timer_allday'] != 1) 
    {
      // เวลาเข้าน้อยกว่า 0 แปลว่า เลยเวลาเข้างานแล้ว
      // เวลาออกมากกว่า 0 แปลว่า ยังไม่ถึงเวลาออก
      if ($in < 0 && $out > 0)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    else
    {
      return true;
    }
  }
  else
  {
    return true;
  }
}

function checkSession()
{
  // ถ้าไม่มีการเข้าสู่ระบบ ให้ไป login ใหม่
  $users = ssUser();
  if(!ssUser())
  {
    if(!empty($users['employee_id']))
    {
      redirect('Income');
    }
    else
    {
      redirect('User');
    }
  }
}

function checkTeacherSession()
{
  // ถ้าไม่มีการเข้าสู่ระบบ ให้ไป login ใหม่
  $users = ssUser();
  if(!ssUser())
  {
    if(!empty($users['teacher_id']))
    {
      redirect('TeacherDTR');
    }
    else
    {
      redirect('User');
    }
  }
}

function ssGetSearch($permission_id, $return_array=false)
{
  if($return_array==false)
  {
    $where = $_SESSION['search'][$permission_id];
    if (count($where) > 0) 
    {
      $str_where = implode(" AND " , $where);
    }
    else
    {
      $str_where = null;
    }

    return $str_where;
  }
  else
  {
    return $_SESSION['search'][$permission_id];
  }
}

function ssSetSearch($permission_id, $where)
{
  $_SESSION['search'][$permission_id] = $where;
}

function ssAddSearch($permission_id, $where)
{
  $_SESSION['search'][$permission_id] = array_merge($_SESSION['search'][$permission_id], $where);
}
