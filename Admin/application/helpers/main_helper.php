<?php
// แปลภาษา
function label($key)
{
	$ci =& get_instance();
	$result = $ci->lang->line($key);
	if($result) 
	{
    return $result;
	}
	else
	{
		return $key;
	}
}

// ปริ้น array object อื่นๆ
function pp($array)
{
	echo '<pre>'; print_r($array); echo '</pre>';
}

// ปริ้น array object อื่นๆ แล้วหยุดทำงาน
function px($array)
{
  echo '<pre>'; print_r($array); echo '</pre>';
  exit;
}

function pp_sql()
{
  $ci =& get_instance();
  echo $ci->db->last_query();
}

function myPassword($password)
{
  return trim(md5(md5($password.'MARK')));
}

function template($title, $view, $data=[], $theme=null)
{
	$ci =& get_instance();

	if($theme==null)
	{
		$theme='adminLTE';// default theme
	}

	$data['theme'] 		= $theme;
	$data['title'] 		= $title;
	$data['content']	= $view;
	$data['assets']		= APPPATH."views/$theme/assets/";
	
	$ci->load->view("$theme/template", $data);
}

// ตัดคอมม่า
function DF2C($string)
{
  return str_replace(",","",$string);
}

// ทศนิยม 2 ตำแหน่ง
function F2C($string)
{
  if ($string==='' || $string===null) 
  {
    return $string;
  }
  else 
  {
    return number_format($string, 2);  
  }
}

// คอมม่า
function FC($string)
{
  if (empty($string)) 
  {
    return $string;
  } 
  else
  {
    return number_format($string);
  }
}

// ลบทั้งโฟเดอร์
function rmFolder($dir) 
{
  foreach(glob($dir . '/*') as $file) 
  {
    if(is_dir($file)){
      rmFolder($file);
    }else{
      unlink($file);
    }
  }
  if (is_dir($dir)) 
  {
    rmdir($dir);
  }
}

// ลบภายในโฟเดอร์
function rmInFolder($dir)
{
  foreach (scandir($dir) as $key => $value) 
  {
    if (is_dir($dir.$value) && ($value != '.') && ($value != '..')) 
    {
      rmFolder($dir.$value);
    }
  }
}

// เช็ครูปภาพ
function checkImage($src = null)
{
  $path = base_url('assets/images/no-image.jpg');
  if ($src == null) 
  {
    return $path;
  }
  else
  {
    if (checkFile($src)) 
    {
      return base_url($src);
    }
    else
    {
      return $path;
    }
  }
}

function checkFile($dir)
{
  if (file_exists($dir) && is_file($dir)) 
  {
    return true;
  }
  else
  {
    return false;
  }
}

function teacher($teacher_id=null)
{
  $implode = "";
  if ($teacher_id != null) {
    $id = json_decode($teacher_id);
  $implode = implode("," , $id);
  }
  
  $ci =& get_instance();
  if ($implode != "") {
    return $ci->Main_model->mainGetMulti("teacher","teacher_status = 1 AND teacher_id IN ($implode)");
  }else{    
    return $ci->Main_model->mainGetMulti("teacher","teacher_status = 1");
  }
}

function school($school_id=null)
{
  $implode = "";
  if ($school_id != null) {
    $id = json_decode($school_id);
    $implode = implode("," , $id);
  }
  
  $ci =& get_instance();
  if ($implode != "") {
    return $ci->Main_model->mainGetMulti("school","school_status = 1 AND school_id IN ($implode)");
  }else{    
    return $ci->Main_model->mainGetMulti("school","school_status = 1");
  }
}

function county($county_id=null)
{
  $implode = "";
  if ($county_id != null) {
    $id = json_decode($county_id);
    $implode = implode("," , $id);
  }
  
  $ci =& get_instance();
  if ($implode != "") {
    return $ci->Main_model->mainGetMulti("county","county_status = 1 AND county_id IN ($implode)");
  }else{
    return  $ci->Main_model->mainGetMulti("county","county_status = 1");;
  }
}
?>