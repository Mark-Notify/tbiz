<?php
// หาจำนวนวันที่ต่างกัน
function dateDiff($date_1, $date_2)
{
  $ts1 = strtotime($date_1);
  $ts2 = strtotime($date_2);
  $seconds_diff = ($ts2 - $ts1)/3600/24; // 1 Day = 3600/24
  return $seconds_diff;
}

// หาเวลาที่ต่างกัน
function timeDiff($time_1,$time_2)
{
  return (strtotime($time_2) - strtotime($time_1))/  ( 60 * 60 ); // 1 Hour =  60*60
}

// หาจำนวนวันเวลาที่ต่างกัน
function dateTimeDiff($date_time_1,$date_time_2)
{
  return (strtotime($date_time_2) - strtotime($date_time_1))/  ( 60 * 60 ); // 1 Hour =  60*60
}

// หาวันที่บวกเพิ่ม, หาวันที่ลดลงก็ได้
function dateAdd($date_add, $date)
{
  $start = strtotime($date);
  $end = strtotime($date_add, $start);
  return date('Y-m-d', $end);
}

// วันที่ปัจบัน
function mainDate()
{
  return date('Y-m-d');
}

// เวลาปัจจุบัน
function mainTime()
{
  return date('H:i:s');
}

// วันเวลาปัจจุบัน
function mainDateTime()
{
  return date('Y-m-d H:i:s');
}

// แปลงเป็นวันที่
function toDate($dateTime)
{
  return date("Y-m-d", strtotime($dateTime));
}

// แปลงเป็นเวลา
function toTime($dateTime)
{
  return date("H:i:s", strtotime($dateTime));
}

function month($month,$lang='TH')
{
  if ($lang == 'TH') 
  {
    $array = ["เดือน","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"];
  }
  else
  {
    $array = ["Month","January","February","March","April","May","June","July","August","September","October","November","December"];
  }
  return $array[(int)$month];
}

function day()
{
  $array = [1=>"Sunday",2=>"Monday",3=>"Tuesday",4=>"Wednesday",5=>"Thursday",6=>"Friday",7=>"Saturday"];
  return $array;
}

?>