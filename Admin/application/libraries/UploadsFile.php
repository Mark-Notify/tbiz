<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UploadsFile {

    private $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->model('Main_model');
    }

    public function fileUpload($file,$path,$resize=false)
    {
        if (isset($file['name'])) 
        {
            if (!is_dir($path))
            {
                @mkdir($path, 0777, true);
            }
               
            $file_ext = strrchr($file["name"],".");
            $file_name = date('Y-m-d').'_'.round(microtime(true) * 1000);
            $full_name = $file_name.$file_ext;
            // Upload file
            $file_target = $path . basename($full_name);

            if ( ! @copy($file["tmp_name"], $file_target))
            {
                if ( ! @move_uploaded_file($file["tmp_name"], $file_target))
                {
                    return false;
                }else{
                    if($resize) {
                        $this->fileUploadResize($file,$path,$full_name);
                    }
                    return $full_name;
                }
            }else{
                if($resize) {
                    $this->fileUploadResize($file,$path,$full_name);
                }
                return $full_name;
            }
        }
        else
        {
            return false;
        }
    }

    public function fileUploadTmp($tmp_name,$name,$path)
    {
        if (isset($name)) 
        {
            if (!is_dir($path))
            {
                @mkdir($path, 0777, true);
            }
               
            $file_ext = strrchr($name,".");
            $file_name = date('Y-m-d').'_'.round(microtime(true) * 1000);
            $full_name = $file_name.$file_ext;
            // Upload file
            $file_target = $path . basename($full_name);

            if ( ! @copy($tmp_name, $file_target))
            {
                if ( ! @move_uploaded_file($tmp_name, $file_target))
                {
                    return 0;
                }
                else
                {
                    return $full_name;
                }
            }
            else
            {
                return $full_name;
            }
        }
    }

    public function fileUploadMultiple($file,$path)
    {
        if (is_array($_FILES[$file]['name']) && count($_FILES[$file]['name'])) 
        {
            if (!is_dir($path))
            {
                @mkdir($path, 0777, true);
            }

            $res=0;
            $name = [];
            foreach ($_FILES[$file]['name'] as $key => $value) 
            {
                $file_ext = strrchr($_FILES[$file]["name"][$key],".");
                $file_name = date('Y-m-d').'_'.round(microtime(true) * 1000);
                $full_name = $file_name.$file_ext;
                // Upload file
                $file_target = $path . basename($full_name);
                if (move_uploaded_file($_FILES[$file]["tmp_name"][$key], $file_target)) 
                {
                    $name[] = [
                        'newname' => $full_name,
                        'oldname' => $_FILES[$file]["name"][$key],
                    ];

                    $res++;
                }
            }

            if (count($_FILES[$file]['name']) == $res) 
            {
                return $name;
            }
            else
            {
                return false;
            }
        }
    }

    public function fileUploadResize($file,$url,$full_name)
    {
        $path = $url.'/resize/';
        // pp($file['tmp_name']);
        if (isset($file['name'])) 
        {
            if (!is_dir($path))
            {
                @mkdir($path, 0777, true);
            }
            
            // $file_ext = strrchr($file["name"],".");
            $file_ext = pathinfo($file["name"], PATHINFO_EXTENSION);
            // $file_name = date('Y-m-d').'_'.round(microtime(true) * 1000);
            // $full_name = $file_name.'.'.$file_ext;

            // Upload file
            $new_images = $path . basename($full_name);

            $width=200; //*** Fix Width & Heigh (Autu caculate) ***//
            $size=GetimageSize($file['tmp_name']);
            $height=round($width*$size[1]/$size[0]);
            if ($file_ext == 'png') 
            {
                $images_orig = ImageCreateFromPNG($file['tmp_name']);
            }
            else if($file_ext == 'jpg')
            {
                $images_orig = imagecreatefromjpeg($file['tmp_name']);
            }
            $photoX = ImagesX($images_orig);
            $photoY = ImagesY($images_orig);
            $images_fin = ImageCreateTrueColor($width, $height);
            ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
            ImageJPEG($images_fin,$new_images);
            ImageDestroy($images_orig);
            $res = ImageDestroy($images_fin);

            if($res) {
                return true;
            }else{
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public function fileRemove($file)
    {
        if (is_file($file)) 
        {
            if(@unlink($file))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    
    public function fileUploadBase64($file_base64,$path)
    {
        if (!is_dir($path))
        {
            @mkdir($path, 0777, true);
        }
        define('UPLOAD_DIR', $path);
        $image_parts = explode(";base64,", $file_base64);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $file_name = date('Y-m-d').'_'.round(microtime(true) * 1000).'.'.$image_type;
        $file = UPLOAD_DIR .$file_name;
        $res = file_put_contents($file, $image_base64);
        if($res) {
            return $file_name;
        }else{
            return false;
        }
    }
}
