<?php
class MainConfig
{
    public static function setConfig($pathfile)
    {
        if(file_exists($pathfile))
        {
            foreach (file($pathfile) as $key => $value) 
            {
                if(preg_match('/=/i', $value))
                {
                    putenv(trim($value));
                }
            }
            return true;
        } 
        else 
        {
            return false;
        }
    }
    
}

?>