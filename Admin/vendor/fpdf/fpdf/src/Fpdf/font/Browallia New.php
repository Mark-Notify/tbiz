<?php
$type = 'TrueType';
$name = 'BrowalliaNew';
$desc = array('Ascent'=>840,'Descent'=>-289,'CapHeight'=>467,'Flags'=>32,'FontBBox'=>'[-459 -385 791 864]','ItalicAngle'=>0,'StemV'=>70,'MissingWidth'=>750);
$up = -100;
$ut = 50;
$cw = array(
	chr(0)=>750,chr(1)=>750,chr(2)=>750,chr(3)=>750,chr(4)=>750,chr(5)=>750,chr(6)=>750,chr(7)=>750,chr(8)=>750,chr(9)=>750,chr(10)=>750,chr(11)=>750,chr(12)=>750,chr(13)=>750,chr(14)=>750,chr(15)=>750,chr(16)=>750,chr(17)=>750,chr(18)=>750,chr(19)=>750,chr(20)=>750,chr(21)=>750,
	chr(22)=>750,chr(23)=>750,chr(24)=>750,chr(25)=>750,chr(26)=>750,chr(27)=>750,chr(28)=>750,chr(29)=>750,chr(30)=>750,chr(31)=>750,' '=>222,'!'=>181,'"'=>232,'#'=>363,'$'=>363,'%'=>581,'&'=>436,'\''=>125,'('=>217,')'=>217,'*'=>254,'+'=>381,
	','=>181,'-'=>217,'.'=>181,'/'=>181,'0'=>363,'1'=>363,'2'=>363,'3'=>363,'4'=>363,'5'=>363,'6'=>363,'7'=>363,'8'=>363,'9'=>363,':'=>181,';'=>181,'<'=>381,'='=>381,'>'=>381,'?'=>363,'@'=>663,'A'=>436,
	'B'=>436,'C'=>471,'D'=>471,'E'=>436,'F'=>399,'G'=>508,'H'=>471,'I'=>181,'J'=>326,'K'=>436,'L'=>363,'M'=>544,'N'=>471,'O'=>508,'P'=>436,'Q'=>508,'R'=>471,'S'=>436,'T'=>399,'U'=>471,'V'=>436,'W'=>616,
	'X'=>436,'Y'=>436,'Z'=>399,'['=>181,'\\'=>181,']'=>181,'^'=>306,'_'=>363,'`'=>217,'a'=>363,'b'=>363,'c'=>326,'d'=>363,'e'=>363,'f'=>181,'g'=>363,'h'=>363,'i'=>145,'j'=>145,'k'=>326,'l'=>145,'m'=>544,
	'n'=>363,'o'=>363,'p'=>363,'q'=>363,'r'=>217,'s'=>326,'t'=>181,'u'=>363,'v'=>326,'w'=>471,'x'=>326,'y'=>326,'z'=>326,'{'=>218,'|'=>169,'}'=>218,'~'=>381,chr(127)=>750,chr(128)=>750,chr(129)=>750,chr(130)=>750,chr(131)=>750,
	chr(132)=>750,chr(133)=>750,chr(134)=>750,chr(135)=>750,chr(136)=>750,chr(137)=>750,chr(138)=>750,chr(139)=>750,chr(140)=>750,chr(141)=>750,chr(142)=>750,chr(143)=>750,chr(144)=>750,chr(145)=>750,chr(146)=>750,chr(147)=>750,chr(148)=>750,chr(149)=>750,chr(150)=>750,chr(151)=>750,chr(152)=>750,chr(153)=>750,
	chr(154)=>750,chr(155)=>750,chr(156)=>750,chr(157)=>750,chr(158)=>750,chr(159)=>750,chr(160)=>222,chr(161)=>420,chr(162)=>407,chr(163)=>428,chr(164)=>419,chr(165)=>407,chr(166)=>504,chr(167)=>323,chr(168)=>372,chr(169)=>434,chr(170)=>415,chr(171)=>446,chr(172)=>541,chr(173)=>562,chr(174)=>462,chr(175)=>453,
	chr(176)=>354,chr(177)=>535,chr(178)=>552,chr(179)=>593,chr(180)=>421,chr(181)=>425,chr(182)=>426,chr(183)=>460,chr(184)=>373,chr(185)=>446,chr(186)=>452,chr(187)=>453,chr(188)=>427,chr(189)=>426,chr(190)=>495,chr(191)=>495,chr(192)=>469,chr(193)=>432,chr(194)=>408,chr(195)=>326,chr(196)=>425,chr(197)=>388,
	chr(198)=>472,chr(199)=>365,chr(200)=>418,chr(201)=>485,chr(202)=>397,chr(203)=>458,chr(204)=>501,chr(205)=>395,chr(206)=>406,chr(207)=>391,chr(208)=>325,chr(209)=>0,chr(210)=>334,chr(211)=>349,chr(212)=>0,chr(213)=>0,chr(214)=>0,chr(215)=>0,chr(216)=>0,chr(217)=>0,chr(218)=>0,chr(219)=>750,
	chr(220)=>750,chr(221)=>750,chr(222)=>750,chr(223)=>436,chr(224)=>224,chr(225)=>390,chr(226)=>278,chr(227)=>261,chr(228)=>307,chr(229)=>343,chr(230)=>471,chr(231)=>0,chr(232)=>0,chr(233)=>0,chr(234)=>0,chr(235)=>0,chr(236)=>0,chr(237)=>0,chr(238)=>0,chr(239)=>516,chr(240)=>415,chr(241)=>437,
	chr(242)=>508,chr(243)=>487,chr(244)=>496,chr(245)=>473,chr(246)=>422,chr(247)=>533,chr(248)=>475,chr(249)=>443,chr(250)=>426,chr(251)=>799,chr(252)=>750,chr(253)=>750,chr(254)=>750,chr(255)=>750);
$enc = 'ISO-8859-11';
$diff = '128 /.notdef 130 /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef 142 /.notdef 145 /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef 158 /.notdef /.notdef 161 /kokaithai /khokhaithai /khokhuatthai /khokhwaithai /khokhonthai /khorakhangthai /ngonguthai /chochanthai /chochingthai /chochangthai /sosothai /chochoethai /yoyingthai /dochadathai /topatakthai /thothanthai /thonangmonthothai /thophuthaothai /nonenthai /dodekthai /totaothai /thothungthai /thothahanthai /thothongthai /nonuthai /bobaimaithai /poplathai /phophungthai /fofathai /phophanthai /fofanthai /phosamphaothai /momathai /yoyakthai /roruathai /ruthai /lolingthai /luthai /wowaenthai /sosalathai /sorusithai /sosuathai /hohipthai /lochulathai /oangthai /honokhukthai /paiyannoithai /saraathai /maihanakatthai /saraaathai /saraamthai /saraithai /saraiithai /sarauethai /saraueethai /sarauthai /sarauuthai /phinthuthai /.notdef /.notdef /.notdef /.notdef /bahtthai /saraethai /saraaethai /saraothai /saraaimaimuanthai /saraaimaimalaithai /lakkhangyaothai /maiyamokthai /maitaikhuthai /maiekthai /maithothai /maitrithai /maichattawathai /thanthakhatthai /nikhahitthai /yamakkanthai /fongmanthai /zerothai /onethai /twothai /threethai /fourthai /fivethai /sixthai /seventhai /eightthai /ninethai /angkhankhuthai /khomutthai /.notdef /.notdef /.notdef /.notdef';
$uv = array(0=>array(0,161),161=>array(3585,58),223=>array(3647,29));
$file = 'Browallia New.z';
$originalsize = 52228;
$subsetted = true;
?>
