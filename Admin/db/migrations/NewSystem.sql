-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 19, 2020 at 08:15 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `account`
--

-- --------------------------------------------------------

--
-- Table structure for table `county`
--

CREATE TABLE `county` (
  `county_id` int(11) NOT NULL,
  `county_name` varchar(255) NOT NULL COMMENT 'ชื่อเขต',
  `county_status` int(1) NOT NULL DEFAULT 1 COMMENT '0.ยกเลิก 1.ใช้งาน'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='เขต';

--
-- Dumping data for table `county`
--

INSERT INTO `county` (`county_id`, `county_name`, `county_status`) VALUES
(1, 'คลองสาน', 1),
(2, 'คลองสามวา', 1),
(3, 'คันนายาว', 1),
(4, 'ดอนเมือง', 1),
(5, 'ดินแดง', 1),
(6, 'ดุสิต', 1),
(7, 'บางกอกใหญ่', 1),
(8, 'บางกะปิ', 1),
(9, 'วังทองหลาง', 1),
(10, 'บางคอแหลม', 1),
(11, 'บางซื่อ', 1),
(12, 'บางรัก', 1),
(13, 'ปทุมวัน', 1),
(14, 'ประเวศ', 1),
(15, 'ป้อมปราบศัตรูพ่าย', 1),
(16, 'พญาไท', 1),
(17, 'พระโขนง', 1),
(18, 'มีนบุรี', 1),
(19, 'ราชเทวี', 1),
(20, 'ลาดกระบัง', 1),
(21, 'วัฒนา', 1),
(22, 'สมุทรปราการ', 1),
(23, 'สวนหลวง', 1),
(24, 'สะพานสูง', 1),
(25, 'หนองจอก', 1),
(26, 'ห้วยขวาง', 1);

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `employee_id` int(11) NOT NULL,
  `employee_username` varchar(255) NOT NULL COMMENT 'ชื่อผู้ใช้งาน',
  `employee_password` varchar(255) NOT NULL COMMENT 'รหัสผ่าน',
  `employee_time_attendance` varchar(255) DEFAULT NULL COMMENT 'เวลาเข้างาน ใช้สำหรับระบบลงเวลา',
  `employee_code` varchar(255) DEFAULT NULL COMMENT 'รหัสพนักงาน',
  `employee_date_start` date DEFAULT NULL COMMENT 'วันเริ่มงาน',
  `employee_date_expire` date DEFAULT NULL COMMENT 'วันที่ลาออก (ผู้ใช้งานหมดอายุ)',
  `employee_salary` varchar(255) DEFAULT NULL COMMENT 'ฐานเงินเดือน',
  `employee_references` varchar(255) DEFAULT NULL COMMENT 'บุคคลอ้างอิง',
  `employee_prefix_id` int(11) NOT NULL COMMENT 'ไอดีคำนำหน้าชื่อ',
  `employee_prefix_name` varchar(255) NOT NULL COMMENT 'คำนำหน้าชื่อ',
  `employee_sex_id` int(11) NOT NULL COMMENT 'ไอดีเพศ',
  `employee_sex_name` varchar(255) NOT NULL COMMENT 'เพศ',
  `employee_name` varchar(255) NOT NULL COMMENT 'ชื่อ',
  `employee_lastname` varchar(255) NOT NULL COMMENT 'นามสกุล',
  `employee_nickname` varchar(255) DEFAULT NULL COMMENT 'ชื่อเล่น',
  `employee_age` varchar(255) DEFAULT NULL COMMENT 'อายุ',
  `employee_birthday` date DEFAULT NULL COMMENT 'วันเกิด',
  `employee_religion` varchar(255) DEFAULT NULL COMMENT 'ศาสนา',
  `employee_nationality` varchar(255) DEFAULT NULL COMMENT 'สัญชาติ',
  `employee_card_number` varchar(255) DEFAULT NULL COMMENT 'เลขบัตรประชาชน/หนังสือเดินทาง',
  `employee_card_place` varchar(255) DEFAULT NULL COMMENT 'สถานที่ออกบัตร',
  `employee_card_date` date DEFAULT NULL COMMENT 'วันที่ออกบัตร',
  `employee_card_expire` date DEFAULT NULL COMMENT 'วันที่บัตรหมดอายุ',
  `employee_tel` varchar(255) DEFAULT NULL COMMENT 'โทรศัพท์',
  `employee_mobile` varchar(255) DEFAULT NULL COMMENT 'โทรศัพท์มือถือ',
  `employee_email` varchar(255) DEFAULT NULL COMMENT 'อีเมล',
  `employee_picture` varchar(255) DEFAULT NULL COMMENT 'รูปถ่ายพนักงาน',
  `employee_addr` text DEFAULT NULL COMMENT 'ที่อยู่ปัจจุบัน',
  `employee_addr_in_card` text DEFAULT NULL COMMENT 'ที่อยู่ตามบัตรประจำตัวประชาชน',
  `employee_family_members` int(11) NOT NULL COMMENT 'จำนวนสมาชิกในครอบครัว',
  `employee_note` text DEFAULT NULL COMMENT 'บันทึกอื่นๆ',
  `employee_signature` varchar(255) NOT NULL COMMENT 'รูปลายเซ็น',
  `employee_default_lang` varchar(255) NOT NULL DEFAULT 'thailand' COMMENT 'ภาษาเริ่มต้นในระบบของผู้ใช้งาน',
  `employee_status` int(1) NOT NULL COMMENT 'สถานะผู้ใช้งาน 0=ซ้อน,1=ใช้งาน, 2=ยกเลิก',
  `employee_status_married` int(11) NOT NULL COMMENT 'สถานะสมรส 1=โสด,2=แต่งงาน,3=หย่าร้าง',
  `employee_status_admin` int(1) NOT NULL COMMENT '0=user,1=admin',
  `employee_country_id` int(11) DEFAULT NULL COMMENT 'ประเทศ',
  `employee_social_security_id` int(11) UNSIGNED NOT NULL COMMENT 'สถานะประกันสังคม',
  `employee_timer_id` int(11) UNSIGNED NOT NULL DEFAULT 1 COMMENT 'เงื่อนไขการเข้าระบบ',
  `job_side_id` int(11) NOT NULL COMMENT 'ไอดีฝ่ายงาน',
  `job_side_name` varchar(255) DEFAULT NULL COMMENT 'ฝ่ายงาน',
  `job_section_id` int(11) NOT NULL COMMENT 'ไอดีส่วนงาน',
  `job_section_name` varchar(255) DEFAULT NULL COMMENT 'ส่วนงาน',
  `job_department_id` int(11) NOT NULL COMMENT 'ไอดีแผนกงาน',
  `job_department_name` varchar(255) DEFAULT NULL COMMENT 'แผนกงาน',
  `job_position_id` int(11) NOT NULL COMMENT 'ไอดีตำแน่งงาน',
  `job_position_name` varchar(255) DEFAULT NULL COMMENT 'ตำแน่งงาน',
  `staff_created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'วันที่สร้าง',
  `staff_updated_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'วันที่แก้ไข',
  `staff_created_id` int(11) NOT NULL COMMENT 'ไอดีพนักงานที่สร้าง',
  `staff_created_name` varchar(255) NOT NULL COMMENT 'ชื่อพนักงานที่สร้าง',
  `staff_updated_id` int(11) NOT NULL COMMENT 'ไอดีพนักงานที่แก้ไข',
  `staff_updated_name` varchar(255) NOT NULL COMMENT 'ชื่อพนักงานที่แก้ไข'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`employee_id`, `employee_username`, `employee_password`, `employee_time_attendance`, `employee_code`, `employee_date_start`, `employee_date_expire`, `employee_salary`, `employee_references`, `employee_prefix_id`, `employee_prefix_name`, `employee_sex_id`, `employee_sex_name`, `employee_name`, `employee_lastname`, `employee_nickname`, `employee_age`, `employee_birthday`, `employee_religion`, `employee_nationality`, `employee_card_number`, `employee_card_place`, `employee_card_date`, `employee_card_expire`, `employee_tel`, `employee_mobile`, `employee_email`, `employee_picture`, `employee_addr`, `employee_addr_in_card`, `employee_family_members`, `employee_note`, `employee_signature`, `employee_default_lang`, `employee_status`, `employee_status_married`, `employee_status_admin`, `employee_country_id`, `employee_social_security_id`, `employee_timer_id`, `job_side_id`, `job_side_name`, `job_section_id`, `job_section_name`, `job_department_id`, `job_department_name`, `job_position_id`, `job_position_name`, `staff_created_at`, `staff_updated_at`, `staff_created_id`, `staff_created_name`, `staff_updated_id`, `staff_updated_name`) VALUES
(1, 'Mark', 'e22eab250f35737dddf8d99f8f62aea3', NULL, NULL, NULL, NULL, NULL, NULL, 0, '', 0, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', 'thailand', 0, 0, 1, NULL, 0, 1, 0, NULL, 0, NULL, 0, NULL, 0, NULL, '2020-04-04 02:36:16', '2020-04-04 02:36:16', 1, 'Mark', 1, 'Mark');

-- --------------------------------------------------------

--
-- Table structure for table `employee_code`
--

CREATE TABLE `employee_code` (
  `employee_code_id` int(11) NOT NULL,
  `employee_code_text` varchar(255) NOT NULL COMMENT 'อักษรนำหน้า รหัสผนักงาน',
  `employee_code_status` int(1) NOT NULL COMMENT '0=ไม่ใช้งาน,1=ใช้งาน	'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `employee_country`
--

CREATE TABLE `employee_country` (
  `employee_country_id` int(11) NOT NULL COMMENT 'ไอดีประเทศ',
  `employee_country_th` varchar(255) DEFAULT NULL COMMENT 'ชื่อภาษาไทย',
  `employee_country_en` varchar(255) DEFAULT NULL COMMENT 'ชื่อภาษาอังกฤษ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employee_country`
--

INSERT INTO `employee_country` (`employee_country_id`, `employee_country_th`, `employee_country_en`) VALUES
(1, 'กัมพูชา', 'Cambodia'),
(2, 'กาตาร์', 'Qatar'),
(3, 'เกาหลีใต้', 'South Korea'),
(4, 'เกาหลีเหนือ', 'North Korea'),
(5, 'คาซัคสถาน', 'Kazakhstan'),
(6, 'คีร์กีซสถาน', 'Kyrgyzstan'),
(7, 'คูเวต', 'Kuwait'),
(8, 'จอร์เจีย', 'Georgia'),
(9, 'จอร์แดน', 'Jordan'),
(10, 'จีน', 'China'),
(11, 'ซาอุดีอาระเบีย', 'Saudi Arabia'),
(12, 'ซีเรีย', 'Syria'),
(13, 'ไซปรัส', 'Cyprus'),
(14, 'ญี่ปุ่น', 'Japan'),
(15, 'ติมอร์-เลสเต', 'Timor-Leste'),
(16, 'ตุรกี', 'Turkey'),
(17, 'เติร์กเมนิสถาน', 'Turkmenistan'),
(18, 'ทาจิกิสถาน', 'Tajikistan'),
(19, 'ไทย', 'Thailand'),
(20, 'เนปาล', 'Nepal'),
(21, 'บรูไนดารุสซาลาม', 'Brunei Darussalam'),
(22, 'บังกลาเทศ', 'Bangladesh'),
(23, 'บาห์เรน', 'Bahrain'),
(24, 'ปากีสถาน', 'Pakistan'),
(25, 'ปาเลสไตน์', 'Palestine'),
(26, 'พม่า', 'Myanmar'),
(27, 'ฟิลิปปินส์', 'Philippines'),
(28, 'ภูฏาน', 'Bhutan'),
(29, 'มองโกเลีย', 'Mongolia'),
(30, 'มัลดีฟส์', 'Maldives'),
(31, 'มาเลเซีย', 'Malaysia'),
(32, 'เยเมน', 'Yemen'),
(33, 'ลาว', 'Laos'),
(34, 'เลบานอน', 'Lebanon'),
(35, 'เวียดนาม', 'Vietnam'),
(36, 'ศรีลังกา', 'Sri Lanka'),
(37, 'สหรัฐอาหรับเอมิเรตส์', 'United Arab Emirates'),
(38, 'สิงคโปร์', 'Singapore'),
(39, 'อัฟกานิสถาน', 'Afghanistan'),
(40, 'อาเซอร์ไบจาน', 'Azerbaijan'),
(41, 'อาร์มีเนีย', 'Armenia'),
(42, 'อินเดีย', 'India'),
(43, 'อินโดนีเซีย', 'Indonesia'),
(44, 'อิรัก', 'Iraq'),
(45, 'อิสราเอล', 'Israel'),
(46, 'อิหร่าน', 'Iran'),
(47, 'อุซเบกิสถาน', 'Uzbekistan'),
(48, 'โอมาน', 'Oman'),
(49, 'คิริบาส', 'Kiribati'),
(50, 'ซามัว', 'Samoa'),
(51, 'ตองกา', 'Tonga'),
(52, 'ตูวาลู', 'Tuvalu'),
(53, 'นาอูรู', 'Nauru'),
(54, 'นิวซีแลนด์', 'New Zealand'),
(55, 'ปาปัวนิวกินี', 'Papua New Guinea'),
(56, 'ปาเลา', 'Palau'),
(57, 'ฟิจิ', 'Fiji'),
(58, 'ไมโครนีเซีย', 'Micronesia'),
(59, 'วานูอาตู', 'Vanuatu'),
(60, 'หมู่เกาะโซโลมอน', 'Solomon Islands'),
(61, 'หมู่เกาะมาร์แชลล์', 'Marshall Islands'),
(62, 'ออสเตรเลีย', 'Australia'),
(63, 'กรีซ', 'Greece'),
(64, 'คอซอวอ', 'Kosovo'),
(65, 'โครเอเชีย', 'Croatia'),
(66, 'ซานมารีโน', 'San Marino'),
(67, 'เซอร์เบีย', 'Serbia'),
(68, 'เดนมาร์ก', 'Denmark'),
(69, 'นครรัฐวาติกัน', 'Vatican City State'),
(70, 'นอร์เวย์', 'Norway'),
(71, 'เนเธอร์แลนด์', 'Netherlands'),
(72, 'บอสเนียและเฮอร์เซโกวีนา', 'Bosnia and Herzegovina'),
(73, 'บัลแกเรีย', 'Bulgaria'),
(74, 'เบลเยียม', 'Belgium'),
(75, 'เบลารุส', 'Belarus'),
(76, 'โปรตุเกส', 'Portugal'),
(77, 'โปแลนด์', 'Poland'),
(78, 'ฝรั่งเศส', 'France'),
(79, 'ฟินแลนด์', 'Finland'),
(80, 'มอนเตเนโกร', 'Montenegro'),
(81, 'มอลโดวา', 'Moldova'),
(82, 'มอลตา', 'Malta'),
(83, 'โมนาโก', 'Monaco'),
(84, 'ยูเครน', 'Ukraine'),
(85, 'เยอรมนี', 'Germany'),
(86, 'รัสเซีย', 'Russia'),
(87, 'โรมาเนีย', 'Romania'),
(88, 'ลักเซมเบิร์ก', 'Luxembourg'),
(89, 'ลัตเวีย', 'Latvia'),
(90, 'ลิกเตนสไตน์', 'Liechtenstein'),
(91, 'ลิทัวเนีย', 'Lithuania'),
(92, 'สเปน', 'Spain'),
(93, 'สโลวาเกีย', 'Slovakia'),
(94, 'สโลวีเนีย', 'Slovenia'),
(95, 'สวิตเซอร์แลนด์', 'Switzerland'),
(96, 'สวีเดน', 'Sweden'),
(97, 'สหราชอาณาจักร', 'United Kingdom'),
(98, 'สาธารณรัฐเช็ก', 'Czech Republic'),
(99, 'สาธารณรัฐมาซิโดเนีย', 'Republic of Macedonia'),
(100, 'ออสเตรีย', 'Austria'),
(101, 'อันดอร์รา', 'Andorra'),
(102, 'อิตาลี', 'Italy'),
(103, 'เอสโตเนีย', 'Estonia'),
(104, 'แอลเบเนีย', 'Albania'),
(105, 'ไอซ์แลนด์', 'Iceland'),
(106, 'ไอร์แลนด์', 'Ireland'),
(107, 'ฮังการี', 'Hungary'),
(108, 'กานา', 'Ghana'),
(109, 'กาบอง', 'Gabon'),
(110, 'กาบูเวร์ดี', 'Cabo Verde'),
(111, 'กินี', 'Guinea'),
(112, 'กินี-บิสเซา', 'Guinea-Bissau'),
(113, 'แกมเบีย', 'The Gambia'),
(114, 'โกตดิวัวร์', 'C?te dIvoire'),
(115, 'คอโมโรส', 'Comoros'),
(116, 'เคนยา', 'Kenya'),
(117, 'แคเมอรูน', 'Cameroon'),
(118, 'จิบูตี', 'Djibouti'),
(119, 'ชาด', 'Chad'),
(120, 'ซิมบับเว', 'Zimbabwe'),
(121, 'ซูดาน', 'Sudan'),
(122, 'เซเชลส์', 'Seychelles'),
(123, 'เซเนกัล', 'Senegal'),
(124, 'เซาตูเมและปรินซิปี', 'S?o Tom? and Pr?ncipe'),
(125, 'เซาท์ซูดาน', 'South Sudan'),
(126, 'เซียร์ราลีโอน', 'Sierra Leone'),
(127, 'แซมเบีย', 'Zambia'),
(128, 'โซมาเลีย', 'Somalia'),
(129, 'ตูนิเซีย', 'Tunisia'),
(130, 'โตโก', 'Togo'),
(131, 'แทนซาเนีย', 'Tanzania'),
(132, 'นามิเบีย', 'Namibia'),
(133, 'ไนจีเรีย', 'Nigeria'),
(134, 'ไนเจอร์', 'Niger'),
(135, 'บอตสวานา', 'Botswana'),
(136, 'บุรุนดี', 'Burundi'),
(137, 'บูร์กินาฟาโซ', 'Burkina Faso'),
(138, 'เบนิน', 'Benin'),
(139, 'มอริเชียส', 'Mauritius'),
(140, 'มอริเตเนีย', 'Mauritania'),
(141, 'มาดากัสการ์', 'Madagascar'),
(142, 'มาลาวี', 'Malawi'),
(143, 'มาลี', 'Mali'),
(144, 'โมซัมบิก', 'Mozambique'),
(145, 'โมร็อกโก', 'Morocco'),
(146, 'ยูกันดา', 'Uganda'),
(147, 'รวันดา', 'Rwanda'),
(148, 'ลิเบีย', 'Libya'),
(149, 'เลโซโท', 'Lesotho'),
(150, 'ไลบีเรีย', 'Liberia'),
(151, 'สวาซิแลนด์', 'Swaziland'),
(152, 'สาธารณรัฐคองโก', 'Republic of the Congo'),
(153, 'สาธารณรัฐประชาธิปไตยคองโก', 'Democratic Republic of the Congo'),
(154, 'สาธารณรัฐแอฟริกากลาง', 'Central African Republic'),
(155, 'อิเควทอเรียลกินี', 'Equatorial Guinea'),
(156, 'อียิปต์', 'Egypt'),
(157, 'เอธิโอเปีย', 'Ethiopia'),
(158, 'เอริเทรีย', 'Eritrea'),
(159, 'แองโกลา', 'Angola'),
(160, 'แอฟริกาใต้', 'South Africa'),
(161, 'แอลจีเรีย', 'Algeria'),
(162, 'กัวเตมาลา', 'Guatemala'),
(163, 'เกรเนดา', 'Grenada'),
(164, 'คอสตาริกา', 'Costa Rica'),
(165, 'คิวบา', 'Cuba'),
(166, 'แคนาดา', 'Canada'),
(167, 'จาเมกา', 'Jamaica'),
(168, 'เซนต์คิตส์และเนวิส', 'Saint Kitts and Nevis'),
(169, 'เซนต์ลูเซีย', 'Saint Lucia'),
(170, 'เซนต์วินเซนต์และเกรนาดีนส์', 'Saint Vincent and the Grenadines'),
(171, 'ดอมินีกา', 'Dominica'),
(172, 'นิการากัว', 'Nicaragua'),
(173, 'บาร์เบโดส', 'Barbados'),
(174, 'บาฮามาส', 'Bahamas'),
(175, 'เบลีซ', 'Belize'),
(176, 'ปานามา', 'Panama'),
(177, 'เม็กซิโก', 'Mexico'),
(178, 'สหรัฐอเมริกา', 'United States of America'),
(179, 'สาธารณรัฐโดมินิกัน', 'Dominican Republic'),
(180, 'เอลซัลวาดอร์', 'El Salvador'),
(181, 'แอนติกาและบาร์บูดา', 'Antigua and Barbuda'),
(182, 'ฮอนดูรัส', 'Honduras'),
(183, 'เฮติ', 'Haiti'),
(184, 'กายอานา', 'Guyana'),
(185, 'โคลอมเบีย', 'Colombia'),
(186, 'ชิลี', 'Chile'),
(187, 'ซูรินาม', 'Suriname'),
(188, 'ตรินิแดดและโตเบโก', 'Trinidad and Tobago'),
(189, 'บราซิล', 'Brazil'),
(190, 'โบลิเวีย', 'Bolivia'),
(191, 'ปารากวัย', 'Paraguay'),
(192, 'เปรู', 'Peru'),
(193, 'เวเนซุเอลา', 'Venezuela'),
(194, 'อาร์เจนตินา', 'Argentina'),
(195, 'อุรุกวัย', 'Uruguay'),
(196, 'เอกวาดอร์', 'Ecuador'),
(197, 'เซาท์ออสซีเชีย', 'South Ossetia'),
(198, 'โซมาลีแลนด์', 'Somaliland'),
(199, 'ไซปรัสเหนือ', 'Northern Cyprus'),
(200, 'ไต้หวัน', 'Taiwan'),
(201, 'ทรานส์นีสเตรีย', 'Transnistria'),
(202, 'นากอร์โน-คาราบัค', 'Nagorno-Karabakh'),
(203, 'สาธารณรัฐประชาธิปไตย อาหรับซาห์ราวี', 'Sahrawi Arab Democratic Republic'),
(204, 'อับฮาเซีย', 'Abkhazia'),
(205, 'กรีนแลนด์', 'Greenland'),
(206, 'กวม', 'Guam'),
(207, 'กัวเดอลุป', 'Guadeloupe'),
(208, 'กือราเซา', 'Cura?ao'),
(209, 'เกาะกลีแปร์ตอน', 'Clipperton Island'),
(210, 'เกาะคริสต์มาส', 'Christmas Island'),
(211, 'เกาะจาร์วิส', 'Jarvis Island'),
(212, 'เกาะเซาท์จอร์เจียและหมู่เกาะเซาท์แซนด์วิช', 'South Georgia and the South Sandwich Islands'),
(213, 'เกาะนอร์ฟอล์ก', 'Norfolk Island'),
(214, 'เกาะนาแวสซา', 'Navassa Island'),
(215, 'เกาะบูแว', 'Bouvet Island'),
(216, 'เกาะเบเกอร์', 'Baker Island'),
(217, 'เกาะปีเตอร์ที่ 1', 'Peter I Island'),
(218, 'เกาะแมน', 'Isle of Man'),
(219, 'เกาะเวก', 'Wake Island'),
(220, 'เกาะฮาวแลนด์', 'Howland Island'),
(221, 'เกาะเฮิร์ดและหมู่เกาะแมกดอนัลด์', 'Heard and McDonald Islands'),
(222, 'เกิร์นซีย์', 'Guernsey'),
(223, 'ควีนมอดแลนด์', 'Queen Maud Land'),
(224, 'คอซอวอ', 'Kosovo'),
(225, 'คิงแมนรีฟ', 'Kingman Reef'),
(226, 'จอห์นสตันอะทอลล์', 'Johnston Atoll'),
(227, 'เจอร์ซีย์', 'Jersey'),
(228, 'ชิเลียนแอนตาร์กติกเทร์ริทอรี', 'Chilean Antarctic Territory'),
(229, 'ซินต์มาร์เติน', 'Sint Maarten'),
(230, 'เซนต์เฮเลนา อัสเซนชัน และตริสตันดากูนยา', 'Saint Helena Ascension and Tristan da Cunha'),
(231, 'เซร์รานียาแบงก์', 'Serranilla Bank'),
(232, 'เซวตา', 'Ceuta'),
(233, 'แซงปีแยร์และมีเกอลง', 'Saint-Pierre and Miquelon'),
(234, 'แซ็ง-บาร์เตเลมี', 'Saint-Barth?lemy'),
(235, 'แซ็ง-มาร์แต็ง', 'Saint-Martin'),
(236, 'โตเกเลา', 'Tokelau'),
(237, 'ไต้หวัน', 'Taiwan'),
(238, 'นากอร์โน-คาราบัค', 'Nagorno-Karabakh'),
(239, 'นิวแคลิโดเนีย', 'New Caledonia'),
(240, 'นีอูเอ', 'Niue'),
(241, 'บริติชอินเดียนโอเชียนเทร์ริทอรี', 'British Indian Ocean Territory'),
(242, 'บริติชแอนตาร์กติกเทร์ริทอรี', 'British Antarctic Territory'),
(243, 'เบอร์มิวดา', 'Bermuda'),
(244, 'บาโฮนวยโวแบงก์', 'Bajo Nuevo Bank'),
(245, 'เปอร์โตริโก', 'Puerto Rico'),
(246, 'แพลไมราอะทอลล์', 'Palmyra Atoll'),
(247, 'เฟรนช์เกียนา', 'French Guiana'),
(248, 'เฟรนช์เซาเทิร์นและแอนตาร์กติกแลนส์', 'French Southern and Antarctic Lands'),
(249, 'เฟรนช์โปลินีเซีย', 'French Polynesia'),
(250, 'มอนต์เซอร์รัต', 'Montserrat'),
(251, 'มาเดรา', 'Madeira'),
(252, 'มายอต', 'Mayotte'),
(253, 'มาร์ตีนิก', 'Martinique'),
(254, 'มิดเวย์อะทอลล์', 'Midway Atoll'),
(255, 'เมลียา', 'Melilla'),
(256, 'ยานไมเอน', 'Jan Mayen'),
(257, 'ยิบรอลตาร์', 'Gibraltar'),
(258, 'รอสส์ดีเพนเดนซี', 'Ross Dependency'),
(259, 'เรอูว์นียง', 'R?union'),
(260, 'วาลลิสและฟุตูนา', 'Wallis and Futuna'),
(261, 'เวสเทิร์นสะฮารา', 'Western Sahara'),
(262, 'สฟาลบาร์', 'Svalbard'),
(263, 'หมู่เกาะคอรัลซี', 'Coral Sea Islands'),
(264, 'หมู่เกาะคะแนรี', 'Canary Islands'),
(265, 'หมู่เกาะคุก', 'Cook Islands'),
(266, 'หมู่เกาะเคย์แมน', 'Cayman Islands'),
(267, 'หมู่เกาะโคโคส (คีลิง)', 'Cocos (Keeling) Islands'),
(268, 'หมู่เกาะเติกส์และหมู่เกาะเคคอส', 'Turks and Caicos Islands'),
(269, 'หมู่เกาะนอร์เทิร์นมาเรียนา', 'Northern Mariana Islands'),
(270, 'หมู่เกาะบริติชเวอร์จิน', 'British Virgin Islands'),
(271, 'หมู่เกาะพิตแคร์น', 'Pitcairn Islands'),
(272, 'หมู่เกาะฟอล์กแลนด์', 'Falkland Islands'),
(273, 'หมู่เกาะแฟโร', 'Faroe Islands'),
(274, 'หมู่เกาะเวอร์จินของสหรัฐอเมริกา', 'United States Virgin Islands'),
(275, 'หมู่เกาะแอชมอร์และเกาะคาร์เทียร์', 'Ashmore and Cartier Islands'),
(276, 'อเมริกันซามัว', 'American Samoa'),
(277, 'ออสเตรเลียนแอนตาร์กติก เทร์ริทอรี', 'Australian Antarctic Territory'),
(278, 'อะโซร์ส', 'Azores'),
(279, 'อาร์เจนไทน์แอนตาร์กติกา', 'Argentine Antarctica'),
(280, 'อารูบา', 'Aruba'),
(281, 'แอโครเทียรีและดิเคเลีย', 'Akrotiri and Dhekelia'),
(282, 'แองกวิลลา', 'Anguilla');

-- --------------------------------------------------------

--
-- Table structure for table `employee_prefix`
--

CREATE TABLE `employee_prefix` (
  `employee_prefix_id` int(11) NOT NULL COMMENT 'ไอดี',
  `employee_prefix_th` varchar(255) NOT NULL COMMENT 'คำนำหน้าภาษาไทย',
  `employee_prefix_en` varchar(255) NOT NULL COMMENT 'คำนำหน้าภาษาอังกฤษ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employee_prefix`
--

INSERT INTO `employee_prefix` (`employee_prefix_id`, `employee_prefix_th`, `employee_prefix_en`) VALUES
(1, 'นาย', 'Mr.'),
(2, 'นาง', 'Mrs.'),
(3, 'นางสาว', 'Miss.');

-- --------------------------------------------------------

--
-- Table structure for table `employee_sex`
--

CREATE TABLE `employee_sex` (
  `employee_sex_id` int(11) NOT NULL COMMENT 'ไอดี',
  `employee_sex_th` varchar(255) NOT NULL COMMENT 'เพศภาษาไทย',
  `employee_sex_en` varchar(255) NOT NULL COMMENT 'เพศภาษาอังกฤษ',
  `employee_sex_image` varchar(255) DEFAULT NULL COMMENT 'รูปโปรไฟล์'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employee_sex`
--

INSERT INTO `employee_sex` (`employee_sex_id`, `employee_sex_th`, `employee_sex_en`, `employee_sex_image`) VALUES
(1, 'ชาย', 'Male', 'man.jpg'),
(2, 'หญิง', 'Female', 'female.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `employee_social_security`
--

CREATE TABLE `employee_social_security` (
  `employee_social_security_id` int(11) NOT NULL COMMENT 'ไอดี',
  `employee_social_security_name` varchar(255) NOT NULL COMMENT 'ประเภทประกันสังคม'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='cesstant_2020 (สถานะประกันสังคม)';

--
-- Dumping data for table `employee_social_security`
--

INSERT INTO `employee_social_security` (`employee_social_security_id`, `employee_social_security_name`) VALUES
(1, 'พนง.ใหม่ที่ไม่ได้ขึ้นทะเบียนประกันสังคม'),
(2, 'พนง.ใหม่ขึ้นทะเบียนประกันสังคมแล้ว'),
(3, 'พนง.บริษัทปกติ'),
(4, 'พนง.ไม่ขึ้นประกันสังคม');

-- --------------------------------------------------------

--
-- Table structure for table `income`
--

CREATE TABLE `income` (
  `income_id` int(11) NOT NULL COMMENT 'ไอดี',
  `income_date_in` date NOT NULL DEFAULT current_timestamp() COMMENT 'วันที่เข้าสอน',
  `income_date_out` date NOT NULL DEFAULT current_timestamp() COMMENT 'วันที่สิ้นสุดการสอน',
  `income_billing_year` int(11) NOT NULL COMMENT 'ปีที่วางบิล',
  `income_billing_month` int(11) NOT NULL COMMENT 'เดือนที่ต้องรับเงิน',
  `income_county_id` int(11) NOT NULL COMMENT 'เขต',
  `income_county_name` varchar(255) NOT NULL COMMENT 'เขต',
  `income_school_id` int(11) NOT NULL COMMENT 'โรงเรียน',
  `income_school_name` varchar(255) NOT NULL COMMENT 'ชื่อโรงเรียน',
  `income_teacher_code` varchar(255) NOT NULL COMMENT 'รหัสครู',
  `income_teacher_id` int(11) NOT NULL COMMENT 'ไอดีครู',
  `income_teacher_name` varchar(255) NOT NULL COMMENT 'ชื่อครู',
  `income_billing_hours` varchar(255) NOT NULL COMMENT 'ชม.วางบิล',
  `income_billing_money` varchar(255) NOT NULL COMMENT 'เงินวางบิล',
  `income_working_hours` varchar(255) NOT NULL COMMENT 'ชม.ทำงาน',
  `income_hours_leave` varchar(255) NOT NULL COMMENT 'ชม.ลา',
  `income_hours_total` varchar(255) NOT NULL COMMENT 'ชม.ทั้งหมด',
  `income_real_hours` varchar(255) NOT NULL COMMENT 'ชม.จริง (รับมาจากโรงเรียน)',
  `income_real_money` varchar(255) NOT NULL COMMENT 'จำนวนเงินจริง (รับมาจากโรงเรียน)',
  `income_accrual_hours` varchar(255) NOT NULL COMMENT 'ชม.คงค้าง (เงินคงค้าง)',
  `income_accrual_money` varchar(255) NOT NULL COMMENT 'จำนวนเงินคงค้าง (เงินคงค้าง)',
  `income_receive_name` varchar(255) NOT NULL COMMENT 'ชื่อผู้รับเงิน',
  `income_bank_id` int(11) NOT NULL COMMENT 'ไอดีธนาคาร',
  `income_bank_name` varchar(255) NOT NULL COMMENT 'ชื่อธนาคาร',
  `income_bank_number` varchar(255) NOT NULL COMMENT 'เลขที่บัญชี',
  `income_petition` varchar(255) NOT NULL COMMENT 'ฎีกา (เอกสารประกอบ)',
  `income_deposit_slip` varchar(255) NOT NULL COMMENT 'ใบฝาก (เอกสารประกอบ)',
  `income_deposit_date` date NOT NULL COMMENT 'วันที่นำฝาก',
  `income_remark` text NOT NULL COMMENT 'หมายเหตุ',
  `income_status` int(1) NOT NULL DEFAULT 1 COMMENT '0.ไม่ใช้งาน 1.ใช้งาน',
  `staff_created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'วันที่สร้าง',
  `staff_updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'วันที่แก้ไข',
  `staff_created_id` int(11) NOT NULL COMMENT 'ไอดีพนักงานที่สร้าง',
  `staff_created_name` varchar(255) NOT NULL COMMENT 'ชื่อพนักงานที่สร้าง',
  `staff_updated_id` int(11) NOT NULL COMMENT 'ไอดีพนักงานที่แก้ไข',
  `staff_updated_name` varchar(255) NOT NULL COMMENT 'ชื่อพนักงานที่แก้ไข'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ตารางบัญชีรายรับ';

-- --------------------------------------------------------

--
-- Table structure for table `money_transaction`
--

CREATE TABLE `money_transaction` (
  `mt_id` int(11) NOT NULL COMMENT 'ไอดี',
  `mt_teacher_code` varchar(255) NOT NULL COMMENT 'รหัสครู',
  `mt_teacher_name` varchar(255) NOT NULL COMMENT 'ชื่อครู',
  `mt_course` varchar(255) NOT NULL COMMENT 'หลักสูตร',
  `mt_billing_time` varchar(255) NOT NULL COMMENT 'ชม.วางบิลลูกค้า',
  `mt_billed_amount` decimal(13,2) NOT NULL COMMENT 'จำนวนเงินที่วางบิลลูกค้า',
  `mt_amount_received_from_school` decimal(13,2) NOT NULL COMMENT 'จำนวนเงินที่รับมาจากโรงเรียน',
  `mt_date_receiving` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'วันที่รับเงิน ',
  `mt_due_days` int(11) NOT NULL COMMENT 'จำนวนวัน DUE',
  `mt_payee` varchar(255) NOT NULL COMMENT 'ผู้รับเงิน',
  `mt_date_deposit` varchar(255) NOT NULL COMMENT 'วันที่นำฝาก',
  `mt_deposited_bank` varchar(255) NOT NULL COMMENT 'ธนาคารที่ฝาก ',
  `mt_notation` text NOT NULL COMMENT 'หมายเหตุ',
  `mt_status` int(11) NOT NULL COMMENT 'สถานะ 0.ไม่ใช้งาน 1.ใช้งาน',
  `staff_created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'วันที่สร้าง',
  `staff_updated_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'วันที่แก้ไข',
  `staff_created_id` int(11) NOT NULL COMMENT 'ไอดีพนักงานที่สร้าง',
  `staff_created_name` varchar(255) NOT NULL COMMENT 'ชื่อพนักงานที่สร้าง',
  `staff_updated_id` int(11) NOT NULL COMMENT 'ไอดีพนักงานที่แก้ไข',
  `staff_updated_name` varchar(255) NOT NULL COMMENT 'ชื่อพนักงานที่แก้ไข'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `online`
--

CREATE TABLE `online` (
  `session_id` varchar(255) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `online_last_time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `online`
--

INSERT INTO `online` (`session_id`, `employee_id`, `ip_address`, `online_last_time`) VALUES
('8h4r6oe5r4gbl9mcfr8no0cpig112n9b', 1, '127.0.0.1', '2020-04-04 04:15:14'),
('c82rm83l12l0o52jg021kmu1k35asims', 1, '127.0.0.1', '2020-04-04 15:36:59'),
('k0cv4e0r743c36upirsmhgbjj1n18jqu', 1, '127.0.0.1', '2020-04-22 22:28:39');

-- --------------------------------------------------------

--
-- Table structure for table `phinxlog`
--

CREATE TABLE `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `school`
--

CREATE TABLE `school` (
  `school_id` int(11) NOT NULL,
  `school_name` varchar(255) NOT NULL COMMENT 'ชื่อโรงเรียน',
  `school_tuition_fee` varchar(255) NOT NULL COMMENT 'ราคาค่าสอน',
  `school_county_id` int(11) NOT NULL COMMENT 'ไอดีเขต',
  `school_status` int(1) NOT NULL DEFAULT 1 COMMENT '0.ไม่ใช้งาน 1.ใช้งาน'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `school`
--

INSERT INTO `school` (`school_id`, `school_name`, `school_tuition_fee`, `school_county_id`, `school_status`) VALUES
(1, 'ร.ร. มัธยมวัดสุทธาราม', '100', 1, 1),
(2, 'ร.ร. วัดทองธรรมชาติ', '200', 1, 1),
(3, 'ร.ร. วัดทองเพลง', '300', 1, 1),
(4, 'ร.ร. วัดสุทธาราม', '400', 1, 1),
(5, 'ร.ร. กลางคลองสอง', '500', 2, 1),
(6, 'ร.ร. วัดบัวแก้ว', '', 2, 1),
(7, 'ร.ร. วัดพระยาสุเรนทร์', '', 2, 1),
(8, 'ร.ร. วัดศรีสุก แย้มเยื้อนอุปถัมภ์', '', 2, 1),
(9, 'ร.ร. สุเหร่าคลองสี่', '', 2, 1),
(10, 'ร.ร. จินดาบำรุง', '', 3, 1),
(11, 'ร.ร. บำรุงรวิวรรณ', '', 4, 1),
(12, 'ร.ร. เปรมประชา', '', 4, 1),
(13, 'ร.ร. วิชากร', '', 5, 1),
(14, 'ร.ร. วัดสวัสดิ์วารีสีมาราม', '', 6, 1),
(15, 'ร.ร. วัดดีดวด', '', 7, 1),
(16, 'ร.ร. วัดราชสิทธาราม', '', 7, 1),
(17, 'ร.ร. วัดท่าพระ', '', 7, 1),
(18, 'ร.ร. วัดนาคกลาง', '', 7, 1),
(19, 'ร.ร. วัดใหม่พิเรนทร์', '', 7, 1),
(20, 'ร.ร. ไขศรีปราโมชอนุสรณ์', '', 8, 1),
(21, 'ร.ร. วัดเทพลีลา', '', 8, 1),
(22, 'ร.ร. วัดบึงทองหลาง', '', 8, 1),
(23, 'ร.ร. วัดศรีบุญเรือง', '', 8, 1),
(24, 'ร.ร. สุเหร่าคลองจั่น', '', 8, 1),
(25, 'ร.ร. สุเหร่าบึงหนองบอน', '', 9, 1),
(26, 'ร.ร. วัดสามัคคีธรรม', '', 9, 1),
(27, 'ร.ร. สุเหร่าดอนสะแก', '', 9, 1),
(28, 'ร.ร. วัดไผ่เงินโชตนาราม', '', 10, 1),
(29, 'ร.ร. วัดทองสุทธาราม', '', 11, 1),
(30, 'ร.ร. วัดบางโพโอมาวาส', '', 11, 1),
(31, 'ร.ร. วัดประชาศรัทธาธรรม', '', 11, 1),
(32, 'ร.ร. วัดประดู่ธรรมาธิปัตย์', '', 11, 1),
(33, 'ร.ร. วัดมัณฌันติการาม', '', 11, 1),
(34, 'ร.ร. วัดเลียบ', '', 11, 1),
(35, 'ร.ร. วัดสร้อยทอง', '', 11, 1),
(36, 'ร.ร. วัดม่วงแค', '', 12, 1),
(37, 'ร.ร. วัดสวนพลู', '', 12, 1),
(38, 'ร.ร. วัดบรมนิวาส', '', 13, 1),
(39, 'ร.ร. วัดสระบัว', '', 13, 1),
(40, 'ร.ร. วัดกระทุ่มเสือปลา', '', 14, 1),
(41, 'ร.ร. แก่นทองอุปถัมภ์', '', 14, 1),
(42, 'ร.ร. คลองปักหลัก', '', 14, 1),
(43, 'ร.ร. คลองมะขามเทศ', '', 14, 1),
(44, 'ร.ร. งามมานะ', '', 14, 1),
(45, 'ร.ร. มัธยมสุวิทย์', '', 14, 1),
(46, 'ร.ร. วัดตะกล่ำ', '', 14, 1),
(47, 'ร.ร. สุเหร่าจรเข้ขบ', '', 14, 1),
(48, 'ร.ร. สุเหร่าทับช้าง', '', 14, 1),
(49, 'ร.ร. สุเหร่าทางควาย', '', 14, 1),
(50, 'ร.ร. สุเหร่าบ้านม้า', '', 14, 1),
(51, 'ร.ร. สุเหร่าบึงหนองบอน', '', 14, 1),
(52, 'ร.ร. สุเหร่าศาลาลอย', '', 14, 1),
(53, 'ร.ร. อยู่เป็นสุขอนุสรณ์', '', 14, 1),
(54, 'ร.ร. วัดคณิตกาผล', '', 15, 1),
(55, 'ร.ร. วัดดิสานุการาม', '', 15, 1),
(56, 'ร.ร. วัดสิตาราม', '', 15, 1),
(57, 'ร.ร. วัดไผ่ตัน', '', 16, 1),
(58, 'ร.ร. พูนสิน', '', 17, 1),
(59, 'ร.ร. วัดธรรมมงคล', '', 17, 1),
(60, 'ร.ร. คลองสองต้นนุ่น', '', 18, 1),
(61, 'ร.ร. คลองสาม', '', 18, 1),
(62, 'ร.ร. บ้านเกาะ', '', 18, 1),
(63, 'ร.ร. วัดบำเพ็ญเหนือ', '', 18, 1),
(64, 'ร.ร. บึงขวาง', '', 18, 1),
(65, 'ร.ร. มีนบุรี', '', 18, 1),
(66, 'ร.ร. วังเล็ก', '', 18, 1),
(67, 'ร.ร. วัดทองสัมฤทธิ์', '', 18, 1),
(68, 'ร.ร. วัดแสนสุข', '', 18, 1),
(69, 'ร.ร. วัดใหม่ลำนกแขวก', '', 18, 1),
(70, 'ร.ร. ศาลาคู้', '', 18, 1),
(71, 'ร.ร. สุเหร่าทรายกองดิน', '', 18, 1),
(72, 'ร.ร. ขุมทองเพชรทองคำ', '', 20, 1),
(73, 'ร.ร. เคหะชุมชนลาดกระบัง', '', 20, 1),
(74, 'ร.ร. แดงเป้า', '', 20, 1),
(75, 'ร.ร. ตำบลขุมทอง', '', 20, 1),
(76, 'ร.ร. วัดบำรุงรื่น', '', 20, 1),
(77, 'ร.ร. ประสานสามัคคี', '', 20, 1),
(78, 'ร.ร. วัดปากบึง', '', 20, 1),
(79, 'ร.ร. ลำพะอง', '', 20, 1),
(80, 'ร.ร. วัดขุมทอง', '', 20, 1),
(81, 'ร.ร. วัดทิพพาวาส', '', 20, 1),
(82, 'ร.ร. วัดปลูกศรัทธา', '', 20, 1),
(83, 'ร.ร. วัดพลมานีย์', '', 20, 1),
(84, 'ร.ร. วัดราชโกษา', '', 20, 1),
(85, 'ร.ร. วัดลานบุญ', '', 20, 1),
(86, 'ร.ร. วัดสุทธาโภชน์', '', 20, 1),
(87, 'ร.ร. สุเหร่าลำนายโส', '', 20, 1),
(88, 'ร.ร. แสงหิรัญวิทยา', '', 20, 1),
(89, 'ร.ร. วิจิตรวิทยา', '', 21, 1),
(90, 'ร.ร. ป้อมนาคราชสวาทยานนท์', '', 22, 1),
(91, 'ร.ร. คลองกลันตัน', '', 23, 1),
(92, 'ร.ร. นาคนาวาอุปถัมภ์', '', 23, 1),
(93, 'ร.ร. มัธยมนาคนาวา', '', 23, 1),
(94, 'ร.ร. วัดใต้', '', 23, 1),
(95, 'ร.ร. วัดปากบ่อ', '', 23, 1),
(96, 'ร.ร. สุเหร่าใหม่', '', 23, 1),
(97, 'ร.ร. หัวหมาก', '', 23, 1),
(98, 'ร.ร. วัดลาดบัวขาว', '', 24, 1),
(99, 'ร.ร. สามแยกคลองหลอแหล', '', 24, 1),
(100, 'ร.ร. สุเหร่าซีรอ', '', 24, 1),
(101, 'ร.ร. บ้านเจียรดับ', '', 25, 1),
(102, 'ร.ร. บ้านลำต้นกล้วย', '', 25, 1),
(103, 'ร.ร. ลำเจดีย์', '', 25, 1),
(104, 'ร.ร. ลำบุหรี่พวง', '', 25, 1),
(105, 'ร.ร. วัดสีชมพู', '', 25, 1),
(106, 'ร.ร. วัดหนองจอก (ภักดีนรเศรษฐ)', '', 25, 1),
(107, 'ร.ร. สุเหร่าสนามกลางลำ', '', 25, 1),
(108, 'ร.ร. สังฆประชานุสสรณ์', '', 25, 1),
(109, 'ร.ร. วัดสามง่าม', '', 25, 1),
(110, 'ร.ร. สามแยกท่าไข่', '', 25, 1),
(111, 'ร.ร. วัดแสนเกษม', '', 25, 1),
(112, 'ร.ร. พระรามเก้ากาญจนาภิเษก', '', 26, 1);

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE `teacher` (
  `teacher_id` int(11) NOT NULL COMMENT 'ไอดี',
  `teacher_text_code` varchar(255) NOT NULL COMMENT 'คำนำหน้ารหัส',
  `teacher_code` varchar(255) NOT NULL COMMENT 'รหัสครู',
  `teacher_prefix` int(11) NOT NULL COMMENT 'คำนำหน้านาม',
  `teacher_name` varchar(255) NOT NULL COMMENT 'ชื่อครู',
  `teacher_nationality` varchar(255) NOT NULL COMMENT 'สัญชาติ',
  `teacher_sex` int(11) NOT NULL COMMENT 'เพศ',
  `teacher_age` varchar(3) NOT NULL COMMENT 'อายุ',
  `teacher_birthday` date NOT NULL COMMENT 'วันเกิด',
  `teacher_mobile` varchar(255) NOT NULL COMMENT 'เบอร์โทร',
  `teacher_address` varchar(255) NOT NULL COMMENT 'ที่อยู่',
  `teacher_email` varchar(255) NOT NULL COMMENT 'อีเมล',
  `teacher_line` varchar(255) NOT NULL COMMENT 'ไอดีไลน์',
  `teacher_text_iden` varchar(255) NOT NULL COMMENT 'บัตรประชาชน',
  `teacher_educational` varchar(255) NOT NULL COMMENT 'วุฒิการศึกษา',
  `teacher_faculty` varchar(255) NOT NULL COMMENT 'สาขา',
  `teacher_visa_type` varchar(255) NOT NULL COMMENT 'ประเภทวีซ่า',
  `teacher_visa_expire` date NOT NULL COMMENT 'วันหมดอายุวีซ่า',
  `teacher_profession_expire` date NOT NULL COMMENT 'วันหมดอายุใบประกอบวิชาชีพครู',
  `teacher_work_expire` date NOT NULL COMMENT 'วันหมดอายุใบอนุญาติทำงาน',
  `teacher_hours_per_mount` int(11) NOT NULL COMMENT 'ชม. สอนต่อเดือน',
  `teaacher_type_salary` int(11) NOT NULL COMMENT 'รูปแบบเงินเดือน 1.รายเดือน 2.รายชม.',
  `teacher_salary` double(13,2) NOT NULL COMMENT ' เงินเดือน (รายเดือน/รายชม.)',
  `techer_subject` varchar(255) NOT NULL COMMENT 'วิชาที่สอน',
  `teacher_image` varchar(255) NOT NULL COMMENT 'รูปถ่าย',
  `teacher_in_country` varchar(255) NOT NULL COMMENT 'ประสบการณ์สอนในประเทศ',
  `teacher_special_ability` varchar(255) NOT NULL COMMENT 'ความสามารถพิเศษ',
  `teacher_aptitude` varchar(255) NOT NULL COMMENT 'ความถนัดในการสอน (อนุบาล/ประถม/มัธยม)',
  `teacher_school_id` text NOT NULL COMMENT 'ชื่อโรงเรียนที่ไปสอน',
  `teacher_district_id` text NOT NULL COMMENT 'ชื่อเขตที่ไปสอน',
  `teacher_bank_name` varchar(255) NOT NULL COMMENT 'ชื่อธนาคาร',
  `teacher_bank_number` varchar(255) NOT NULL COMMENT 'เลขที่บัญชี',
  `teacher_status` int(11) NOT NULL DEFAULT 1 COMMENT '0.ยกเลิก 1.ใช้งาน',
  `staff_created_at` date NOT NULL COMMENT 'วันที่สร้าง',
  `staff_updated_at` date NOT NULL COMMENT 'วันที่แก้ไข',
  `staff_created_id` int(11) NOT NULL COMMENT 'ไอดีพนักงานที่สร้าง',
  `staff_created_name` varchar(255) NOT NULL COMMENT 'ชื่อพนักงานที่สร้าง',
  `staff_updated_id` int(11) NOT NULL COMMENT 'ไอดีพนักงานที่แก้ไข',
  `staff_updated_name` varchar(255) NOT NULL COMMENT 'ชื่อพนักงานที่แก้ไข'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `county`
--
ALTER TABLE `county`
  ADD PRIMARY KEY (`county_id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`employee_id`);

--
-- Indexes for table `income`
--
ALTER TABLE `income`
  ADD PRIMARY KEY (`income_id`);

--
-- Indexes for table `money_transaction`
--
ALTER TABLE `money_transaction`
  ADD PRIMARY KEY (`mt_id`);

--
-- Indexes for table `phinxlog`
--
ALTER TABLE `phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `school`
--
ALTER TABLE `school`
  ADD PRIMARY KEY (`school_id`);

--
-- Indexes for table `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`teacher_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `county`
--
ALTER TABLE `county`
  MODIFY `county_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `employee_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `income`
--
ALTER TABLE `income`
  MODIFY `income_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ไอดี';

--
-- AUTO_INCREMENT for table `money_transaction`
--
ALTER TABLE `money_transaction`
  MODIFY `mt_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ไอดี';

--
-- AUTO_INCREMENT for table `school`
--
ALTER TABLE `school`
  MODIFY `school_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT for table `teacher`
--
ALTER TABLE `teacher`
  MODIFY `teacher_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ไอดี';
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
