<?php
require_once('MainConfig.php');
MainConfig::setConfig('config.env');
$config = [
	"paths" => [
		"migrations" => ["db/migrations"],
	],
	"environments" => [
		"default_migration_table" => "phinxlog",
		"default_database" => "default",
		"default" => [
			"adapter" => "mysql",
			"host" 	=> getenv('host'),
			"name" => getenv('name'),
			"user" => getenv('user'),
			"pass" => getenv('pass'),
			"port" => getenv('port'),
			"charset" => getenv('charset'),
			"collation" => getenv('collation'),
		],
	],
];

return $config;