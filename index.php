<?php require_once('./template/connect.php') ?>
<?php require_once('./template/header.php') ?>
<?php require_once('./template/navbar.php') ?>
<style>
@media (min-width: 1200px)
{
  .container, .container-lg, .container-md, .container-sm, .container-xl {
    max-width: 1600px;
  }
}
.form-block input {
  height: 40px;
}
.row_center {
  vertical-align : middle !important;
  text-align:center !important;
}
</style>


  <!--Breadcrumb Area-->
  <section class="breadcrumb-area banner-6">
    <div class="text-block">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 v-center">
            <div class="bread-inner">
              <div class="bread-menu">
                <ul>
                  <li><a href="index.html">หน้าแรก</a></li>
                  <li><a href="#">ใบลงเวลาครู</a></li>
                </ul>
              </div>
              <div class="bread-title">
                <h2>ใบลงเวลาครู</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--End Breadcrumb Area-->
  <!--Start Enquire Form-->
  <section class="contact-page pad-tb">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-12 v-center">
          <div class="common-heading text-l"  style="float: right;">
            <div class="fieldsets mt20"> <button type="button" onclick="get_data_from_school()" class="btn-main bg-btn2 lnk">ค้นหา <span class="circle"></span></button> </div>
          </div>
          <?php
            $teacher = ssTeacher();
            // pp($teacher);
          ?>
          <div class="form-block">

            <form id="form_input">
              <!-- <div class="row">
                <div class="fieldsets mt20"> <button type="button" onclick="get_data_from_school()" class="btn-main bg-btn3 lnk">ค้นหา <span class="circle"></span></button> </div>
              </div> -->
              <div class="row">

                <input type="hidden" name="t_dtr_id" id="t_dtr_id" value="">
                <input type="hidden" name="t_dtr_date" id="t_dtr_date" value="<?php echo date('Y-m-1') ?>">
                <input type="hidden" name="t_dtr_teacher_id" id="t_dtr_teacher_id" value="<?php echo $teacher['teacher_id'] ?>">
                <input type="hidden" name="t_dtr_teacher_code" id="t_dtr_teacher_code" value="<?php echo $teacher['teacher_code'] ?>">
                <input type="hidden" name="t_dtr_teacher_name" id="t_dtr_teacher_name" value="<?php echo $teacher['teacher_name'] ?>">
                <input type="hidden" name="t_dtr_school_name" id="t_dtr_school_name" value="">
                <input type="hidden" name="t_dtr_county_name" id="t_dtr_county_name" value="">
                <input type="hidden" name="t_dtr_total" id="t_dtr_total" value="">


                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4 mb-2 mt-2">
                  <div class="form-group">
                    <label class="l-light" for="t_dtr_school_id">ชื่อโรงเรียน</label>
                      <select name="t_dtr_school_id" id="t_dtr_school_id" class="form-control">
                        <option value="" selected>กรุณาเลือกชื่อโรงเรียน</option>
                        <?php
                        $id = json_decode($teacher['teacher_school_id']);
                        $implode = implode(',',$id);
                          $sql = "SELECT * FROM school WHERE school_id IN ($implode)";
                          $query = mysqli_query($conn, $sql);
                          if (mysqli_num_rows($query) > 0) {
                            while($rows = $query->fetch_assoc())
                            {
                              echo '<option value="'.$rows['school_id'].'">'.$rows['school_name'].'</option>';
                            }
                          }
                        ?>
                      </select>
                  </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4 mb-2 mt-2">
                  <div class="form-group">
                    <label class="l-light" for="t_dtr_subject">วิชาที่สอน</label>
                    <select id="t_dtr_subject" name="t_dtr_subject" class="form-control">
                      <option value="CH" <?php echo ($teacher['teacher_text_code'] == "CH") ? 'selected' : "" ?>>ครูจีน (CH)</option>
                      <option value="EN" <?php echo ($teacher['teacher_text_code'] == "EN") ? 'selected' : "" ?>>ครูอังกฤษ (EN)</option>
                      <option value="JP" <?php echo ($teacher['teacher_text_code'] == "JP") ? 'selected' : "" ?>>ครูญี่ปุ่น (JP)</option>
                    </select>
                  </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4 mb-2 mt-2">
                  <div class="form-group">
                    <label class="l-light" for="t_dtr_county_id">ชื่อเขต</label>
                    <select name="t_dtr_county_id" id="t_dtr_county_id" class="form-control selectpicker show-menu-arrow"  data-size="8"  data-live-search="true">
                      <option value="">กรุณาเลือกเขต</option>
                        <?php
                          $id = json_decode($teacher['teacher_district_id']);
                          $implode = implode(',',$id);
                          echo $sql = "SELECT * FROM county WHERE county_id IN ($implode)";
                          $query = mysqli_query($conn, $sql);
                          if (mysqli_num_rows($query) > 0) {
                            while($rows = $query->fetch_assoc())
                            {
                              echo '<option value="'.$rows['county_id'].'">'.$rows['county_name'].'</option>';
                            }
                          }
                        ?>
                    </select>
                  </div>
                </div>

              </div>

              <div class="row">
                <div class="table-responsive">
                  <table class="table table-bordered table-striped">
                    <thead>
                      <tr align="center">
                        <th colspan="3"><label class="l-light">Teacher Code : </label> </th>  
                        <th colspan="7">(Hr.Week,ชม./สัปดาห์)</th>    
                        <th colspan="3"><label class="l-light">Teacher's name : </label> </th>
                      </tr>
                      <tr align="center">
                        <th rowspan="3" class="row_center">Date</th>
                        <th rowspan="3" class="row_center">Day</th>
                        <th rowspan="3" class="row_center">Time In</th>
                        <th colspan="10">Time</th>
                      </tr>
                      <tr align="center">
                        <th>8:30</th>
                        <th>9:30</th>
                        <th>10:30</th>
                        <th rowspan="2" class="row_center">LunchBreak</th>
                        <th>12:30</th>
                        <th>13:30</th>
                        <th>14:30</th>
                        <th>15:30</th>
                        <th rowspan="2" class="row_center">Time Out</th>
                        <th rowspan="2" class="row_center">Hour/Day</th>
                      </tr>
                      <tr align="center">
                        <th>9:30</th>
                        <th>10:30</th>
                        <th>11:30</th>
                        <!-- <th>LunchBreak</th> -->
                        <th>13:30</th>
                        <th>14:30</th>
                        <th>15:30</th>
                        <th>16:30</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                        $j= 1;
                        for ($i=0; $i <= 30; $i++) { 
                      ?>
                      <tr align="center">
                        <td><?php echo $j ?><input type="hidden" name="t_dtr_d_date[]" id="t_dtr_d_date_<?php echo $i ?>" value="<?php echo $j ?>"></td>
                        <td><input type="text" class="form-control clear text-center t_dtr_d_day" name="t_dtr_d_day[]" value=""></td>
                        <td><input type="time" class="form-control clear text-center t_dtr_d_time_in" name="t_dtr_d_time_in[]" value=""></td>
                        <td><input type="text" class="form-control clear text-center t_dtr_d_time_1" name="t_dtr_d_time_1[]" value=""></td>
                        <td><input type="text" class="form-control clear text-center t_dtr_d_time_2" name="t_dtr_d_time_2[]" value=""></td>
                        <td><input type="text" class="form-control clear text-center t_dtr_d_time_3" name="t_dtr_d_time_3[]" value=""></td>
                        <td></td> <!-- Lunch Break -->
                        <td><input type="text" class="form-control clear text-center t_dtr_d_time_4" name="t_dtr_d_time_4[]" value=""></td>
                        <td><input type="text" class="form-control clear text-center t_dtr_d_time_5" name="t_dtr_d_time_5[]" value=""></td>
                        <td><input type="text" class="form-control clear text-center t_dtr_d_time_6" name="t_dtr_d_time_6[]" value=""></td>
                        <td><input type="text" class="form-control clear text-center t_dtr_d_time_7" name="t_dtr_d_time_7[]" value=""></td>
                        <td><input type="time" class="form-control clear text-center t_dtr_d_time_out" name="t_dtr_d_time_out[]" value=""></td>
                        <td><input type="text" class="form-control clear text-center cal_hour i_number c_select t_dtr_d_time_total" name="t_dtr_d_time_total[]" value="" onkeyup="cal_hour()"></td>
                      </tr>
                      <?php 
                          $j++;
                        } 
                      ?>
                      <tr>
                        <td colspan="10" align="right">Total</td>
                        <td colspan="3" align="center" id="total">  Hour/Month</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

              <div class="row" style="float: right;">
                <div class="fieldsets mt20"> <button type="button" onclick="btn_save()" class="btn-main bg-btn3 lnk">บันทึก <span class="circle"></span></button> </div>
              </div>
            </form>

          </div>
        </div>
        
      </div>
    </div>
  </section>
  <!--End Enquire Form-->
<?php require_once('./template/mobile_menu.php') ?>
<?php require_once('./template/footer.php') ?>
<?php require_once('./template/script.php') ?>
  
<script>
  function btn_save() 
  {
    $.ajax({
      type: "POST",
      url: "template/action_save.php",
      data: $("#form_input").serialize(),
      success: function(res) 
      {
        // res_swal(res,1);
      }
    });
  }

  function get_data_from_school() 
  {
    $('.clear').val("");
    var school_id = $('#t_dtr_school_id option:selected').val();
    var teacher_id = $('#t_dtr_teacher_id option:selected').val();
    $.ajax({
      type: "POST",
      url: "template/get_data_from_school.php",
      data: {
        school_id:school_id,
        teacher_id :teacher_id
      },
      dataType: "json",
      success: function (res) 
      {
        console.log(res)
      }
    });
  }

  function res_swal(result,reload=0,fx={})
  {
    if((result==false) || (result=='') || (result==0))
    {
      result = {
        type: 'error',
        title: 'แจ้งเตือน',
        text: 'เกิดข้อผิดพลาดโปรดติดต่อเจ้าหน้าที่!',
      }
      reload = 0;
    }
    
    Swal.fire({
      icon:result.type,
      title:result.title,
      text:result.text,
      html:result.html,
      footer:result.footer,
      onClose: () => {
        if(reload==1)
        {
          location.reload();
        }
        else if(reload==2)
        {
          if(result.type == 'success') // ถ้าผิดพลาด ไม่ปิด modal
          {
            $('.modal').modal('hide');
          }
        }
        else if(reload==3)
        {
          if(result.type == 'success') // ถ้าผิดพลาด ไม่ปิด modal
          {
            fx();
            $('.modal').modal('hide');
          }
        }
      }
    });
  }
</script>