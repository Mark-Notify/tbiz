
  <!--scroll to top-->
  <a id="scrollUp" href="#top"></a>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="./access/js/jquery.min.js"></script>
  <script src="./access/js/jquery.validate.min.js"></script>
  <script src="./access/js/bootstrap.min.js"></script>
  <script src="./access/js/popper.min.js"></script>
  <script src="./access/js/isotope-min.js"></script>
  <script src="./access/js/imagesloaded.pkgd.min.js"></script>
  <script src="./access/js/owl.carousel.js"></script>
  <script src="./access/js/SmoothScroll.min.js"></script>
  <script src="./access/js/jquery.waypoints.min.js"></script>
  <script src="./access/js/jquery.counterup.min.js"></script>
  <script src="./access/js/jquery.scrollUp.min.js"></script>
  <!--common script file-->
  <script src="./access/js/main.js"></script>
</body>
</html>