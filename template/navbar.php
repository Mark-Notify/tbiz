  <!--Start Header -->
  <header class="top-header">
      <nav class="navbar navbar-expand-lg navbar-light justify-content-right navbar-mobile fixed-top">
        <div class="container">
          <a class="navbar-brand" href="./"> <img src="./access/images/logo.png" alt="Logo" width="250" /></a>
          <button class="navbar-toggler mobile-none" type="button" data-toggle="collapse" data-target="#navbar4" aria-controls="navbar4" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
          <div class="collapse navbar-collapse animate slideIn mobile-none" id="navbar4">
          <ul class="mr-auto"></ul>
          <ul class="navbar-nav d-menu">
            <li class="nav-item"> <a class="nav-link" href="./">ใบลงเวลาครู </a> </li>
            <li class="nav-item"> <a class="nav-link" href="#">ฟอร์มเบิกล่วงหน้า</a> </li>
            <li class="nav-item"> <a class="nav-link custom-btn lnk btn-main bg-btn" href="#">Get A Quote <span class="circle"></span></a> </li>
          </ul>
        </div>
        <div class="mobile-menu">
          <ul class="mob-nav">
            <li> <a class="custom-btn lnk btn-main bg-btn" href="#">Get A Quote<span class="circle"></span></a></li>
            <li> <a class="nav-link mobilemenu" href="#" data-toggle="modal" data-target="#menu"><i class="fas fa-bars"></i></a> </li>
          </ul>
        </div>
      </div>
    </nav>
  </header>
  <!--End Header -->