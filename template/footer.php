    <!--Start Footer-->
    <footer>
    <div class="footer-svg">
      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        viewBox="0 0 1920 80" style="enable-background:new 0 0 1920 80;" xml:space="preserve">
        <path class="st0" d="M0,27.2c589.2,129.4,1044-69,1920,31v-60H3.2L0,27.2z"/>
      </svg>
    </div>
    <div class="footer-row2">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-sm-6">
            <h5>Contact Us</h5>
            <ul class="footer-address-list">
              <li>
                <span><i class="fas fa-envelope"></i></span>
                <p>Email <span> <a href="mailto:info@businessname.com">info@businessname.com</a></span></p>
              </li>
              <li>
                <span><i class="fas fa-phone-alt"></i></span>
                <p>Phone <span> <a href="tel:+10000000000">+1 0000 000 000</a></span></p>
              </li>
              <li>
                <span><i class="fas fa-map-marker-alt"></i></span>
                <p>Address <span> 123 Business Centre London SW1A 1AA</span></p>
              </li>
            </ul>
          </div>
          <div class="col-lg-3 col-sm-6">
            <h5>Company</h5>
            <ul class="footer-address-list link-hover">
              <li><a href="about.html">About us</a></li>
              <li><a href="service.html">Services</a></li>
              <li><a href="portfolio.html">Portfolio</a></li>
              <li><a href="blog-grid.html">Blogs</a></li>
            </ul>
          </div>
          <!-- <div class="col-lg-3 col-sm-6">
            <h5>Service</h5>
            <ul class="footer-address-list link-hover">
              <li><a href="javascript:void(0)">Logo & Branding</a></li>
              <li><a href="javascript:void(0)">Website Development</a></li>
              <li><a href="javascript:void(0)">Mobile App Development</a></li>
              <li><a href="javascript:void(0)">Search Engine Optimization</a></li>
              <li><a href="javascript:void(0)">Pay-Per-Click</a></li>
              <li><a href="javascript:void(0)">Social Media Marketing</a></li>
            </ul>
          </div> -->
          <div class="col-lg-3 col-sm-6">
            <h5>Support</h5>
            <ul class="footer-address-list link-hover">
              <li><a href="get-quote.html">Contact</a></li>
              <li><a href="javascript:void(0)">Privacy Policy</a></li>
              <li><a href="javascript:void(0)">Sitemap</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <hr class="hline">
    <div class="footer-row3">
      <div class="copyright">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="footer-social-media-icons">
                <a href="javascript:void(0)" target="blank"><i class="fab fa-facebook"></i></a>
                <a href="javascript:void(0)" target="blank"><i class="fab fa-twitter"></i></a>
                <a href="javascript:void(0)" target="blank"><i class="fab fa-instagram"></i></a>
                <a href="javascript:void(0)" target="blank"><i class="fab fa-linkedin"></i></a>
                <a href="javascript:void(0)" target="blank"><i class="fab fa-youtube"></i></a>
                <a href="javascript:void(0)" target="blank"><i class="fab fa-pinterest-p"></i></a>
                <a href="javascript:void(0)" target="blank"><i class="fab fa-vimeo-v"></i></a>
                <a href="javascript:void(0)" target="blank"><i class="fab fa-dribbble"></i></a>
                <a href="javascript:void(0)" target="blank"><i class="fab fa-behance"></i></a>
              </div>
              <div class="footer-">
              <p>Copyright &copy; 2020 Niwax. All rights reserved.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!--End Footer-->
