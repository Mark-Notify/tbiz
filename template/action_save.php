<?php
    require_once('connect.php');
    // echo '<pre>'; print_r($_POST); echo '</pre>';
    $date = date('Y-m-d H:i:s');
    $sql_head = "INSERT INTO teacher_dtr(
            t_dtr_date,
            t_dtr_subject,
            t_dtr_teacher_id,
            t_dtr_teacher_code,
            t_dtr_teacher_name,
            t_dtr_school_id,
            t_dtr_school_name,
            t_dtr_county_id,
            t_dtr_county_name,
            t_dtr_total,
            staff_created_at,
            staff_updated_at
        ) 
        VALUES
        (
            '$_POST[t_dtr_date]',
            '$_POST[t_dtr_subject]',
            '$_POST[t_dtr_teacher_id]',
            '$_POST[t_dtr_teacher_code]',
            '$_POST[t_dtr_teacher_name]',
            '$_POST[t_dtr_school_id]',
            '$_POST[t_dtr_school_name]',
            '$_POST[t_dtr_county_id]',
            '$_POST[t_dtr_county_name]',
            '$_POST[t_dtr_total]',
            '$date',
            '$date'
        )";
    $result = mysqli_query($conn,$sql_head);
    $last_id = mysqli_insert_id($conn);
    // echo $last_id;
    // echo '<pre>'; print_r($_POST['t_dtr_d_time_1']); echo '</pre>';
    foreach ($_POST['t_dtr_d_date'] as $key => $value) 
    {
        // echo '<pre>'; print_r($_POST['t_dtr_d_time_1'][$key]); echo '</pre>';
        $sql_detail = "INSERT INTO teacher_dtr_detail(
            t_dtr_id,
            t_dtr_d_date,
            t_dtr_d_day,
            t_dtr_d_time_in,
            t_dtr_d_time_1,
            t_dtr_d_time_2,
            t_dtr_d_time_3,
            t_dtr_d_time_4,
            t_dtr_d_time_5,
            t_dtr_d_time_6,
            t_dtr_d_time_7,
            t_dtr_d_time_out,
            t_dtr_d_time_total
        ) 
        VALUES
        (
            '".$last_id."',
            '".$_POST['t_dtr_d_date'][$key]."',
            '".$_POST['t_dtr_d_day'][$key]."',
            '".$_POST['t_dtr_d_time_in'][$key]."',
            '".$_POST['t_dtr_d_time_1'][$key]."',
            '".$_POST['t_dtr_d_time_2'][$key]."',
            '".$_POST['t_dtr_d_time_3'][$key]."',
            '".$_POST['t_dtr_d_time_4'][$key]."',
            '".$_POST['t_dtr_d_time_5'][$key]."',
            '".$_POST['t_dtr_d_time_6'][$key]."',
            '".$_POST['t_dtr_d_time_7'][$key]."',
            '".$_POST['t_dtr_d_time_out'][$key]."',
            '".$_POST['t_dtr_d_time_total'][$key]."'
        )";
        $result = mysqli_query($conn,$sql_detail);
    }
    if($result > 0){
        echo json_encode(array('type' => 'success','title' => 'บันทึกสำเร็จ','text' => 'กดปุ่มเพื่อดำเนินการต่อ'));
    }else{
        echo json_encode(array('type' => 'error','title' => 'บันทึกผิดพลาด','text' => 'โปรดลองใหม่อีกครั้ง'));
    }
    // echo $result;
    // echo '<pre>'; print_r($result); echo '</pre>';
?>