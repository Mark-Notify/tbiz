<?php
  require_once('./template/helper.php')
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!--website title-->
  <title>Contact - Web Design & Digital Marketing Agency HTML Template</title>
  <!--seo-meta-tag-->
  <meta charset="UTF-8"/>
  <meta name="description" content="Creative Agency, Marketing Agency Template">
  <meta name="keywords" content="Creative Agency, Marketing Agency">
  <meta name="author" content="rajesh-doot">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!--website-favicon-->
  <link rel="icon" type="image/png" href="images/favicon.png">
  <!--plugin-css-->
  <link href="./access/css/bootstrap.min.css" rel="stylesheet">
  <link href="./access/css/owl.carousel.min.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css"  rel="stylesheet">
  <!--google-fonts-->
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">
  <!-- template-style-->
  <link type="text/css" href="./access/css/style.css" rel="stylesheet">
  <link type="text/css" href="./access/css/responsive.css" rel="stylesheet">
</head>
<body>