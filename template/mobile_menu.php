  <!--Responsive Mobile Menu-->
  <div id="menu" class="res-menu p-0 modal fade" role="dialog" aria-labelledby="menu" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content full">
        <div class="modal-header" data-dismiss="modal">
          <img src="images/logo.png" alt="Logo" width="100"/> <i class="far fa-times-circle"></i>
        </div>
        <div class="menu modal-body">
          <div class="m-nav">
            <nav class="navbar p0">
              <ul class="navbar-nav">
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="homepage" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Home</a>
                  <div class="dropdown-menu animate slideIn" aria-labelledby="homepage">
                    <a class="dropdown-item" href="./">Web Development</a>
                    <a class="dropdown-item" href="index-dark.html">Web Development - Dark</a>
                    <a class="dropdown-item" href="index1.html">Digital Marketing</a>
                    <a class="dropdown-item" href="index1-dark.html">Digital Marketing - Dark</a>
                    <a class="dropdown-item" href="index2.html">Freelance Portfolio</a>
                    <a class="dropdown-item" href="index2-dark.html">Freelance Portfolio - Dark</a>
                    <a class="dropdown-item" href="index3.html">Lead Generation Agency</a>
                    <a class="dropdown-item" href="index3-dark.html">Lead Generation Agency - Dark</a>
                  </div>
                </li>
                <li class="nav-item"> <a class="nav-link" href="about.html">About </a>  </li>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="Services2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Services </a>
                  <div class="dropdown-menu animate slideIn" aria-labelledby="Services2">
                    <a class="dropdown-item" href="service.html">Service</a>
                    <a class="dropdown-item" href="service-details.html">Service Details</a>
                  </div>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="Portfolio2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Portfolio </a>
                  <div class="dropdown-menu animate slideIn" aria-labelledby="Portfolio2">
                    <a class="dropdown-item" href="portfolio.html">Portfolio Grid</a>
                    <a class="dropdown-item" href="portfolio-details.html">Portfolio Details</a>
                  </div>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="Pages2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Pages </a>
                  <div class="dropdown-menu animate slideIn" aria-labelledby="Pages2">
                    <a class="dropdown-item" href="pricing.html">Pricing</a>
                    <a class="dropdown-item" href="team.html">Team</a>
                    <a class="dropdown-item" href="team-details.html">Team Single</a>
                    <a class="dropdown-item" href="case-study.html">Case Study</a>
                    <a class="dropdown-item" href="case-study-details.html">Case Study Single</a>
                    <a class="dropdown-item" href="typography.html">Typography</a>
                    <a class="dropdown-item" href="button.html">Buttons</a>
                    <a class="dropdown-item" href="faq.html">Accordion & FAQ</a>
                    <a class="dropdown-item" href="error.html">404 Page</a>
                    <a class="dropdown-item" href="gradients.html">Background Gradients</a>
                  </div>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="Blog2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Blog </a>
                  <div class="dropdown-menu animate slideIn" aria-labelledby="Blog2">
                    <a class="dropdown-item" href="blog-grid.html">Blog Grid</a>
                    <a class="dropdown-item" href="blog-single.html">Blog Single</a>
                  </div>
                </li>
                <li class="nav-item"><a class="nav-link" href="get-quote.html">Contact</a>  </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div>